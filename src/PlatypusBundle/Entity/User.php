<?php

namespace PlatypusBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Length as Length;
use Doctrine\Common\Collections\ArrayCollection;


/**
* User
*/
class User implements UserInterface
{
  /**
  * @var int
  */
  private $id;

  /**
  * @var string
  * @Assert\Regex("/^[A-Za-z]+\d*$/")
  * @Assert\Length(
  * min = 3,
  * max = 50
  * )
  */
  private $username;

  /**
  * @var string
  *@Assert\Length(
  * min=3,
  * max=50
  * )
  */
  private $password;

  /**
  * @var string
  */
  private $email;

  /**
  * @var string
  */
  private $firstName;

  /**
  * @var string
  */
  private $lastName;

  /**
  * @var array
  */
  private $roles;

  /**
  * @var \string
  */
  private $creattionDate;



  /**
  * Get id
  *
  * @return int
  */
  public function getId()
  {
    return $this->id;
  }

  /**
  * Set username
  *
  * @param string $username
  *
  * @return User
  */
  public function setUsername($username)
  {
    $this->username = $username;

    return $this;
  }

  /**
  * Get username
  *
  * @return string
  */
  public function getUsername()
  {
    return $this->username;
  }

  /**
  * Set password
  *
  * @param string $password
  *
  * @return User
  */
  public function setPassword($password)
  {
    $this->password = $password;

    return $this;
  }

  /**
  * Get password
  *
  * @return string
  */
  public function getPassword()
  {
    return $this->password;
  }

  /**
  * Set email
  *
  * @param string $email
  *
  * @return User
  */
  public function setEmail($email)
  {
    $this->email = $email;

    return $this;
  }

  /**
  * Get email
  *
  * @return string
  */
  public function getEmail()
  {
    return $this->email;
  }

  /**
  * Set firstName
  *
  * @param string $firstName
  *
  * @return User
  */
  public function setFirstName($firstName)
  {
    $this->firstName = $firstName;

    return $this;
  }

  /**
  * Get firstName
  *
  * @return string
  */
  public function getFirstName()
  {
    return $this->firstName;
  }

  /**
  * Set lastName
  *
  * @param string $lastName
  *
  * @return User
  */
  public function setLastName($lastName)
  {
    $this->lastName = $lastName;

    return $this;
  }

  /**
  * Get lastName
  *
  * @return string
  */
  public function getLastName()
  {
    return $this->lastName;
  }

  /**
  * Set roles
  *
  * @param array $roles
  *
  * @return User
  */
  public function setRoles($roles)
  {
    $this->roles = $roles;

    return $this;
  }

  /**
  * Get roles
  *
  * @return array
  */
  public function getRoles()
  {
    return $this->roles;
  }

  /**
  * Set creattionDate
  *
  * @param \string $creattionDate
  *
  * @return User
  */
  public function setCreattionDate($creattionDate)
  {
    $this->creattionDate = $creattionDate;

    return $this;
  }

  /**
  * Get creattionDate
  *
  * @return \string
  */
  public function getCreattionDate()
  {
    return $this->creattionDate;
  }

  public function eraseCredentials()
  {
  }

  public function getSalt()
  {

    return null;
  }
}
