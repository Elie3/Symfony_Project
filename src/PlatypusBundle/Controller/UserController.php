<?php

namespace PlatypusBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PlatypusBundle\Entity\User;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use PlatypusBundle\Repository\UserRepository;


class UserController extends Controller
{
  public function indexAction()
  {
    $authUtils = $this->get('security.authentication_utils');

    $error = $authUtils->getLastAuthenticationError();

    // last username entered by the user
    $lastUsername = $authUtils->getLastUsername();
    return $this->render('PlatypusBundle:User:login.html.twig', array(
      'last_username' => $lastUsername,
      'error'         => $error
    ));
  }

  public function registerAction(Request $request)
  {

    $user = new User();

    $user->setUsername("Write your Username");
    $user->setPassword("Write your Password");
    $user->setEmail("Write your Email");
    $user->setFirstName("Write your First Name");
    $user->setLastName("Write your Last Name");

    $form = $this->createFormBuilder($user)
    ->add('Username', TextType::class)
    ->add('Password', PasswordType::class)
    ->add('Email', EmailType::class)
    ->add('First_Name', TextType::class)
    ->add('Last_Name', TextType::class)
    ->add('save', SubmitType::class, array('label' => 'Create Post'))
    ->getForm();

    $form->handleRequest($request);

    if ($form->IsSubmitted() && $form->IsValid())
    {
      $passwordEncoder = $this->get('security.password_encoder');
      $user->setPassword($passwordEncoder->encodePassword($user, $user->getPassword()));
      $user->setCreattionDate(new \DateTime());
      $user->setRoles(["ROLE_BLOGGER"]);
      $user = $form->getData();

      $em = $this->getDoctrine()->getManager();
      $em->persist($user);
      $em->flush();

      return $this->render('PlatypusBundle:User:register.html.twig',
      array("text" => "The user ".$user->getFirstName()." ".$user->getLastName()." was created", 'form' => $form->createView(), 'tag' => "ok"));
    }
    else if ($form->IsSubmitted())
    {
      return $this->render('PlatypusBundle:User:register.html.twig', array("text" => "Failure of the registration", 'form' => $form->createView(), 'tag' => "Erreur"));
    }
    else
    {

      return $this->render('PlatypusBundle:User:register.html.twig', array(
        'form' => $form->createView(), "text" => "", 'tag' => "pas encore fait"
      ));
    }
  }

  public function loginAction(Request $request)
  {
    $authUtils = $this->get('security.authentication_utils');
    // get the login error if there is one
    $error = $authUtils->getLastAuthenticationError();

    // last username entered by the user
    $lastUsername = $authUtils->getLastUsername();

    if ($error == null)
    {
      /*
      return $this->render('PlatypusBundle:User:my_page.html.twig', array(
      'last_username' => $lastUsername,
      'error'         => $error,
      'message' => "Hello ".$lastUsername
    ));*/
  }

  return $this->render('PlatypusBundle:User:login.html.twig', array(
    'last_username' => $lastUsername,
    'error'         => $error
  ));
}

public function myPageAction()
{
  return $this->render('PlatypusBundle:User:my_page.html.twig', array('user' => $this->getUser()));
}

public function editUserAction(Request $request)
{
  $user = $this->get('security.token_storage')->getToken()->getUser();


  $form = $this->createFormBuilder($user)
  ->add('Username', TextType::class)
  ->add('Password', RepeatedType::class,
  array('type' => PasswordType::class,
  'invalid_message' => 'The passwords must match.',
  'required' =>'true'))
  ->add('Email', EmailType::class)
  ->add('First_Name', TextType::class)
  ->add('Last_Name', TextType::class)
  ->add('save', SubmitType::class, array('label' => 'Create Post'))
  ->getForm();

  $form->handleRequest($request);

  if ($form->IsSubmitted() && $form->IsValid())
  {
    $passwordEncoder = $this->get('security.password_encoder');
    $user->setPassword($passwordEncoder->encodePassword($user, $user->getPassword()));
    $user = $form->getData();

    $em = $this->getDoctrine()->getManager();
    $em->persist($user);
    $em->flush();

    return $this->render('PlatypusBundle:User:my_page.html.twig',
    array( 'user' => $this->getUser()));
  }
  else if ($form->IsSubmitted())
  {
    return $this->render('PlatypusBundle:User:edit_user.html.twig',
    array('form' => $form->createView(), 'text' => "Le formulaire n'a pas ete correctement rempli"));
  }
  else
  {
    return $this->render('PlatypusBundle:User:edit_user.html.twig',
    array('form' => $form->createView(), 'text' => "Veuillez remplir le formulaire"));
  }
}

public function listUserAction()
{
  $user = $this->get('security.token_storage')->getToken()->getUser();
  if ($user->getRoles()[0] !=  "ROLE_ADMIN")
  {
    $repository = $this->getDoctrine()->getRepository(User::class);
    $users = $repository->findAll();
    return $this->render('PlatypusBundle:User:all_users.html.twig', array('users' => $users));
  }
  else
  {
    $repository = $this->getDoctrine()->getRepository(User::class);
    $users = $repository->findAll();
    return $this->render('PlatypusBundle:User:all_users_admin.html.twig', array('users' => $users));
  }
}

public function edit_a_userAction($id, Request $request)
{
  $repository = $this->getDoctrine()->getRepository(User::class);
  $user = $repository->loadUserById($id);

  $form = $this->createFormBuilder($user)
  ->add('Username', TextType::class)
  ->add('Password', RepeatedType::class,
  array('type' => PasswordType::class,
  'invalid_message' => 'The passwords must match.',
  'required' =>'true'))
  ->add('Email', EmailType::class)
  ->add('First_Name', TextType::class)
  ->add('Last_Name', TextType::class)
  ->add('save', SubmitType::class, array('label' => 'Create Post'))
  ->getForm();

  $form->handleRequest($request);

  if ($form->IsSubmitted() && $form->IsValid())
  {
    $passwordEncoder = $this->get('security.password_encoder');
    $user->setPassword($passwordEncoder->encodePassword($user, $user->getPassword()));
    $user = $form->getData();

    $em = $this->getDoctrine()->getManager();
    $em->persist($user);
    $em->flush();

    $repository = $this->getDoctrine()->getRepository(User::class);
    $users = $repository->findAll();
    return $this->render('PlatypusBundle:User:all_users_admin.html.twig', array('users' => $users));
  }
  else
  {
    return $this->render('PlatypusBundle:User:edit_user.html.twig',
    array('form' => $form->createView(), 'text' => "Veuillez remplir le formulaire"));
  }

}

public function delete_a_userAction($id)
{
  $repository = $this->getDoctrine()->getRepository(User::class);
  $user = $repository->loadUserById($id);
  $em = $this->getDoctrine()->getManager();
  $em->remove($user);
  $em->flush();

  $users = $repository->findAll();
  return $this->render('PlatypusBundle:User:all_users_admin.html.twig', array('users' => $users));
}
}

?>
