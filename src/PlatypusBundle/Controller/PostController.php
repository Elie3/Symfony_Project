<?php

namespace PlatypusBundle\Controller;

use PlatypusBundle\Entity\Post;
use PlatypusBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use PlatypusBundle\Repository\UserRepository;
use PlatypusBundle\Repository\PostRepository;


/**
* Post controller.
*
*/
class PostController extends Controller
{
  /**
  * Lists all post entities.
  *
  */
  public function indexAction()
  {
    $em = $this->getDoctrine()->getManager();

    $posts = $em->getRepository('PlatypusBundle:Post')->findAll();

    return $this->render('post/index.html.twig', array(
      'posts' => $posts,
    ));
  }

  /**
  * Creates a new post entity.
  *
  */
  public function newAction(Request $request)
  {
    $post = new Post();

    $form = $this->createFormBuilder($post)
    ->add('Title', TextType::class)
    ->add('Content', TextareaType::class)
    ->add('save', SubmitType::class, array('label' => 'Create Post'))
    ->getForm();
    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid())
    {
      $post->setCreationDate(new \DateTime());
      $post->setModificationDate(new \DateTime());
      $user = $this->get('security.token_storage')->getToken()->getUser();
      $post->setUser($user);
      // var_dump($user->());
      $post = $form->getData();
      $em = $this->getDoctrine()->getManager();
      $em->persist($post);
      $em->flush();


      $repository = $this->getDoctrine()->getRepository(Post::class);
      $posts = $repository->findAll();
      return $this->render('PlatypusBundle:Post:posts_admin.html.twig', array('posts' => $posts));

    }
    else if ($form->isSubmitted())
    {
      return $this->render('PlatypusBundle:Post:new.html.twig', array(
        'form' => $form->createView(),
        'text' => 'Il y a une erreur avec votre formulaire'
      ));
    }
    else
    {
      return $this->render('PlatypusBundle:Post:new.html.twig', array(
        'form' => $form->createView(),
        'text' =>'Veuillez remplir votre formulaire'
      ));
    }
  }

  /**
  * Finds and displays a post entity.
  *
  */
  public function showAction(Post $post)
  {
    $deleteForm = $this->createDeleteForm($post);

    return $this->render('post/show.html.twig', array(
      'post' => $post,
      'delete_form' => $deleteForm->createView(),
    ));
  }

  /**
  * Displays a form to edit an existing post entity.
  *
  */
  public function editAction(Request $request, $id)
  {
    $repository = $this->getDoctrine()->getRepository(Post::class);
    $post = $repository->loadPostById($id);
    $form = $this->createFormBuilder($post)
    ->add('Title', TextType::class)
    ->add('Content', TextareaType::class)
    ->add('save', SubmitType::class, array('label' => 'Edit Post'))
    ->getForm();

    $form->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid())
    {
      $post = $form->getData();
      $em = $this->getDoctrine()->getManager();
      $em->persist($post);
      $em->flush();
      $repository = $this->getDoctrine()->getRepository(Post::class);
      $posts = $repository->findAll();

      $user = $this->get('security.token_storage')->getToken()->getUser();
      $repository = $this->getDoctrine()->getRepository(Post::class);
      $posts = $repository->findAll();
      if ($user->getRoles()[0] ==  "ROLE_USER")
      {
        return $this->render('PlatypusBundle:Post:posts_user.html.twig', array('posts' => $posts));
      }
      else if ($user->getRoles()[0] == "ROLE_BLOGGER")
      {
        return $this->render('PlatypusBundle:Post:posts_blogger.html.twig', array('posts' => $posts));
      }
      else
      {
        return $this->render('PlatypusBundle:Post:posts_admin.html.twig', array('posts' => $posts));
      }
    }

    return $this->render('PlatypusBundle:Post:edit_post.html.twig', array(
      'form' => $form->createView(),

    ));
  }

  /**
  * Deletes a post entity.
  *
  */
  public function deleteAction(Request $request, Post $post)
  {
    $em = $this->getDoctrine()->getManager();
    $em->remove($post);
    $em->flush();

    $repository = $this->getDoctrine()->getRepository(Post::class);
    $posts = $repository->findAll();
    return $this->render('PlatypusBundle:Post:posts_admin.html.twig', array('posts' => $posts));


  }

  /**
  * Creates a form to delete a post entity.
  *
  * @param Post $post The post entity
  *
  * @return \Symfony\Component\Form\Form The form
  */
  private function createDeleteForm(Post $post)
  {
    return $this->createFormBuilder()
    ->setAction($this->generateUrl('post_delete', array('id' => $post->getId())))
    ->setMethod('DELETE')
    ->getForm()
    ;
  }

  public function listAction()
  {
    $user = $this->get('security.token_storage')->getToken()->getUser();
    $repository = $this->getDoctrine()->getRepository(Post::class);
    $posts = $repository->findAll();
    if ($user->getRoles()[0] ==  "ROLE_USER")
    {
      return $this->render('PlatypusBundle:Post:posts_user.html.twig', array('posts' => $posts));
    }
    else if ($user->getRoles()[0] == "ROLE_BLOGGER")
    {
      return $this->render('PlatypusBundle:Post:posts_blogger.html.twig', array('posts' => $posts));
    }
    else
    {
      return $this->render('PlatypusBundle:Post:posts_admin.html.twig', array('posts' => $posts));
    }
  }



  public function mypostAction()
  {
    $user = $this->get('security.token_storage')->getToken()->getUser();
    $id = $user->getId();
    $repository = $this->getDoctrine()->getRepository(Post::class);
    $posts = $repository->loadPostByUserId($id);
    return $this->render('PlatypusBundle:Post:my_posts.html.twig', array('posts' => $posts));
  }



}
