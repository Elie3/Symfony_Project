<?php

namespace PlatypusBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use PlatypusBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData implements FixtureInterface, ContainerAwareInterface
{

 private $container;

 public function setContainer(ContainerInterface $container = null)
 {
   $this->container = $container;
 }

   public function load(ObjectManager $manager)
   {
     $encoder = $this->container->get('security.password_encoder');

       $userBlogger = new User();
       $userBlogger->setUsername('Martin');
       $userBlogger->setFirstName('Martin');
       $userBlogger->setLastName('Matin');
       $userBlogger->setPassword($encoder->encodePassword($userBlogger, 'platypus'));
       $userBlogger->setRoles(['ROLE_BLOGGER']);
       $userBlogger->setEmail('martin@coding.fr');
       $userBlogger->setCreattionDate(new \DateTime());

       $userAdmin = new User();
       $userAdmin->setUsername('Gecko');
       $userAdmin->setFirstName('Gecko');
       $userAdmin->setLastName('Coco');
       $userAdmin->setPassword($encoder->encodePassword($userAdmin, 'coding'));
       $userAdmin->setRoles(['ROLE_ADMIN']);
       $userAdmin->setEmail('gecko@coding.eu');
       $userAdmin->setCreattionDate(new \DateTime());


       $manager->persist($userAdmin);
       $manager->persist($userBlogger);

       $manager->flush();
   }
}
?>
