<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevDebugProjectContainerUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $trimmedPathinfo = rtrim($pathinfo, '/');
        $context = $this->context;
        $request = $this->request;
        $requestMethod = $canonicalMethod = $context->getMethod();
        $scheme = $context->getScheme();

        if ('HEAD' === $requestMethod) {
            $canonicalMethod = 'GET';
        }


        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if ('/_profiler' === $trimmedPathinfo) {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ('/_profiler/search' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ('/_profiler/search_bar' === $pathinfo) {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_phpinfo
                if ('/_profiler/phpinfo' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler_open_file
                if ('/_profiler/open' === $pathinfo) {
                    return array (  '_controller' => 'web_profiler.controller.profiler:openAction',  '_route' => '_profiler_open_file',);
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        elseif (0 === strpos($pathinfo, '/post')) {
            // post_index
            if ('/post' === $trimmedPathinfo) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto not_post_index;
                }

                if (substr($pathinfo, -1) !== '/') {
                    return $this->redirect($pathinfo.'/', 'post_index');
                }

                return array (  '_controller' => 'PlatypusBundle\\Controller\\PostController::indexAction',  '_route' => 'post_index',);
            }
            not_post_index:

            // post_show
            if (preg_match('#^/post/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
                if ('GET' !== $canonicalMethod) {
                    $allow[] = 'GET';
                    goto not_post_show;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'post_show')), array (  '_controller' => 'PlatypusBundle\\Controller\\PostController::showAction',));
            }
            not_post_show:

            // post_new
            if ('/post/new' === $pathinfo) {
                if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                    $allow = array_merge($allow, array('GET', 'POST'));
                    goto not_post_new;
                }

                return array (  '_controller' => 'PlatypusBundle\\Controller\\PostController::newAction',  '_route' => 'post_new',);
            }
            not_post_new:

            // post_edit
            if (preg_match('#^/post/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                if (!in_array($canonicalMethod, array('GET', 'POST'))) {
                    $allow = array_merge($allow, array('GET', 'POST'));
                    goto not_post_edit;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'post_edit')), array (  '_controller' => 'PlatypusBundle\\Controller\\PostController::editAction',));
            }
            not_post_edit:

            // post_delete
            if (preg_match('#^/post/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                if ('DELETE' !== $canonicalMethod) {
                    $allow[] = 'DELETE';
                    goto not_post_delete;
                }

                return $this->mergeDefaults(array_replace($matches, array('_route' => 'post_delete')), array (  '_controller' => 'PlatypusBundle\\Controller\\PostController::deleteAction',));
            }
            not_post_delete:

        }

        // platypus_homepage
        if ('' === $trimmedPathinfo) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'platypus_homepage');
            }

            return array (  '_controller' => 'PlatypusBundle\\Controller\\UserController::loginAction',  '_route' => 'platypus_homepage',);
        }

        // show
        if (preg_match('#^/(?P<type>Gecko|Human|Platypus)/show/(?P<id>\\d+)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'show')), array (  '_controller' => 'PlatypusBundle\\Controller\\CheckerController::showAction',));
        }

        // showParams
        if ('/checker/showParams' === $pathinfo) {
            return array (  '_controller' => 'PlatypusBundle\\Controller\\CheckerController::showParamsAction',  '_route' => 'showParams',);
        }

        // platypus_index
        if ('/index' === $pathinfo) {
            return array (  '_controller' => 'PlatypusBundle\\Controller\\UserController::loginAction',  '_route' => 'platypus_index',);
        }

        // register
        if ('/register' === $pathinfo) {
            return array (  '_controller' => 'PlatypusBundle\\Controller\\UserController::registerAction',  '_route' => 'register',);
        }

        // login
        if ('/login' === $pathinfo) {
            return array (  '_controller' => 'PlatypusBundle\\Controller\\UserController::loginAction',  '_route' => 'login',);
        }

        // my_page
        if ('/my_page' === $pathinfo) {
            return array (  '_controller' => 'PlatypusBundle\\Controller\\UserController::myPageAction',  '_route' => 'my_page',);
        }

        if (0 === strpos($pathinfo, '/user')) {
            // edit_user
            if ('/user/edit' === $pathinfo) {
                return array (  '_controller' => 'PlatypusBundle\\Controller\\UserController::editUserAction',  '_route' => 'edit_user',);
            }

            // list_user
            if ('/user/list' === $pathinfo) {
                return array (  '_controller' => 'PlatypusBundle\\Controller\\UserController::listUserAction',  '_route' => 'list_user',);
            }

            // edit_a_user
            if (preg_match('#^/user/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'edit_a_user')), array (  '_controller' => 'PlatypusBundle\\Controller\\UserController::edit_a_userAction',));
            }

            // delete_a_user
            if (preg_match('#^/user/(?P<id>[^/]++)/delete_a_user$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'delete_a_user')), array (  '_controller' => 'PlatypusBundle\\Controller\\UserController::delete_a_userAction',));
            }

        }

        elseif (0 === strpos($pathinfo, '/post')) {
            // view_all_posts
            if ('/post/list' === $pathinfo) {
                return array (  '_controller' => 'PlatypusBundle\\Controller\\PostController::listAction',  '_route' => 'view_all_posts',);
            }

            // create_new_post
            if ('/post/new' === $pathinfo) {
                return array (  '_controller' => 'PlatypusBundle\\Controller\\PostController::newAction',  '_route' => 'create_new_post',);
            }

            // edit_a_post
            if (preg_match('#^/post/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'edit_a_post')), array (  '_controller' => 'PlatypusBundle\\Controller\\PostController::editAction',));
            }

            // delete_a_post
            if (preg_match('#^/post/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => 'delete_a_post')), array (  '_controller' => 'PlatypusBundle\\Controller\\PostController::deleteAction',));
            }

            // show_my_posts
            if ('/post/my_post' === $pathinfo) {
                return array (  '_controller' => 'PlatypusBundle\\Controller\\PostController::mypostAction',  '_route' => 'show_my_posts',);
            }

        }

        // homepage
        if ('' === $trimmedPathinfo) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'homepage');
            }

            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::indexAction',  '_route' => 'homepage',);
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
