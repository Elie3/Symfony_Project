<?php

/* :post:index.html.twig */
class __TwigTemplate_dd98bce3c97bc268a38b547b137af89be51a19f31200a0ee860e9bb96a8d7fba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":post:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_aa9829569185cbe10e0a49fd09837bd90d6a5852c43ed450b6c4298feee9fc7e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aa9829569185cbe10e0a49fd09837bd90d6a5852c43ed450b6c4298feee9fc7e->enter($__internal_aa9829569185cbe10e0a49fd09837bd90d6a5852c43ed450b6c4298feee9fc7e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":post:index.html.twig"));

        $__internal_e49bd23b611d14940b86dcb66c38e69857752ad27be8f706049bb8278d8f830a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e49bd23b611d14940b86dcb66c38e69857752ad27be8f706049bb8278d8f830a->enter($__internal_e49bd23b611d14940b86dcb66c38e69857752ad27be8f706049bb8278d8f830a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":post:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_aa9829569185cbe10e0a49fd09837bd90d6a5852c43ed450b6c4298feee9fc7e->leave($__internal_aa9829569185cbe10e0a49fd09837bd90d6a5852c43ed450b6c4298feee9fc7e_prof);

        
        $__internal_e49bd23b611d14940b86dcb66c38e69857752ad27be8f706049bb8278d8f830a->leave($__internal_e49bd23b611d14940b86dcb66c38e69857752ad27be8f706049bb8278d8f830a_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_6ee7b991e6af9fd54619dc51ed4b2547ebe284772ff9d91752acb8a9b232e200 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6ee7b991e6af9fd54619dc51ed4b2547ebe284772ff9d91752acb8a9b232e200->enter($__internal_6ee7b991e6af9fd54619dc51ed4b2547ebe284772ff9d91752acb8a9b232e200_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_53523c109fbf13feb0001535ab4934025e343643e3f2a930c32d58b9486f3b87 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_53523c109fbf13feb0001535ab4934025e343643e3f2a930c32d58b9486f3b87->enter($__internal_53523c109fbf13feb0001535ab4934025e343643e3f2a930c32d58b9486f3b87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Posts list</h1>

    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Content</th>
                <th>Creationdate</th>
                <th>Modificationdate</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 18
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new Twig_Error_Runtime('Variable "posts" does not exist.', 18, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["post"]) {
            // line 19
            echo "            <tr>
                <td><a href=\"";
            // line 20
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("post_show", array("id" => twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "id", array()))), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "id", array()), "html", null, true);
            echo "</a></td>
                <td>";
            // line 21
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "title", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 22
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "content", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 23
            if (twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "creationDate", array())) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "creationDate", array()), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                <td>";
            // line 24
            if (twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "modificationDate", array())) {
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "modificationDate", array()), "Y-m-d H:i:s"), "html", null, true);
            }
            echo "</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"";
            // line 28
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("post_show", array("id" => twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "id", array()))), "html", null, true);
            echo "\">show</a>
                        </li>
                        <li>
                            <a href=\"";
            // line 31
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("post_edit", array("id" => twig_get_attribute($this->env, $this->getSourceContext(), $context["post"], "id", array()))), "html", null, true);
            echo "\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['post'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 37
        echo "        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 42
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("post_new");
        echo "\">Create a new post</a>
        </li>
    </ul>
";
        
        $__internal_53523c109fbf13feb0001535ab4934025e343643e3f2a930c32d58b9486f3b87->leave($__internal_53523c109fbf13feb0001535ab4934025e343643e3f2a930c32d58b9486f3b87_prof);

        
        $__internal_6ee7b991e6af9fd54619dc51ed4b2547ebe284772ff9d91752acb8a9b232e200->leave($__internal_6ee7b991e6af9fd54619dc51ed4b2547ebe284772ff9d91752acb8a9b232e200_prof);

    }

    public function getTemplateName()
    {
        return ":post:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 42,  119 => 37,  107 => 31,  101 => 28,  92 => 24,  86 => 23,  82 => 22,  78 => 21,  72 => 20,  69 => 19,  65 => 18,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Posts list</h1>

    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Content</th>
                <th>Creationdate</th>
                <th>Modificationdate</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for post in posts %}
            <tr>
                <td><a href=\"{{ path('post_show', { 'id': post.id }) }}\">{{ post.id }}</a></td>
                <td>{{ post.title }}</td>
                <td>{{ post.content }}</td>
                <td>{% if post.creationDate %}{{ post.creationDate|date('Y-m-d H:i:s') }}{% endif %}</td>
                <td>{% if post.modificationDate %}{{ post.modificationDate|date('Y-m-d H:i:s') }}{% endif %}</td>
                <td>
                    <ul>
                        <li>
                            <a href=\"{{ path('post_show', { 'id': post.id }) }}\">show</a>
                        </li>
                        <li>
                            <a href=\"{{ path('post_edit', { 'id': post.id }) }}\">edit</a>
                        </li>
                    </ul>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('post_new') }}\">Create a new post</a>
        </li>
    </ul>
{% endblock %}
", ":post:index.html.twig", "/home/elie/Documents/rendu/PHP/pool_symfony/test/app/Resources/views/post/index.html.twig");
    }
}
