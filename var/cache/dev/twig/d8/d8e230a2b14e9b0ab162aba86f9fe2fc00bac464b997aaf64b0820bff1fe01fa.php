<?php

/* WebProfilerBundle:Profiler:open.html.twig */
class __TwigTemplate_e4e61d1ca61e041a7465b70c76386d2ba013a7f12b8eed265777b2060f5d44d3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/base.html.twig", "WebProfilerBundle:Profiler:open.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f8e6c8fea2e3785ad6d56ed72242c0c2c468de7238ad3e8d9c243ca144d7ead1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f8e6c8fea2e3785ad6d56ed72242c0c2c468de7238ad3e8d9c243ca144d7ead1->enter($__internal_f8e6c8fea2e3785ad6d56ed72242c0c2c468de7238ad3e8d9c243ca144d7ead1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $__internal_6df1b9f05e0da7f65a4a0f03fc5e5ff26574d3e20f19f7120732f4b390bef83f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6df1b9f05e0da7f65a4a0f03fc5e5ff26574d3e20f19f7120732f4b390bef83f->enter($__internal_6df1b9f05e0da7f65a4a0f03fc5e5ff26574d3e20f19f7120732f4b390bef83f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:open.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f8e6c8fea2e3785ad6d56ed72242c0c2c468de7238ad3e8d9c243ca144d7ead1->leave($__internal_f8e6c8fea2e3785ad6d56ed72242c0c2c468de7238ad3e8d9c243ca144d7ead1_prof);

        
        $__internal_6df1b9f05e0da7f65a4a0f03fc5e5ff26574d3e20f19f7120732f4b390bef83f->leave($__internal_6df1b9f05e0da7f65a4a0f03fc5e5ff26574d3e20f19f7120732f4b390bef83f_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_28837f79a94fb903e52233568295c7c5e5ee73a2f0c3b663fdc9b4658865960e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_28837f79a94fb903e52233568295c7c5e5ee73a2f0c3b663fdc9b4658865960e->enter($__internal_28837f79a94fb903e52233568295c7c5e5ee73a2f0c3b663fdc9b4658865960e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_1f6ca11b8b4c7a8ac48d2c6cba05a6db90ff8c24b9a6d799cac63672731a0cc8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1f6ca11b8b4c7a8ac48d2c6cba05a6db90ff8c24b9a6d799cac63672731a0cc8->enter($__internal_1f6ca11b8b4c7a8ac48d2c6cba05a6db90ff8c24b9a6d799cac63672731a0cc8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        ";
        // line 5
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/open.css.twig");
        echo "
    </style>
";
        
        $__internal_1f6ca11b8b4c7a8ac48d2c6cba05a6db90ff8c24b9a6d799cac63672731a0cc8->leave($__internal_1f6ca11b8b4c7a8ac48d2c6cba05a6db90ff8c24b9a6d799cac63672731a0cc8_prof);

        
        $__internal_28837f79a94fb903e52233568295c7c5e5ee73a2f0c3b663fdc9b4658865960e->leave($__internal_28837f79a94fb903e52233568295c7c5e5ee73a2f0c3b663fdc9b4658865960e_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_4f4cb50f9ec2053bd489f04368c08514d6ab5d46a5258e4d2125ee8aebbe35e6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4f4cb50f9ec2053bd489f04368c08514d6ab5d46a5258e4d2125ee8aebbe35e6->enter($__internal_4f4cb50f9ec2053bd489f04368c08514d6ab5d46a5258e4d2125ee8aebbe35e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_13fe725899e2f62e6b509feb9001b532b3b95a85699faa673db9328fb9d3f4b1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_13fe725899e2f62e6b509feb9001b532b3b95a85699faa673db9328fb9d3f4b1->enter($__internal_13fe725899e2f62e6b509feb9001b532b3b95a85699faa673db9328fb9d3f4b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "<div class=\"header\">
    <h1>";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["file"]) || array_key_exists("file", $context) ? $context["file"] : (function () { throw new Twig_Error_Runtime('Variable "file" does not exist.', 11, $this->getSourceContext()); })()), "html", null, true);
        echo " <small>line ";
        echo twig_escape_filter($this->env, (isset($context["line"]) || array_key_exists("line", $context) ? $context["line"] : (function () { throw new Twig_Error_Runtime('Variable "line" does not exist.', 11, $this->getSourceContext()); })()), "html", null, true);
        echo "</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/";
        // line 12
        echo twig_escape_filter($this->env, twig_constant("Symfony\\Component\\HttpKernel\\Kernel::VERSION"), "html", null, true);
        echo "/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    ";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\CodeExtension')->fileExcerpt((isset($context["filename"]) || array_key_exists("filename", $context) ? $context["filename"] : (function () { throw new Twig_Error_Runtime('Variable "filename" does not exist.', 15, $this->getSourceContext()); })()), (isset($context["line"]) || array_key_exists("line", $context) ? $context["line"] : (function () { throw new Twig_Error_Runtime('Variable "line" does not exist.', 15, $this->getSourceContext()); })()),  -1);
        echo "
</div>
";
        
        $__internal_13fe725899e2f62e6b509feb9001b532b3b95a85699faa673db9328fb9d3f4b1->leave($__internal_13fe725899e2f62e6b509feb9001b532b3b95a85699faa673db9328fb9d3f4b1_prof);

        
        $__internal_4f4cb50f9ec2053bd489f04368c08514d6ab5d46a5258e4d2125ee8aebbe35e6->leave($__internal_4f4cb50f9ec2053bd489f04368c08514d6ab5d46a5258e4d2125ee8aebbe35e6_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:open.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 15,  84 => 12,  78 => 11,  75 => 10,  66 => 9,  53 => 5,  50 => 4,  41 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/base.html.twig' %}

{% block head %}
    <style>
        {{ include('@WebProfiler/Profiler/open.css.twig') }}
    </style>
{% endblock %}

{% block body %}
<div class=\"header\">
    <h1>{{ file }} <small>line {{ line }}</small></h1>
    <a class=\"doc\" href=\"https://symfony.com/doc/{{ constant('Symfony\\\\Component\\\\HttpKernel\\\\Kernel::VERSION') }}/reference/configuration/framework.html#ide\" rel=\"help\">Open in your IDE?</a>
</div>
<div class=\"source\">
    {{ filename|file_excerpt(line, -1) }}
</div>
{% endblock %}
", "WebProfilerBundle:Profiler:open.html.twig", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/open.html.twig");
    }
}
