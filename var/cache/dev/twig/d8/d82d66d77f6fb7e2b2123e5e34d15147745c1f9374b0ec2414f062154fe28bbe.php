<?php

/* :post:edit.html.twig */
class __TwigTemplate_84da487381d0207575f3aa25e737f76d3ab793a08ea4bc045e021db48fdb96a7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":post:edit.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e458703df8831c0aacd9a33a7524950a4a9a983155004a58d811e24d239694b9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e458703df8831c0aacd9a33a7524950a4a9a983155004a58d811e24d239694b9->enter($__internal_e458703df8831c0aacd9a33a7524950a4a9a983155004a58d811e24d239694b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":post:edit.html.twig"));

        $__internal_4c587ac6210e58b991fc3ae38d4a87cc9e91f66538a2ebf514ac8072e6a30a59 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4c587ac6210e58b991fc3ae38d4a87cc9e91f66538a2ebf514ac8072e6a30a59->enter($__internal_4c587ac6210e58b991fc3ae38d4a87cc9e91f66538a2ebf514ac8072e6a30a59_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":post:edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e458703df8831c0aacd9a33a7524950a4a9a983155004a58d811e24d239694b9->leave($__internal_e458703df8831c0aacd9a33a7524950a4a9a983155004a58d811e24d239694b9_prof);

        
        $__internal_4c587ac6210e58b991fc3ae38d4a87cc9e91f66538a2ebf514ac8072e6a30a59->leave($__internal_4c587ac6210e58b991fc3ae38d4a87cc9e91f66538a2ebf514ac8072e6a30a59_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_c423018abec0480631b3ae68b94f6fc7df074894ed4ee49a7d19d92e027a476e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c423018abec0480631b3ae68b94f6fc7df074894ed4ee49a7d19d92e027a476e->enter($__internal_c423018abec0480631b3ae68b94f6fc7df074894ed4ee49a7d19d92e027a476e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_1f9c5c8b4a6eea92e09f507b5703049f5305fd2d72e1cb345ee0c5ffd6ac7939 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1f9c5c8b4a6eea92e09f507b5703049f5305fd2d72e1cb345ee0c5ffd6ac7939->enter($__internal_1f9c5c8b4a6eea92e09f507b5703049f5305fd2d72e1cb345ee0c5ffd6ac7939_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Post edit</h1>

    ";
        // line 6
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["edit_form"]) || array_key_exists("edit_form", $context) ? $context["edit_form"] : (function () { throw new Twig_Error_Runtime('Variable "edit_form" does not exist.', 6, $this->getSourceContext()); })()), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["edit_form"]) || array_key_exists("edit_form", $context) ? $context["edit_form"] : (function () { throw new Twig_Error_Runtime('Variable "edit_form" does not exist.', 7, $this->getSourceContext()); })()), 'widget');
        echo "
        <input type=\"submit\" value=\"Edit\" />
    ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["edit_form"]) || array_key_exists("edit_form", $context) ? $context["edit_form"] : (function () { throw new Twig_Error_Runtime('Variable "edit_form" does not exist.', 9, $this->getSourceContext()); })()), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("post_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            ";
        // line 16
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["delete_form"]) || array_key_exists("delete_form", $context) ? $context["delete_form"] : (function () { throw new Twig_Error_Runtime('Variable "delete_form" does not exist.', 16, $this->getSourceContext()); })()), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 18
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["delete_form"]) || array_key_exists("delete_form", $context) ? $context["delete_form"] : (function () { throw new Twig_Error_Runtime('Variable "delete_form" does not exist.', 18, $this->getSourceContext()); })()), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_1f9c5c8b4a6eea92e09f507b5703049f5305fd2d72e1cb345ee0c5ffd6ac7939->leave($__internal_1f9c5c8b4a6eea92e09f507b5703049f5305fd2d72e1cb345ee0c5ffd6ac7939_prof);

        
        $__internal_c423018abec0480631b3ae68b94f6fc7df074894ed4ee49a7d19d92e027a476e->leave($__internal_c423018abec0480631b3ae68b94f6fc7df074894ed4ee49a7d19d92e027a476e_prof);

    }

    public function getTemplateName()
    {
        return ":post:edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 18,  75 => 16,  69 => 13,  62 => 9,  57 => 7,  53 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Post edit</h1>

    {{ form_start(edit_form) }}
        {{ form_widget(edit_form) }}
        <input type=\"submit\" value=\"Edit\" />
    {{ form_end(edit_form) }}

    <ul>
        <li>
            <a href=\"{{ path('post_index') }}\">Back to the list</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", ":post:edit.html.twig", "/home/elie/Documents/rendu/PHP/pool_symfony/test/app/Resources/views/post/edit.html.twig");
    }
}
