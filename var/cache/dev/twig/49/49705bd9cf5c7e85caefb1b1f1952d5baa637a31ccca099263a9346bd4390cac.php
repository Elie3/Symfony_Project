<?php

/* TwigBundle:Exception:error.css.twig */
class __TwigTemplate_76404c5fabb4d4179e1c5abe01233cef35360e427487fba53ccbfed17d5cb66c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_93e06ce81b2dd381bef774c4b70b45fec9954682d4569ad861623fce33a371a7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_93e06ce81b2dd381bef774c4b70b45fec9954682d4569ad861623fce33a371a7->enter($__internal_93e06ce81b2dd381bef774c4b70b45fec9954682d4569ad861623fce33a371a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        $__internal_55f24c991489f8c0c00cab82b82582ab0a83fbfe6ccc5c647bd5010d0b78f9e7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_55f24c991489f8c0c00cab82b82582ab0a83fbfe6ccc5c647bd5010d0b78f9e7->enter($__internal_55f24c991489f8c0c00cab82b82582ab0a83fbfe6ccc5c647bd5010d0b78f9e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) || array_key_exists("status_code", $context) ? $context["status_code"] : (function () { throw new Twig_Error_Runtime('Variable "status_code" does not exist.', 2, $this->getSourceContext()); })()), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) || array_key_exists("status_text", $context) ? $context["status_text"] : (function () { throw new Twig_Error_Runtime('Variable "status_text" does not exist.', 2, $this->getSourceContext()); })()), "css", null, true);
        echo "

*/
";
        
        $__internal_93e06ce81b2dd381bef774c4b70b45fec9954682d4569ad861623fce33a371a7->leave($__internal_93e06ce81b2dd381bef774c4b70b45fec9954682d4569ad861623fce33a371a7_prof);

        
        $__internal_55f24c991489f8c0c00cab82b82582ab0a83fbfe6ccc5c647bd5010d0b78f9e7->leave($__internal_55f24c991489f8c0c00cab82b82582ab0a83fbfe6ccc5c647bd5010d0b78f9e7_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ status_code }} {{ status_text }}

*/
", "TwigBundle:Exception:error.css.twig", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.css.twig");
    }
}
