<?php

/* @Framework/Form/datetime_widget.html.php */
class __TwigTemplate_3f307ff16ab6df60139c79bef12fd31483cc1ea795768f99f1977fc25af3de40 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_edbdfd46b4f26b3b84aa6bc4602e25c4b939baa3c3fe1c6b0a77df1f7efafeaf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_edbdfd46b4f26b3b84aa6bc4602e25c4b939baa3c3fe1c6b0a77df1f7efafeaf->enter($__internal_edbdfd46b4f26b3b84aa6bc4602e25c4b939baa3c3fe1c6b0a77df1f7efafeaf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        $__internal_32ef81cd06ea085f4a0cf875ef5efe891e598c83423e4bc9574f5a33bb6c0bee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_32ef81cd06ea085f4a0cf875ef5efe891e598c83423e4bc9574f5a33bb6c0bee->enter($__internal_32ef81cd06ea085f4a0cf875ef5efe891e598c83423e4bc9574f5a33bb6c0bee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $__internal_edbdfd46b4f26b3b84aa6bc4602e25c4b939baa3c3fe1c6b0a77df1f7efafeaf->leave($__internal_edbdfd46b4f26b3b84aa6bc4602e25c4b939baa3c3fe1c6b0a77df1f7efafeaf_prof);

        
        $__internal_32ef81cd06ea085f4a0cf875ef5efe891e598c83423e4bc9574f5a33bb6c0bee->leave($__internal_32ef81cd06ea085f4a0cf875ef5efe891e598c83423e4bc9574f5a33bb6c0bee_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetime_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
", "@Framework/Form/datetime_widget.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/datetime_widget.html.php");
    }
}
