<?php

/* @Framework/Form/form_errors.html.php */
class __TwigTemplate_f999432b5b4aa907a60a17a12299715ff89fc9bf8dd464c798caa48e50b69489 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_801c9c3000827b166b1db3250efc7ffd0342b9bcad57f091f7759a96c2a67d0b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_801c9c3000827b166b1db3250efc7ffd0342b9bcad57f091f7759a96c2a67d0b->enter($__internal_801c9c3000827b166b1db3250efc7ffd0342b9bcad57f091f7759a96c2a67d0b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        $__internal_ec943f9b0c200c60c827226a057bfbc431c92d684bdbe66644fd5e86db516198 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ec943f9b0c200c60c827226a057bfbc431c92d684bdbe66644fd5e86db516198->enter($__internal_ec943f9b0c200c60c827226a057bfbc431c92d684bdbe66644fd5e86db516198_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_errors.html.php"));

        // line 1
        echo "<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
";
        
        $__internal_801c9c3000827b166b1db3250efc7ffd0342b9bcad57f091f7759a96c2a67d0b->leave($__internal_801c9c3000827b166b1db3250efc7ffd0342b9bcad57f091f7759a96c2a67d0b_prof);

        
        $__internal_ec943f9b0c200c60c827226a057bfbc431c92d684bdbe66644fd5e86db516198->leave($__internal_ec943f9b0c200c60c827226a057bfbc431c92d684bdbe66644fd5e86db516198_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_errors.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (count(\$errors) > 0): ?>
    <ul>
        <?php foreach (\$errors as \$error): ?>
            <li><?php echo \$error->getMessage() ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif ?>
", "@Framework/Form/form_errors.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_errors.html.php");
    }
}
