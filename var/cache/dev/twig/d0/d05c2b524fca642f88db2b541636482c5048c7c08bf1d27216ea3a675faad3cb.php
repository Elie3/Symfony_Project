<?php

/* @Framework/Form/form_widget_simple.html.php */
class __TwigTemplate_256effdaa7b675172da42e74be9318a9e8674b614d11c9998742dc2b70e24f9c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ad39c14857b7672c7ed36dc8f994e2a2e67a23fb442b7b165b3583f7723683b4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ad39c14857b7672c7ed36dc8f994e2a2e67a23fb442b7b165b3583f7723683b4->enter($__internal_ad39c14857b7672c7ed36dc8f994e2a2e67a23fb442b7b165b3583f7723683b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        $__internal_b6ced4bdfac5acf50e038b2d244276fdb7a386b5734653520fbc85ed20fa188b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b6ced4bdfac5acf50e038b2d244276fdb7a386b5734653520fbc85ed20fa188b->enter($__internal_b6ced4bdfac5acf50e038b2d244276fdb7a386b5734653520fbc85ed20fa188b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        // line 1
        echo "<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
";
        
        $__internal_ad39c14857b7672c7ed36dc8f994e2a2e67a23fb442b7b165b3583f7723683b4->leave($__internal_ad39c14857b7672c7ed36dc8f994e2a2e67a23fb442b7b165b3583f7723683b4_prof);

        
        $__internal_b6ced4bdfac5acf50e038b2d244276fdb7a386b5734653520fbc85ed20fa188b->leave($__internal_b6ced4bdfac5acf50e038b2d244276fdb7a386b5734653520fbc85ed20fa188b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_simple.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
", "@Framework/Form/form_widget_simple.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget_simple.html.php");
    }
}
