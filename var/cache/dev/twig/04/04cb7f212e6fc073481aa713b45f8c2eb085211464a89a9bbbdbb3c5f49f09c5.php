<?php

/* @Framework/Form/checkbox_widget.html.php */
class __TwigTemplate_58263f4e03d50c5cf9878f99efd29918963ed525153142cba9be12fb135021f6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cf794446beabff0c77e41ae4867a6f50ff1e4feb7dfcbd07a663090071e1f003 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cf794446beabff0c77e41ae4867a6f50ff1e4feb7dfcbd07a663090071e1f003->enter($__internal_cf794446beabff0c77e41ae4867a6f50ff1e4feb7dfcbd07a663090071e1f003_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        $__internal_252f1db62d0cf036104a1581bddf3f1a6b0b21d9764e9491e03b76e28b678621 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_252f1db62d0cf036104a1581bddf3f1a6b0b21d9764e9491e03b76e28b678621->enter($__internal_252f1db62d0cf036104a1581bddf3f1a6b0b21d9764e9491e03b76e28b678621_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/checkbox_widget.html.php"));

        // line 1
        echo "<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_cf794446beabff0c77e41ae4867a6f50ff1e4feb7dfcbd07a663090071e1f003->leave($__internal_cf794446beabff0c77e41ae4867a6f50ff1e4feb7dfcbd07a663090071e1f003_prof);

        
        $__internal_252f1db62d0cf036104a1581bddf3f1a6b0b21d9764e9491e03b76e28b678621->leave($__internal_252f1db62d0cf036104a1581bddf3f1a6b0b21d9764e9491e03b76e28b678621_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/checkbox_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"checkbox\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    <?php if (strlen(\$value) > 0): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?>
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/checkbox_widget.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/checkbox_widget.html.php");
    }
}
