<?php

/* @Framework/Form/form_widget_compound.html.php */
class __TwigTemplate_185ba3fe692def0904203c1c72ec9cdbb468db6c6f9883824b84cab1e9f90b74 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b44344aeb67fccf2341cbc156bd38c0ca92b3ef3bbbd7ca272f312b67b614bc1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b44344aeb67fccf2341cbc156bd38c0ca92b3ef3bbbd7ca272f312b67b614bc1->enter($__internal_b44344aeb67fccf2341cbc156bd38c0ca92b3ef3bbbd7ca272f312b67b614bc1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        $__internal_b02d7815e396796050f8cd97b2a133290b60a2ee9380a6fad655d383895c4988 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b02d7815e396796050f8cd97b2a133290b60a2ee9380a6fad655d383895c4988->enter($__internal_b02d7815e396796050f8cd97b2a133290b60a2ee9380a6fad655d383895c4988_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $__internal_b44344aeb67fccf2341cbc156bd38c0ca92b3ef3bbbd7ca272f312b67b614bc1->leave($__internal_b44344aeb67fccf2341cbc156bd38c0ca92b3ef3bbbd7ca272f312b67b614bc1_prof);

        
        $__internal_b02d7815e396796050f8cd97b2a133290b60a2ee9380a6fad655d383895c4988->leave($__internal_b02d7815e396796050f8cd97b2a133290b60a2ee9380a6fad655d383895c4988_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
", "@Framework/Form/form_widget_compound.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget_compound.html.php");
    }
}
