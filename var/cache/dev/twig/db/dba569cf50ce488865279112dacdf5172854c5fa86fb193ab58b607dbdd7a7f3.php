<?php

/* @WebProfiler/Icon/forward.svg */
class __TwigTemplate_75302b545ddaece716759644ece977ffbf925584953d2f88ca6559cdde8fc21b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_87997c486c0b9126fbf27bb4d9fb8b2904d13ea150c3bff03367188026b497da = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_87997c486c0b9126fbf27bb4d9fb8b2904d13ea150c3bff03367188026b497da->enter($__internal_87997c486c0b9126fbf27bb4d9fb8b2904d13ea150c3bff03367188026b497da_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/forward.svg"));

        $__internal_819dddb66486f11724d45f2011d483a1eab2a353b04d7984cc1c6a6239fe21bd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_819dddb66486f11724d45f2011d483a1eab2a353b04d7984cc1c6a6239fe21bd->enter($__internal_819dddb66486f11724d45f2011d483a1eab2a353b04d7984cc1c6a6239fe21bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/forward.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path style=\"fill:#aaa\" d=\"M23.61,11.07L17.07,4.35A1.2,1.2,0,0,0,15,5.28V9H1.4A1.82,1.82,0,0,0,0,10.82v2.61A1.55,
        1.55,0,0,0,1.4,15H15v3.72a1.2,1.2,0,0,0,2.07.93l6.63-6.72A1.32,1.32,0,0,0,23.61,11.07Z\"/>
</svg>
";
        
        $__internal_87997c486c0b9126fbf27bb4d9fb8b2904d13ea150c3bff03367188026b497da->leave($__internal_87997c486c0b9126fbf27bb4d9fb8b2904d13ea150c3bff03367188026b497da_prof);

        
        $__internal_819dddb66486f11724d45f2011d483a1eab2a353b04d7984cc1c6a6239fe21bd->leave($__internal_819dddb66486f11724d45f2011d483a1eab2a353b04d7984cc1c6a6239fe21bd_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/forward.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path style=\"fill:#aaa\" d=\"M23.61,11.07L17.07,4.35A1.2,1.2,0,0,0,15,5.28V9H1.4A1.82,1.82,0,0,0,0,10.82v2.61A1.55,
        1.55,0,0,0,1.4,15H15v3.72a1.2,1.2,0,0,0,2.07.93l6.63-6.72A1.32,1.32,0,0,0,23.61,11.07Z\"/>
</svg>
", "@WebProfiler/Icon/forward.svg", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Icon/forward.svg");
    }
}
