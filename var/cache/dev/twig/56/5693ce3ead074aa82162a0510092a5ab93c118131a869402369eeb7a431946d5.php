<?php

/* @WebProfiler/Icon/memory.svg */
class __TwigTemplate_5d0e5ab0511a07b4fa7d06a09e8a3c6fe4bcf45480e01e0be7d935422913a21a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_acd2c56a102a8b04c5f5b76791d472beb2068742d3a9e49f75585c0f68e6988e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_acd2c56a102a8b04c5f5b76791d472beb2068742d3a9e49f75585c0f68e6988e->enter($__internal_acd2c56a102a8b04c5f5b76791d472beb2068742d3a9e49f75585c0f68e6988e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/memory.svg"));

        $__internal_ade5a4d14ceab9cb5d111561784dd7362f0e8947815cab577d0f4c4c55acc959 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ade5a4d14ceab9cb5d111561784dd7362f0e8947815cab577d0f4c4c55acc959->enter($__internal_ade5a4d14ceab9cb5d111561784dd7362f0e8947815cab577d0f4c4c55acc959_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/memory.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M6,18.9V15h12v3.9c0,0.7-0.2,1.1-1,1.1H7C6.2,20,6,19.6,6,18.9z M20,1C20,1,20,1,20,1c-0.6,0-1,0.5-1,1.1
    l0,18c0,0.5-0.4,0.9-0.9,0.9H5.9C5.4,21,5,20.6,5,20.1l0-18C5,1.5,4.6,1,4,1c0,0,0,0,0,0C3.5,1,3,1.5,3,2.1l0,18
    C3,21.7,4.3,23,5.9,23h12.2c1.6,0,2.9-1.3,2.9-2.9l0-18C21,1.5,20.6,1,20,1z M18,9H6v5h12V9z\"/>
</svg>
";
        
        $__internal_acd2c56a102a8b04c5f5b76791d472beb2068742d3a9e49f75585c0f68e6988e->leave($__internal_acd2c56a102a8b04c5f5b76791d472beb2068742d3a9e49f75585c0f68e6988e_prof);

        
        $__internal_ade5a4d14ceab9cb5d111561784dd7362f0e8947815cab577d0f4c4c55acc959->leave($__internal_ade5a4d14ceab9cb5d111561784dd7362f0e8947815cab577d0f4c4c55acc959_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/memory.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
<path fill=\"#AAAAAA\" d=\"M6,18.9V15h12v3.9c0,0.7-0.2,1.1-1,1.1H7C6.2,20,6,19.6,6,18.9z M20,1C20,1,20,1,20,1c-0.6,0-1,0.5-1,1.1
    l0,18c0,0.5-0.4,0.9-0.9,0.9H5.9C5.4,21,5,20.6,5,20.1l0-18C5,1.5,4.6,1,4,1c0,0,0,0,0,0C3.5,1,3,1.5,3,2.1l0,18
    C3,21.7,4.3,23,5.9,23h12.2c1.6,0,2.9-1.3,2.9-2.9l0-18C21,1.5,20.6,1,20,1z M18,9H6v5h12V9z\"/>
</svg>
", "@WebProfiler/Icon/memory.svg", "/home/elie/Gitlab/Symfony_Project/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Icon/memory.svg");
    }
}
