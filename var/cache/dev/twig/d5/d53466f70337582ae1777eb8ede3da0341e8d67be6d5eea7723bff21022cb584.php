<?php

/* @Twig/layout.html.twig */
class __TwigTemplate_088e3193ef70bb6bafbdb5b67faf1b58e27d448c89ca0ff9004d8adcb50a3518 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5ee660a53aba13f8db575c3768bcf14ed5d34fda8506be271dedc2aaaf852559 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5ee660a53aba13f8db575c3768bcf14ed5d34fda8506be271dedc2aaaf852559->enter($__internal_5ee660a53aba13f8db575c3768bcf14ed5d34fda8506be271dedc2aaaf852559_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        $__internal_25959f90542dcbdcdcf7fdc26d6d171e4d3a0d4bd3d3cf0444d60bbaceebc5fe = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_25959f90542dcbdcdcf7fdc26d6d171e4d3a0d4bd3d3cf0444d60bbaceebc5fe->enter($__internal_25959f90542dcbdcdcf7fdc26d6d171e4d3a0d4bd3d3cf0444d60bbaceebc5fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_5ee660a53aba13f8db575c3768bcf14ed5d34fda8506be271dedc2aaaf852559->leave($__internal_5ee660a53aba13f8db575c3768bcf14ed5d34fda8506be271dedc2aaaf852559_prof);

        
        $__internal_25959f90542dcbdcdcf7fdc26d6d171e4d3a0d4bd3d3cf0444d60bbaceebc5fe->leave($__internal_25959f90542dcbdcdcf7fdc26d6d171e4d3a0d4bd3d3cf0444d60bbaceebc5fe_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_be8503174a68fc99a8c1b48772d7b032fcbae472f5df2f50efe0ca569156b490 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_be8503174a68fc99a8c1b48772d7b032fcbae472f5df2f50efe0ca569156b490->enter($__internal_be8503174a68fc99a8c1b48772d7b032fcbae472f5df2f50efe0ca569156b490_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_358fe91b03fc64e00d2918d4b9a85b54c9df6dce1a06323ff1ba346a5e3384ee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_358fe91b03fc64e00d2918d4b9a85b54c9df6dce1a06323ff1ba346a5e3384ee->enter($__internal_358fe91b03fc64e00d2918d4b9a85b54c9df6dce1a06323ff1ba346a5e3384ee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_358fe91b03fc64e00d2918d4b9a85b54c9df6dce1a06323ff1ba346a5e3384ee->leave($__internal_358fe91b03fc64e00d2918d4b9a85b54c9df6dce1a06323ff1ba346a5e3384ee_prof);

        
        $__internal_be8503174a68fc99a8c1b48772d7b032fcbae472f5df2f50efe0ca569156b490->leave($__internal_be8503174a68fc99a8c1b48772d7b032fcbae472f5df2f50efe0ca569156b490_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_0ca2003b9bb70930894ec3b5ea939a34e4c4a04c899d63000f3e7b9b6a37991c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0ca2003b9bb70930894ec3b5ea939a34e4c4a04c899d63000f3e7b9b6a37991c->enter($__internal_0ca2003b9bb70930894ec3b5ea939a34e4c4a04c899d63000f3e7b9b6a37991c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_685125b72ef813f2ea95fddd17728eb0be38f011cd4993923c9c57c54b18ee81 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_685125b72ef813f2ea95fddd17728eb0be38f011cd4993923c9c57c54b18ee81->enter($__internal_685125b72ef813f2ea95fddd17728eb0be38f011cd4993923c9c57c54b18ee81_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_685125b72ef813f2ea95fddd17728eb0be38f011cd4993923c9c57c54b18ee81->leave($__internal_685125b72ef813f2ea95fddd17728eb0be38f011cd4993923c9c57c54b18ee81_prof);

        
        $__internal_0ca2003b9bb70930894ec3b5ea939a34e4c4a04c899d63000f3e7b9b6a37991c->leave($__internal_0ca2003b9bb70930894ec3b5ea939a34e4c4a04c899d63000f3e7b9b6a37991c_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_e0bbb3aa4da5d94fb7292f02db6c13fa1715cc72bb023cff750c3b1fd586991d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e0bbb3aa4da5d94fb7292f02db6c13fa1715cc72bb023cff750c3b1fd586991d->enter($__internal_e0bbb3aa4da5d94fb7292f02db6c13fa1715cc72bb023cff750c3b1fd586991d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_dc082e2ca8e7c96a1996ac2eeff77d3050e9aa1eceaf65e83707e0f8d8e71e8b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dc082e2ca8e7c96a1996ac2eeff77d3050e9aa1eceaf65e83707e0f8d8e71e8b->enter($__internal_dc082e2ca8e7c96a1996ac2eeff77d3050e9aa1eceaf65e83707e0f8d8e71e8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_dc082e2ca8e7c96a1996ac2eeff77d3050e9aa1eceaf65e83707e0f8d8e71e8b->leave($__internal_dc082e2ca8e7c96a1996ac2eeff77d3050e9aa1eceaf65e83707e0f8d8e71e8b_prof);

        
        $__internal_e0bbb3aa4da5d94fb7292f02db6c13fa1715cc72bb023cff750c3b1fd586991d->leave($__internal_e0bbb3aa4da5d94fb7292f02db6c13fa1715cc72bb023cff750c3b1fd586991d_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "@Twig/layout.html.twig", "/home/elie/Gitlab/Symfony_Project/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/layout.html.twig");
    }
}
