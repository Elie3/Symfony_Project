<?php

/* @Framework/Form/choice_widget_expanded.html.php */
class __TwigTemplate_681119f560756d0910b80b0ed4c59c2aac9518ee3f404aa92ab2a8759637c574 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4866454d0a202bf46bd2de6220793b1d33fb4ee787f38d23b2c5b76f7d10773e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4866454d0a202bf46bd2de6220793b1d33fb4ee787f38d23b2c5b76f7d10773e->enter($__internal_4866454d0a202bf46bd2de6220793b1d33fb4ee787f38d23b2c5b76f7d10773e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        $__internal_41bda1d791ab3f4eff5af86b1b4239807d952e99719af45e5b162ce075d82ef4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_41bda1d791ab3f4eff5af86b1b4239807d952e99719af45e5b162ce075d82ef4->enter($__internal_41bda1d791ab3f4eff5af86b1b4239807d952e99719af45e5b162ce075d82ef4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
";
        
        $__internal_4866454d0a202bf46bd2de6220793b1d33fb4ee787f38d23b2c5b76f7d10773e->leave($__internal_4866454d0a202bf46bd2de6220793b1d33fb4ee787f38d23b2c5b76f7d10773e_prof);

        
        $__internal_41bda1d791ab3f4eff5af86b1b4239807d952e99719af45e5b162ce075d82ef4->leave($__internal_41bda1d791ab3f4eff5af86b1b4239807d952e99719af45e5b162ce075d82ef4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget_expanded.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
", "@Framework/Form/choice_widget_expanded.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_widget_expanded.html.php");
    }
}
