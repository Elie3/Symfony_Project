<?php

/* PlatypusBundle:Post:posts.html.twig */
class __TwigTemplate_f0e0eb6ee5dfb98202bf272653e566b632940e33811c26a97a4ab3e1581fde93 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "PlatypusBundle:Post:posts.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_087a841a9c5b5486b2e9e2b3b2bc1f69c102a04e399dba499477c8fa2bdd902b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_087a841a9c5b5486b2e9e2b3b2bc1f69c102a04e399dba499477c8fa2bdd902b->enter($__internal_087a841a9c5b5486b2e9e2b3b2bc1f69c102a04e399dba499477c8fa2bdd902b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:Post:posts.html.twig"));

        $__internal_9b2a7a0e9aea2c3a4e78bb7b8bc246d214e647d25e77abc5c2fd7d94f7db847a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9b2a7a0e9aea2c3a4e78bb7b8bc246d214e647d25e77abc5c2fd7d94f7db847a->enter($__internal_9b2a7a0e9aea2c3a4e78bb7b8bc246d214e647d25e77abc5c2fd7d94f7db847a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:Post:posts.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_087a841a9c5b5486b2e9e2b3b2bc1f69c102a04e399dba499477c8fa2bdd902b->leave($__internal_087a841a9c5b5486b2e9e2b3b2bc1f69c102a04e399dba499477c8fa2bdd902b_prof);

        
        $__internal_9b2a7a0e9aea2c3a4e78bb7b8bc246d214e647d25e77abc5c2fd7d94f7db847a->leave($__internal_9b2a7a0e9aea2c3a4e78bb7b8bc246d214e647d25e77abc5c2fd7d94f7db847a_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_28a80f2d0c1ab505ef1717384afc6ddb4c5e00717ebc9abe3f11bc2174b2ee75 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_28a80f2d0c1ab505ef1717384afc6ddb4c5e00717ebc9abe3f11bc2174b2ee75->enter($__internal_28a80f2d0c1ab505ef1717384afc6ddb4c5e00717ebc9abe3f11bc2174b2ee75_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_d1d475ba88f9b8ba7b6027883b7a2c341f39c7fe6b1fca3e609f474b4e58e4ff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d1d475ba88f9b8ba7b6027883b7a2c341f39c7fe6b1fca3e609f474b4e58e4ff->enter($__internal_d1d475ba88f9b8ba7b6027883b7a2c341f39c7fe6b1fca3e609f474b4e58e4ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("style.css"), "html", null, true);
        echo "\"/>
<table>

<tr><td>Id</td><td>Title</td><td>Content</td><td>Creation Date</td><td>Modification Date</td></tr>
";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new Twig_Error_Runtime('Variable "posts" does not exist.', 9, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["elem"]) {
            // line 10
            echo "<tr>
<td>";
            // line 11
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "id", array()), "html", null, true);
            echo "</td>
<td>";
            // line 12
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "title", array()), "html", null, true);
            echo "</td>
<td>";
            // line 13
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "content", array()), "html", null, true);
            echo "</td>
<td>";
            // line 14
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "creationDate", array()), "Y-m-d"), "html", null, true);
            echo "</td>
<td>";
            // line 15
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "modificationDate", array()), "Y-m-d"), "html", null, true);
            echo "</td>
</tr>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['elem'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "
<table>
<p><a href=\"";
        // line 21
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_new_post");
        echo "\">Create a new Post</a></p>




";
        
        $__internal_d1d475ba88f9b8ba7b6027883b7a2c341f39c7fe6b1fca3e609f474b4e58e4ff->leave($__internal_d1d475ba88f9b8ba7b6027883b7a2c341f39c7fe6b1fca3e609f474b4e58e4ff_prof);

        
        $__internal_28a80f2d0c1ab505ef1717384afc6ddb4c5e00717ebc9abe3f11bc2174b2ee75->leave($__internal_28a80f2d0c1ab505ef1717384afc6ddb4c5e00717ebc9abe3f11bc2174b2ee75_prof);

    }

    public function getTemplateName()
    {
        return "PlatypusBundle:Post:posts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 21,  92 => 19,  82 => 15,  78 => 14,  74 => 13,  70 => 12,  66 => 11,  63 => 10,  59 => 9,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"base.html.twig\" %}

{% block body%}

<link rel=\"stylesheet\" href=\"{{ asset('style.css')}}\"/>
<table>

<tr><td>Id</td><td>Title</td><td>Content</td><td>Creation Date</td><td>Modification Date</td></tr>
{% for elem in posts%}
<tr>
<td>{{elem.id}}</td>
<td>{{elem.title}}</td>
<td>{{elem.content}}</td>
<td>{{elem.creationDate|date('Y-m-d')}}</td>
<td>{{elem.modificationDate|date('Y-m-d')}}</td>
</tr>

{%endfor%}

<table>
<p><a href=\"{{ path('create_new_post') }}\">Create a new Post</a></p>




{% endblock %}
", "PlatypusBundle:Post:posts.html.twig", "/home/elie/Documents/rendu/PHP/pool_symfony/test/src/PlatypusBundle/Resources/views/Post/posts.html.twig");
    }
}
