<?php

/* @Framework/Form/radio_widget.html.php */
class __TwigTemplate_8a0986d5bf5598ade5a5a6c4687dc086a0057bb31dd1865929869549ff5d4b0d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4e7255af8befb7bac5c5440ee57e7d80733f9542639785e2ce8559a8d36d2e99 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4e7255af8befb7bac5c5440ee57e7d80733f9542639785e2ce8559a8d36d2e99->enter($__internal_4e7255af8befb7bac5c5440ee57e7d80733f9542639785e2ce8559a8d36d2e99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        $__internal_925987e25699fa358d93f7f995ca1f83d62470a3de56825ab93d454d8fe84d4b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_925987e25699fa358d93f7f995ca1f83d62470a3de56825ab93d454d8fe84d4b->enter($__internal_925987e25699fa358d93f7f995ca1f83d62470a3de56825ab93d454d8fe84d4b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/radio_widget.html.php"));

        // line 1
        echo "<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
";
        
        $__internal_4e7255af8befb7bac5c5440ee57e7d80733f9542639785e2ce8559a8d36d2e99->leave($__internal_4e7255af8befb7bac5c5440ee57e7d80733f9542639785e2ce8559a8d36d2e99_prof);

        
        $__internal_925987e25699fa358d93f7f995ca1f83d62470a3de56825ab93d454d8fe84d4b->leave($__internal_925987e25699fa358d93f7f995ca1f83d62470a3de56825ab93d454d8fe84d4b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/radio_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"radio\"
    <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
    value=\"<?php echo \$view->escape(\$value) ?>\"
    <?php if (\$checked): ?> checked=\"checked\"<?php endif ?>
/>
", "@Framework/Form/radio_widget.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/radio_widget.html.php");
    }
}
