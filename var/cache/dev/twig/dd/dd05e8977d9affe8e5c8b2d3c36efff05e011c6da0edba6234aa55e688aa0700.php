<?php

/* base.html.twig */
class __TwigTemplate_33be6c8e5f7a4389dca3ebff338ccae7177f681b991500a44cfde90a719ce42a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_01d6f224501192f308232ee35378fa2d16e814922b600c1d6c64a6d13019d26d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_01d6f224501192f308232ee35378fa2d16e814922b600c1d6c64a6d13019d26d->enter($__internal_01d6f224501192f308232ee35378fa2d16e814922b600c1d6c64a6d13019d26d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        $__internal_9f560ffac1d2091d135c692b5de2c5c906a2914f05be813c12595aa616cc1005 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9f560ffac1d2091d135c692b5de2c5c906a2914f05be813c12595aa616cc1005->enter($__internal_9f560ffac1d2091d135c692b5de2c5c906a2914f05be813c12595aa616cc1005_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
<head>


  <meta charset=\"UTF-8\" />
  <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
  ";
        // line 8
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 9
        echo "  <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
</head>
<body>
  ";
        // line 12
        $this->loadTemplate("PlatypusBundle:Default:header.html.twig", "base.html.twig", 12)->display($context);
        // line 13
        echo "  ";
        $this->displayBlock('body', $context, $blocks);
        // line 14
        echo "  ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 15
        echo "  ";
        $this->loadTemplate("PlatypusBundle:Default:footer.html.twig", "base.html.twig", 15)->display($context);
        // line 16
        echo "
</body>
</html>
";
        
        $__internal_01d6f224501192f308232ee35378fa2d16e814922b600c1d6c64a6d13019d26d->leave($__internal_01d6f224501192f308232ee35378fa2d16e814922b600c1d6c64a6d13019d26d_prof);

        
        $__internal_9f560ffac1d2091d135c692b5de2c5c906a2914f05be813c12595aa616cc1005->leave($__internal_9f560ffac1d2091d135c692b5de2c5c906a2914f05be813c12595aa616cc1005_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_f2187594631cd6eb3a90ed875d8a97d25e96d6682f694bf18e0c267eb7d1cf07 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f2187594631cd6eb3a90ed875d8a97d25e96d6682f694bf18e0c267eb7d1cf07->enter($__internal_f2187594631cd6eb3a90ed875d8a97d25e96d6682f694bf18e0c267eb7d1cf07_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_4365b3f5cd0dad9ea7d7d3db9baf26536315434e72879a194e2b0dba805177a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4365b3f5cd0dad9ea7d7d3db9baf26536315434e72879a194e2b0dba805177a8->enter($__internal_4365b3f5cd0dad9ea7d7d3db9baf26536315434e72879a194e2b0dba805177a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_4365b3f5cd0dad9ea7d7d3db9baf26536315434e72879a194e2b0dba805177a8->leave($__internal_4365b3f5cd0dad9ea7d7d3db9baf26536315434e72879a194e2b0dba805177a8_prof);

        
        $__internal_f2187594631cd6eb3a90ed875d8a97d25e96d6682f694bf18e0c267eb7d1cf07->leave($__internal_f2187594631cd6eb3a90ed875d8a97d25e96d6682f694bf18e0c267eb7d1cf07_prof);

    }

    // line 8
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_76d40db40cd79ff5c76919e37a28535a7249fea9aee88b94d51e6d46d73ab85c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_76d40db40cd79ff5c76919e37a28535a7249fea9aee88b94d51e6d46d73ab85c->enter($__internal_76d40db40cd79ff5c76919e37a28535a7249fea9aee88b94d51e6d46d73ab85c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_b89f0d4cd73e08d45abd11be287dbf22e738c398ae497a98de0d66fa751dfeac = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b89f0d4cd73e08d45abd11be287dbf22e738c398ae497a98de0d66fa751dfeac->enter($__internal_b89f0d4cd73e08d45abd11be287dbf22e738c398ae497a98de0d66fa751dfeac_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_b89f0d4cd73e08d45abd11be287dbf22e738c398ae497a98de0d66fa751dfeac->leave($__internal_b89f0d4cd73e08d45abd11be287dbf22e738c398ae497a98de0d66fa751dfeac_prof);

        
        $__internal_76d40db40cd79ff5c76919e37a28535a7249fea9aee88b94d51e6d46d73ab85c->leave($__internal_76d40db40cd79ff5c76919e37a28535a7249fea9aee88b94d51e6d46d73ab85c_prof);

    }

    // line 13
    public function block_body($context, array $blocks = array())
    {
        $__internal_5f0b92ce7deeec26b7f821882dc18b7b549c6e0021e3126f32910698bfa4d449 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5f0b92ce7deeec26b7f821882dc18b7b549c6e0021e3126f32910698bfa4d449->enter($__internal_5f0b92ce7deeec26b7f821882dc18b7b549c6e0021e3126f32910698bfa4d449_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_59326ea1c8674989887ecc7e51d693d5768906102171b5eb271ce70e31322204 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_59326ea1c8674989887ecc7e51d693d5768906102171b5eb271ce70e31322204->enter($__internal_59326ea1c8674989887ecc7e51d693d5768906102171b5eb271ce70e31322204_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_59326ea1c8674989887ecc7e51d693d5768906102171b5eb271ce70e31322204->leave($__internal_59326ea1c8674989887ecc7e51d693d5768906102171b5eb271ce70e31322204_prof);

        
        $__internal_5f0b92ce7deeec26b7f821882dc18b7b549c6e0021e3126f32910698bfa4d449->leave($__internal_5f0b92ce7deeec26b7f821882dc18b7b549c6e0021e3126f32910698bfa4d449_prof);

    }

    // line 14
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_7fdfd3d4538988e6fb041b5acdc8c355bbdc5335316543bbd3d3e06971740970 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7fdfd3d4538988e6fb041b5acdc8c355bbdc5335316543bbd3d3e06971740970->enter($__internal_7fdfd3d4538988e6fb041b5acdc8c355bbdc5335316543bbd3d3e06971740970_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_9f0b0d1d8fcd2ad2190bee98ca320119bbff6ad5e3192b2711ff8fb10cfd1c1d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9f0b0d1d8fcd2ad2190bee98ca320119bbff6ad5e3192b2711ff8fb10cfd1c1d->enter($__internal_9f0b0d1d8fcd2ad2190bee98ca320119bbff6ad5e3192b2711ff8fb10cfd1c1d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_9f0b0d1d8fcd2ad2190bee98ca320119bbff6ad5e3192b2711ff8fb10cfd1c1d->leave($__internal_9f0b0d1d8fcd2ad2190bee98ca320119bbff6ad5e3192b2711ff8fb10cfd1c1d_prof);

        
        $__internal_7fdfd3d4538988e6fb041b5acdc8c355bbdc5335316543bbd3d3e06971740970->leave($__internal_7fdfd3d4538988e6fb041b5acdc8c355bbdc5335316543bbd3d3e06971740970_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 14,  109 => 13,  92 => 8,  74 => 7,  61 => 16,  58 => 15,  55 => 14,  52 => 13,  50 => 12,  43 => 9,  41 => 8,  37 => 7,  29 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
<head>


  <meta charset=\"UTF-8\" />
  <title>{% block title %}Welcome!{% endblock %}</title>
  {% block stylesheets %}{% endblock %}
  <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
</head>
<body>
  {% include \"PlatypusBundle:Default:header.html.twig\" %}
  {% block body %}{% endblock %}
  {% block javascripts %}{% endblock %}
  {% include \"PlatypusBundle:Default:footer.html.twig\" %}

</body>
</html>
", "base.html.twig", "/home/elie/Gitlab/Symfony_Project/app/Resources/views/base.html.twig");
    }
}
