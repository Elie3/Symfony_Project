<?php

/* PlatypusBundle:User:edit_user.html.twig */
class __TwigTemplate_c0baf769c526a134df3824590b30087a1f1f81a718f637b597fe136e99fbac06 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "PlatypusBundle:User:edit_user.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6aff99aeabdd2b8fd2839810232fc2a6126def0a1b5ee24eb8f1302bbc817852 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6aff99aeabdd2b8fd2839810232fc2a6126def0a1b5ee24eb8f1302bbc817852->enter($__internal_6aff99aeabdd2b8fd2839810232fc2a6126def0a1b5ee24eb8f1302bbc817852_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:User:edit_user.html.twig"));

        $__internal_1fa317f6748286d029847166a4ff33a4b8bd8499facd5ef24f3a9619314049c8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1fa317f6748286d029847166a4ff33a4b8bd8499facd5ef24f3a9619314049c8->enter($__internal_1fa317f6748286d029847166a4ff33a4b8bd8499facd5ef24f3a9619314049c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:User:edit_user.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6aff99aeabdd2b8fd2839810232fc2a6126def0a1b5ee24eb8f1302bbc817852->leave($__internal_6aff99aeabdd2b8fd2839810232fc2a6126def0a1b5ee24eb8f1302bbc817852_prof);

        
        $__internal_1fa317f6748286d029847166a4ff33a4b8bd8499facd5ef24f3a9619314049c8->leave($__internal_1fa317f6748286d029847166a4ff33a4b8bd8499facd5ef24f3a9619314049c8_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_1e2ff4e7ac6417fe108393811536c911b219d3dcf9256dbc3e9a846bd6c4797c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1e2ff4e7ac6417fe108393811536c911b219d3dcf9256dbc3e9a846bd6c4797c->enter($__internal_1e2ff4e7ac6417fe108393811536c911b219d3dcf9256dbc3e9a846bd6c4797c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_d7c448d091648de6c2899feca7a3e8a7f52331ad7976dd8378abed52f7ea5552 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d7c448d091648de6c2899feca7a3e8a7f52331ad7976dd8378abed52f7ea5552->enter($__internal_d7c448d091648de6c2899feca7a3e8a7f52331ad7976dd8378abed52f7ea5552_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo twig_escape_filter($this->env, (isset($context["text"]) || array_key_exists("text", $context) ? $context["text"] : (function () { throw new Twig_Error_Runtime('Variable "text" does not exist.', 4, $this->getSourceContext()); })()), "html", null, true);
        echo "
";
        // line 5
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 5, $this->getSourceContext()); })()), 'form_start');
        echo "
";
        // line 6
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 6, $this->getSourceContext()); })()), 'widget');
        echo "
";
        // line 7
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 7, $this->getSourceContext()); })()), 'form_end');
        echo "

";
        
        $__internal_d7c448d091648de6c2899feca7a3e8a7f52331ad7976dd8378abed52f7ea5552->leave($__internal_d7c448d091648de6c2899feca7a3e8a7f52331ad7976dd8378abed52f7ea5552_prof);

        
        $__internal_1e2ff4e7ac6417fe108393811536c911b219d3dcf9256dbc3e9a846bd6c4797c->leave($__internal_1e2ff4e7ac6417fe108393811536c911b219d3dcf9256dbc3e9a846bd6c4797c_prof);

    }

    public function getTemplateName()
    {
        return "PlatypusBundle:User:edit_user.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 7,  57 => 6,  53 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"base.html.twig\" %}

{% block body%}
{{text}}
{{ form_start(form) }}
{{ form_widget(form) }}
{{ form_end(form) }}

{% endblock %}
", "PlatypusBundle:User:edit_user.html.twig", "/home/elie/Documents/rendu/PHP/pool_symfony/test/src/PlatypusBundle/Resources/views/User/edit_user.html.twig");
    }
}
