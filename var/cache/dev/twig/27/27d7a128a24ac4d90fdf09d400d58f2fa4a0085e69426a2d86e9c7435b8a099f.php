<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_5575cdd5d06694f81fa439b8967ded2ef5bba1db65288cf36c22a9486f9f8687 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b3cdafcaade280c724227ab7c17c45464488462f6d890389b1612b80270c1431 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b3cdafcaade280c724227ab7c17c45464488462f6d890389b1612b80270c1431->enter($__internal_b3cdafcaade280c724227ab7c17c45464488462f6d890389b1612b80270c1431_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        $__internal_721ff17f65c41775b31d93f837e630f2a295215fe981d7422c61fd24c819bbe1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_721ff17f65c41775b31d93f837e630f2a295215fe981d7422c61fd24c819bbe1->enter($__internal_721ff17f65c41775b31d93f837e630f2a295215fe981d7422c61fd24c819bbe1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_b3cdafcaade280c724227ab7c17c45464488462f6d890389b1612b80270c1431->leave($__internal_b3cdafcaade280c724227ab7c17c45464488462f6d890389b1612b80270c1431_prof);

        
        $__internal_721ff17f65c41775b31d93f837e630f2a295215fe981d7422c61fd24c819bbe1->leave($__internal_721ff17f65c41775b31d93f837e630f2a295215fe981d7422c61fd24c819bbe1_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
", "@Framework/Form/form_rows.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rows.html.php");
    }
}
