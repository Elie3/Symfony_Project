<?php

/* @Framework/Form/form_end.html.php */
class __TwigTemplate_3340ab11ba7500f2d3ab826367a6f4819c97f5bd9c7273a6673e09bcbe29988e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_968e6bc3c62f354265bd3a9f710fd56c710ea98bebae8293d544448c547ffd38 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_968e6bc3c62f354265bd3a9f710fd56c710ea98bebae8293d544448c547ffd38->enter($__internal_968e6bc3c62f354265bd3a9f710fd56c710ea98bebae8293d544448c547ffd38_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        $__internal_3bc6a0935701dee4c5ab8d2bb9565626c49c16348f5f774ccaf4f9a4c60ea98f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3bc6a0935701dee4c5ab8d2bb9565626c49c16348f5f774ccaf4f9a4c60ea98f->enter($__internal_3bc6a0935701dee4c5ab8d2bb9565626c49c16348f5f774ccaf4f9a4c60ea98f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_end.html.php"));

        // line 1
        echo "<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
";
        
        $__internal_968e6bc3c62f354265bd3a9f710fd56c710ea98bebae8293d544448c547ffd38->leave($__internal_968e6bc3c62f354265bd3a9f710fd56c710ea98bebae8293d544448c547ffd38_prof);

        
        $__internal_3bc6a0935701dee4c5ab8d2bb9565626c49c16348f5f774ccaf4f9a4c60ea98f->leave($__internal_3bc6a0935701dee4c5ab8d2bb9565626c49c16348f5f774ccaf4f9a4c60ea98f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_end.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!isset(\$render_rest) || \$render_rest): ?>
<?php echo \$view['form']->rest(\$form) ?>
<?php endif ?>
</form>
", "@Framework/Form/form_end.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_end.html.php");
    }
}
