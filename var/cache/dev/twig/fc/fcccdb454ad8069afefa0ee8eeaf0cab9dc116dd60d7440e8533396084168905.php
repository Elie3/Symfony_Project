<?php

/* @Framework/Form/submit_widget.html.php */
class __TwigTemplate_264345f78078058f0203e3d1f1b3f454279ab4d309aabe1367a9eaccedd7faa7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_08254f47e66fe824c9215630fa2a844bf948719b10252f75f6b3340b66621e81 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_08254f47e66fe824c9215630fa2a844bf948719b10252f75f6b3340b66621e81->enter($__internal_08254f47e66fe824c9215630fa2a844bf948719b10252f75f6b3340b66621e81_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        $__internal_58d87018dcd84640d75ca6ee5681dc077ccf7a3cc04b64d2e8dd76020ed1c986 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_58d87018dcd84640d75ca6ee5681dc077ccf7a3cc04b64d2e8dd76020ed1c986->enter($__internal_58d87018dcd84640d75ca6ee5681dc077ccf7a3cc04b64d2e8dd76020ed1c986_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/submit_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
";
        
        $__internal_08254f47e66fe824c9215630fa2a844bf948719b10252f75f6b3340b66621e81->leave($__internal_08254f47e66fe824c9215630fa2a844bf948719b10252f75f6b3340b66621e81_prof);

        
        $__internal_58d87018dcd84640d75ca6ee5681dc077ccf7a3cc04b64d2e8dd76020ed1c986->leave($__internal_58d87018dcd84640d75ca6ee5681dc077ccf7a3cc04b64d2e8dd76020ed1c986_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/submit_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'submit')) ?>
", "@Framework/Form/submit_widget.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/submit_widget.html.php");
    }
}
