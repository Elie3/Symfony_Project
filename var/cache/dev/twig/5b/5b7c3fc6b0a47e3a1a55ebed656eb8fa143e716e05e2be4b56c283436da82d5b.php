<?php

/* PlatypusBundle:Checker:show_params.html.twig */
class __TwigTemplate_7864f39ec34f8fc83e177a4226dd949a38ee20d5c0ffdde854cc2b9677a9bee8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("PlatypusBundle:Default:layout.html.twig", "PlatypusBundle:Checker:show_params.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "PlatypusBundle:Default:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_52b3dd4d788db7b1e4c78a045d1b9a29d5e8fed3a6da214990ab174c3e539c75 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_52b3dd4d788db7b1e4c78a045d1b9a29d5e8fed3a6da214990ab174c3e539c75->enter($__internal_52b3dd4d788db7b1e4c78a045d1b9a29d5e8fed3a6da214990ab174c3e539c75_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:Checker:show_params.html.twig"));

        $__internal_473fa3b8354773078b9cc10866a8639d38624171a8ad591ab0a23047ffb552f0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_473fa3b8354773078b9cc10866a8639d38624171a8ad591ab0a23047ffb552f0->enter($__internal_473fa3b8354773078b9cc10866a8639d38624171a8ad591ab0a23047ffb552f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:Checker:show_params.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_52b3dd4d788db7b1e4c78a045d1b9a29d5e8fed3a6da214990ab174c3e539c75->leave($__internal_52b3dd4d788db7b1e4c78a045d1b9a29d5e8fed3a6da214990ab174c3e539c75_prof);

        
        $__internal_473fa3b8354773078b9cc10866a8639d38624171a8ad591ab0a23047ffb552f0->leave($__internal_473fa3b8354773078b9cc10866a8639d38624171a8ad591ab0a23047ffb552f0_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_006cb62d62853f9a901c972a3d8a2db6a22320b6a701372f07b92aabaef0a9f4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_006cb62d62853f9a901c972a3d8a2db6a22320b6a701372f07b92aabaef0a9f4->enter($__internal_006cb62d62853f9a901c972a3d8a2db6a22320b6a701372f07b92aabaef0a9f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_089d0b642b94db557c702e4ed7d1783a886573a83cf2ae103a3b35be9ca05711 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_089d0b642b94db557c702e4ed7d1783a886573a83cf2ae103a3b35be9ca05711->enter($__internal_089d0b642b94db557c702e4ed7d1783a886573a83cf2ae103a3b35be9ca05711_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "PlatypusBundle:Checker:showParams

";
        
        $__internal_089d0b642b94db557c702e4ed7d1783a886573a83cf2ae103a3b35be9ca05711->leave($__internal_089d0b642b94db557c702e4ed7d1783a886573a83cf2ae103a3b35be9ca05711_prof);

        
        $__internal_006cb62d62853f9a901c972a3d8a2db6a22320b6a701372f07b92aabaef0a9f4->leave($__internal_006cb62d62853f9a901c972a3d8a2db6a22320b6a701372f07b92aabaef0a9f4_prof);

    }

    // line 7
    public function block_body($context, array $blocks = array())
    {
        $__internal_d892c1cb114739f3a6f0c343248bfb1f5eb182c6a678663ffdbf1dbb18b66b2a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d892c1cb114739f3a6f0c343248bfb1f5eb182c6a678663ffdbf1dbb18b66b2a->enter($__internal_d892c1cb114739f3a6f0c343248bfb1f5eb182c6a678663ffdbf1dbb18b66b2a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_12c31232aab0c36c28f87c44cb72486a27413f9ab5270877ab084d817a3743de = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_12c31232aab0c36c28f87c44cb72486a27413f9ab5270877ab084d817a3743de->enter($__internal_12c31232aab0c36c28f87c44cb72486a27413f9ab5270877ab084d817a3743de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 8
        echo "

<link rel=\"stylesheet\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("style.css"), "html", null, true);
        echo "\" />

<table>

    ";
        // line 14
        $context["count"] = 0;
        // line 15
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["GET"]) || array_key_exists("GET", $context) ? $context["GET"] : (function () { throw new Twig_Error_Runtime('Variable "GET" does not exist.', 15, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 16
            echo "        <tr><td>";
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "</td> <td>";
            echo twig_escape_filter($this->env, $context["value"], "html", null, true);
            echo "</td></tr>
        ";
            // line 17
            $context["count"] = ((isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new Twig_Error_Runtime('Variable "count" does not exist.', 17, $this->getSourceContext()); })()) + 1);
            // line 18
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "
    ";
        // line 20
        echo twig_escape_filter($this->env, (isset($context["count"]) || array_key_exists("count", $context) ? $context["count"] : (function () { throw new Twig_Error_Runtime('Variable "count" does not exist.', 20, $this->getSourceContext()); })()), "html", null, true);
        echo "
    ";
        // line 21
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new Twig_Error_Runtime('Variable "app" does not exist.', 21, $this->getSourceContext()); })()), "environment", array()), "html", null, true);
        echo "


</table>


";
        
        $__internal_12c31232aab0c36c28f87c44cb72486a27413f9ab5270877ab084d817a3743de->leave($__internal_12c31232aab0c36c28f87c44cb72486a27413f9ab5270877ab084d817a3743de_prof);

        
        $__internal_d892c1cb114739f3a6f0c343248bfb1f5eb182c6a678663ffdbf1dbb18b66b2a->leave($__internal_d892c1cb114739f3a6f0c343248bfb1f5eb182c6a678663ffdbf1dbb18b66b2a_prof);

    }

    public function getTemplateName()
    {
        return "PlatypusBundle:Checker:show_params.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 21,  106 => 20,  103 => 19,  97 => 18,  95 => 17,  88 => 16,  83 => 15,  81 => 14,  74 => 10,  70 => 8,  61 => 7,  41 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"PlatypusBundle:Default:layout.html.twig\" %}

{% block title %}PlatypusBundle:Checker:showParams

{% endblock %}

{% block body %}


<link rel=\"stylesheet\" href=\"{{ asset('style.css') }}\" />

<table>

    {% set count = 0 %}
    {% for key, value in GET  %}
        <tr><td>{{key}}</td> <td>{{value}}</td></tr>
        {% set count = count + 1 %}
    {% endfor %}

    {{count}}
    {{app.environment}}


</table>


{% endblock %}
", "PlatypusBundle:Checker:show_params.html.twig", "/home/elie/Documents/rendu/PHP/pool_symfony/test/src/PlatypusBundle/Resources/views/Checker/show_params.html.twig");
    }
}
