<?php

/* WebProfilerBundle:Profiler:info.html.twig */
class __TwigTemplate_1e734c42058fbbc91888a4c54ed4dc283303a60b29cd4f5f7dbe8d6428cca2b5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Profiler:info.html.twig", 1);
        $this->blocks = array(
            'summary' => array($this, 'block_summary'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cb0aece788acf6225fddc8703b0a55955504b9c6a293c9695dd5b482a9117a0a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cb0aece788acf6225fddc8703b0a55955504b9c6a293c9695dd5b482a9117a0a->enter($__internal_cb0aece788acf6225fddc8703b0a55955504b9c6a293c9695dd5b482a9117a0a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        $__internal_9baf33a165131c11c959d82ee778abd3cdb982a5c8696bbc92cdf083a7a1c6d4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9baf33a165131c11c959d82ee778abd3cdb982a5c8696bbc92cdf083a7a1c6d4->enter($__internal_9baf33a165131c11c959d82ee778abd3cdb982a5c8696bbc92cdf083a7a1c6d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        // line 3
        $context["messages"] = array("no_token" => array("status" => "error", "title" => (((((        // line 6
array_key_exists("token", $context)) ? (_twig_default_filter((isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 6, $this->getSourceContext()); })()), "")) : ("")) == "latest")) ? ("There are no profiles") : ("Token not found")), "message" => (((((        // line 7
array_key_exists("token", $context)) ? (_twig_default_filter((isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 7, $this->getSourceContext()); })()), "")) : ("")) == "latest")) ? ("No profiles found in the database.") : ((("Token \"" . ((array_key_exists("token", $context)) ? (_twig_default_filter((isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 7, $this->getSourceContext()); })()), "")) : (""))) . "\" was not found in the database.")))));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cb0aece788acf6225fddc8703b0a55955504b9c6a293c9695dd5b482a9117a0a->leave($__internal_cb0aece788acf6225fddc8703b0a55955504b9c6a293c9695dd5b482a9117a0a_prof);

        
        $__internal_9baf33a165131c11c959d82ee778abd3cdb982a5c8696bbc92cdf083a7a1c6d4->leave($__internal_9baf33a165131c11c959d82ee778abd3cdb982a5c8696bbc92cdf083a7a1c6d4_prof);

    }

    // line 11
    public function block_summary($context, array $blocks = array())
    {
        $__internal_fe81f3112aa7dda4fa660d1be08f44b54b9668bba27593c5987ef6a2c0dfd075 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fe81f3112aa7dda4fa660d1be08f44b54b9668bba27593c5987ef6a2c0dfd075->enter($__internal_fe81f3112aa7dda4fa660d1be08f44b54b9668bba27593c5987ef6a2c0dfd075_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        $__internal_72298d64ad120ebbc22a759880ed89b0953b6d7c815ab3084142cd492da29630 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_72298d64ad120ebbc22a759880ed89b0953b6d7c815ab3084142cd492da29630->enter($__internal_72298d64ad120ebbc22a759880ed89b0953b6d7c815ab3084142cd492da29630_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        // line 12
        echo "    <div class=\"status status-";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["messages"]) || array_key_exists("messages", $context) ? $context["messages"] : (function () { throw new Twig_Error_Runtime('Variable "messages" does not exist.', 12, $this->getSourceContext()); })()), (isset($context["about"]) || array_key_exists("about", $context) ? $context["about"] : (function () { throw new Twig_Error_Runtime('Variable "about" does not exist.', 12, $this->getSourceContext()); })()), array(), "array"), "status", array()), "html", null, true);
        echo "\">
        <div class=\"container\">
            <h2>";
        // line 14
        echo twig_escape_filter($this->env, twig_title_string_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["messages"]) || array_key_exists("messages", $context) ? $context["messages"] : (function () { throw new Twig_Error_Runtime('Variable "messages" does not exist.', 14, $this->getSourceContext()); })()), (isset($context["about"]) || array_key_exists("about", $context) ? $context["about"] : (function () { throw new Twig_Error_Runtime('Variable "about" does not exist.', 14, $this->getSourceContext()); })()), array(), "array"), "status", array())), "html", null, true);
        echo "</h2>
        </div>
    </div>
";
        
        $__internal_72298d64ad120ebbc22a759880ed89b0953b6d7c815ab3084142cd492da29630->leave($__internal_72298d64ad120ebbc22a759880ed89b0953b6d7c815ab3084142cd492da29630_prof);

        
        $__internal_fe81f3112aa7dda4fa660d1be08f44b54b9668bba27593c5987ef6a2c0dfd075->leave($__internal_fe81f3112aa7dda4fa660d1be08f44b54b9668bba27593c5987ef6a2c0dfd075_prof);

    }

    // line 19
    public function block_panel($context, array $blocks = array())
    {
        $__internal_7c68203dab4241f4aeca353e69b779551213b2a9f8e553ffc03fa18309f541c0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7c68203dab4241f4aeca353e69b779551213b2a9f8e553ffc03fa18309f541c0->enter($__internal_7c68203dab4241f4aeca353e69b779551213b2a9f8e553ffc03fa18309f541c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_272086c92d92c5e0a428f301a823f2d513c2ca694b6f1b1d2062dacdc41fedc7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_272086c92d92c5e0a428f301a823f2d513c2ca694b6f1b1d2062dacdc41fedc7->enter($__internal_272086c92d92c5e0a428f301a823f2d513c2ca694b6f1b1d2062dacdc41fedc7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 20
        echo "    <h2>";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["messages"]) || array_key_exists("messages", $context) ? $context["messages"] : (function () { throw new Twig_Error_Runtime('Variable "messages" does not exist.', 20, $this->getSourceContext()); })()), (isset($context["about"]) || array_key_exists("about", $context) ? $context["about"] : (function () { throw new Twig_Error_Runtime('Variable "about" does not exist.', 20, $this->getSourceContext()); })()), array(), "array"), "title", array()), "html", null, true);
        echo "</h2>
    <p>";
        // line 21
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["messages"]) || array_key_exists("messages", $context) ? $context["messages"] : (function () { throw new Twig_Error_Runtime('Variable "messages" does not exist.', 21, $this->getSourceContext()); })()), (isset($context["about"]) || array_key_exists("about", $context) ? $context["about"] : (function () { throw new Twig_Error_Runtime('Variable "about" does not exist.', 21, $this->getSourceContext()); })()), array(), "array"), "message", array()), "html", null, true);
        echo "</p>
";
        
        $__internal_272086c92d92c5e0a428f301a823f2d513c2ca694b6f1b1d2062dacdc41fedc7->leave($__internal_272086c92d92c5e0a428f301a823f2d513c2ca694b6f1b1d2062dacdc41fedc7_prof);

        
        $__internal_7c68203dab4241f4aeca353e69b779551213b2a9f8e553ffc03fa18309f541c0->leave($__internal_7c68203dab4241f4aeca353e69b779551213b2a9f8e553ffc03fa18309f541c0_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 21,  84 => 20,  75 => 19,  61 => 14,  55 => 12,  46 => 11,  36 => 1,  34 => 7,  33 => 6,  32 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% set messages = {
    'no_token' : {
        status:  'error',
        title:   (token|default('') == 'latest') ? 'There are no profiles' : 'Token not found',
        message: (token|default('') == 'latest') ? 'No profiles found in the database.' : 'Token \"' ~ token|default('') ~ '\" was not found in the database.'
    }
} %}

{% block summary %}
    <div class=\"status status-{{ messages[about].status }}\">
        <div class=\"container\">
            <h2>{{ messages[about].status|title }}</h2>
        </div>
    </div>
{% endblock %}

{% block panel %}
    <h2>{{ messages[about].title }}</h2>
    <p>{{ messages[about].message }}</p>
{% endblock %}
", "WebProfilerBundle:Profiler:info.html.twig", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/info.html.twig");
    }
}
