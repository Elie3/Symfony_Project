<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_bbdf2d02f6a469cefe700918f2519bf108680707fa48e2e027582ece7942340a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_31a2f711b724e9c513a7ef913ac67438f145aff64378c9b490e5775712b481fe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_31a2f711b724e9c513a7ef913ac67438f145aff64378c9b490e5775712b481fe->enter($__internal_31a2f711b724e9c513a7ef913ac67438f145aff64378c9b490e5775712b481fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        $__internal_73c14ade0df3097d9d457f538a12770ccd8543a1c3681efd2c219b4a94a524f8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_73c14ade0df3097d9d457f538a12770ccd8543a1c3681efd2c219b4a94a524f8->enter($__internal_73c14ade0df3097d9d457f538a12770ccd8543a1c3681efd2c219b4a94a524f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/exception.xml.twig", array("exception" => (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 1, $this->getSourceContext()); })())));
        echo "
";
        
        $__internal_31a2f711b724e9c513a7ef913ac67438f145aff64378c9b490e5775712b481fe->leave($__internal_31a2f711b724e9c513a7ef913ac67438f145aff64378c9b490e5775712b481fe_prof);

        
        $__internal_73c14ade0df3097d9d457f538a12770ccd8543a1c3681efd2c219b4a94a524f8->leave($__internal_73c14ade0df3097d9d457f538a12770ccd8543a1c3681efd2c219b4a94a524f8_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/exception.xml.twig', { exception: exception }) }}
", "TwigBundle:Exception:exception.atom.twig", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.atom.twig");
    }
}
