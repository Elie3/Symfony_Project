<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_debf8e5bb5a67a2d92a0b7c68a933f6c2f24df9bd80aab9b6cb86f8f65f4bfd9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5fe7fe364321a5508b1b0e198f3ac7c3019d6b55633ef1541a58427d74f97400 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5fe7fe364321a5508b1b0e198f3ac7c3019d6b55633ef1541a58427d74f97400->enter($__internal_5fe7fe364321a5508b1b0e198f3ac7c3019d6b55633ef1541a58427d74f97400_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        $__internal_04c20ac1642c939becd2a063543764ad35c3bd0cd4b0e52ee07148de26bf1694 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_04c20ac1642c939becd2a063543764ad35c3bd0cd4b0e52ee07148de26bf1694->enter($__internal_04c20ac1642c939becd2a063543764ad35c3bd0cd4b0e52ee07148de26bf1694_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_5fe7fe364321a5508b1b0e198f3ac7c3019d6b55633ef1541a58427d74f97400->leave($__internal_5fe7fe364321a5508b1b0e198f3ac7c3019d6b55633ef1541a58427d74f97400_prof);

        
        $__internal_04c20ac1642c939becd2a063543764ad35c3bd0cd4b0e52ee07148de26bf1694->leave($__internal_04c20ac1642c939becd2a063543764ad35c3bd0cd4b0e52ee07148de26bf1694_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
", "@Framework/Form/url_widget.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/url_widget.html.php");
    }
}
