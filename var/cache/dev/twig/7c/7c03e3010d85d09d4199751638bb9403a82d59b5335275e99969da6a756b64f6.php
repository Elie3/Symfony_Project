<?php

/* PlatypusBundle:User:index.html.twig */
class __TwigTemplate_281c527e239333e7b3fbf5261fb3a41291582245dfefb0029fae134f6480497a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "PlatypusBundle:User:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_34b1c571012dc2b1550118de30d786de66782616ad899aa49f0ebe2bfc90148e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_34b1c571012dc2b1550118de30d786de66782616ad899aa49f0ebe2bfc90148e->enter($__internal_34b1c571012dc2b1550118de30d786de66782616ad899aa49f0ebe2bfc90148e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:User:index.html.twig"));

        $__internal_439a216d9e2bbee449663283ac893ffa3608d47a7414eabf183fdfe1298c7f67 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_439a216d9e2bbee449663283ac893ffa3608d47a7414eabf183fdfe1298c7f67->enter($__internal_439a216d9e2bbee449663283ac893ffa3608d47a7414eabf183fdfe1298c7f67_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:User:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_34b1c571012dc2b1550118de30d786de66782616ad899aa49f0ebe2bfc90148e->leave($__internal_34b1c571012dc2b1550118de30d786de66782616ad899aa49f0ebe2bfc90148e_prof);

        
        $__internal_439a216d9e2bbee449663283ac893ffa3608d47a7414eabf183fdfe1298c7f67->leave($__internal_439a216d9e2bbee449663283ac893ffa3608d47a7414eabf183fdfe1298c7f67_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_49ce780a2ec6f5311a2c971e39997ce119abe3572569217e1ce7849860414956 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_49ce780a2ec6f5311a2c971e39997ce119abe3572569217e1ce7849860414956->enter($__internal_49ce780a2ec6f5311a2c971e39997ce119abe3572569217e1ce7849860414956_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_2dfff9d34aa9be895030c2b37e6d05b0d6619bb5a2bb19509d7a2a6fb18b8b26 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2dfff9d34aa9be895030c2b37e6d05b0d6619bb5a2bb19509d7a2a6fb18b8b26->enter($__internal_2dfff9d34aa9be895030c2b37e6d05b0d6619bb5a2bb19509d7a2a6fb18b8b26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "

";
        // line 8
        echo "
";
        // line 9
        if ((isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 9, $this->getSourceContext()); })())) {
            // line 10
            echo "    <div>";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 10, $this->getSourceContext()); })()), "messageKey", array()), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 10, $this->getSourceContext()); })()), "messageData", array()), "security"), "html", null, true);
            echo "</div>
";
        }
        // line 12
        echo "
<form action=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("login");
        echo "\" method=\"post\">
    <label for=\"username\">Username:</label>
    <input type=\"text\" id=\"username\" name=\"_username\" value=\"";
        // line 15
        echo twig_escape_filter($this->env, (isset($context["last_username"]) || array_key_exists("last_username", $context) ? $context["last_username"] : (function () { throw new Twig_Error_Runtime('Variable "last_username" does not exist.', 15, $this->getSourceContext()); })()), "html", null, true);
        echo "\" />

    <label for=\"password\">Password:</label>
    <input type=\"password\" id=\"password\" name=\"_password\" />

    ";
        // line 25
        echo "
    <input type=\"checkbox\" name=\"_remember_me\" id=\"remember_me\">
    <label for=\"remember_me\">Keep me logged in</label>

    <button type=\"submit\">login</button>
</form>
";
        
        $__internal_2dfff9d34aa9be895030c2b37e6d05b0d6619bb5a2bb19509d7a2a6fb18b8b26->leave($__internal_2dfff9d34aa9be895030c2b37e6d05b0d6619bb5a2bb19509d7a2a6fb18b8b26_prof);

        
        $__internal_49ce780a2ec6f5311a2c971e39997ce119abe3572569217e1ce7849860414956->leave($__internal_49ce780a2ec6f5311a2c971e39997ce119abe3572569217e1ce7849860414956_prof);

    }

    public function getTemplateName()
    {
        return "PlatypusBundle:User:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 25,  72 => 15,  67 => 13,  64 => 12,  58 => 10,  56 => 9,  53 => 8,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"base.html.twig\" %}

{% block body%}


{# app/Resources/views/security/login.html.twig #}
{# ... you will probably extend your base template, like base.html.twig #}

{% if error %}
    <div>{{ error.messageKey|trans(error.messageData, 'security') }}</div>
{% endif %}

<form action=\"{{ path('login') }}\" method=\"post\">
    <label for=\"username\">Username:</label>
    <input type=\"text\" id=\"username\" name=\"_username\" value=\"{{ last_username }}\" />

    <label for=\"password\">Password:</label>
    <input type=\"password\" id=\"password\" name=\"_password\" />

    {#
        If you want to control the URL the user
        is redirected to on success (more details below)
        <input type=\"hidden\" name=\"_target_path\" value=\"/account\" />
    #}

    <input type=\"checkbox\" name=\"_remember_me\" id=\"remember_me\">
    <label for=\"remember_me\">Keep me logged in</label>

    <button type=\"submit\">login</button>
</form>
{% endblock%}
", "PlatypusBundle:User:index.html.twig", "/home/elie/Documents/rendu/PHP/pool_symfony/test/src/PlatypusBundle/Resources/views/User/index.html.twig");
    }
}
