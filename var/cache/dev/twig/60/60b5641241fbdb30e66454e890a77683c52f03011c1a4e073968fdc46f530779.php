<?php

/* PlatypusBundle:Default:header.html.twig */
class __TwigTemplate_67aa2c89ef23e99ad50759b7eb32da7d1366397e2d4dcb7f4752b39c595ec8c8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1d9fc175b097873fca4669366d1903b93ac0921c846db55faea023fee58c23bc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1d9fc175b097873fca4669366d1903b93ac0921c846db55faea023fee58c23bc->enter($__internal_1d9fc175b097873fca4669366d1903b93ac0921c846db55faea023fee58c23bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:Default:header.html.twig"));

        $__internal_eec19aa4f38dd68b7259f173f7d794843d237859cecb70e2ba1a247022e58320 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eec19aa4f38dd68b7259f173f7d794843d237859cecb70e2ba1a247022e58320->enter($__internal_eec19aa4f38dd68b7259f173f7d794843d237859cecb70e2ba1a247022e58320_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:Default:header.html.twig"));

        // line 1
        echo "<a class=\"home_btn\" href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("platypus_homepage");
        echo "\">Home</a>
";
        
        $__internal_1d9fc175b097873fca4669366d1903b93ac0921c846db55faea023fee58c23bc->leave($__internal_1d9fc175b097873fca4669366d1903b93ac0921c846db55faea023fee58c23bc_prof);

        
        $__internal_eec19aa4f38dd68b7259f173f7d794843d237859cecb70e2ba1a247022e58320->leave($__internal_eec19aa4f38dd68b7259f173f7d794843d237859cecb70e2ba1a247022e58320_prof);

    }

    public function getTemplateName()
    {
        return "PlatypusBundle:Default:header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<a class=\"home_btn\" href=\"{{ path('platypus_homepage') }}\">Home</a>
", "PlatypusBundle:Default:header.html.twig", "/home/elie/Gitlab/Symfony_Project/src/PlatypusBundle/Resources/views/Default/header.html.twig");
    }
}
