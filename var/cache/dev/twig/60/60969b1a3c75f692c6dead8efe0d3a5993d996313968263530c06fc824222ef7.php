<?php

/* PlatypusBundle:Post:posts_admin.html.twig */
class __TwigTemplate_6699e73bf3e3584343a1c94a2952cefcf89dad8eb9661d329fb175b641abeec5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "PlatypusBundle:Post:posts_admin.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bb9bf687324d5e3e1002d65748ec93a764aa2e0c3bcd18223595f09fa596d7a9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bb9bf687324d5e3e1002d65748ec93a764aa2e0c3bcd18223595f09fa596d7a9->enter($__internal_bb9bf687324d5e3e1002d65748ec93a764aa2e0c3bcd18223595f09fa596d7a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:Post:posts_admin.html.twig"));

        $__internal_bfbdbae5876631bd7be77e45c4c6e8d9bf172cb32eed0f3eb87a1708a60f7799 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bfbdbae5876631bd7be77e45c4c6e8d9bf172cb32eed0f3eb87a1708a60f7799->enter($__internal_bfbdbae5876631bd7be77e45c4c6e8d9bf172cb32eed0f3eb87a1708a60f7799_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:Post:posts_admin.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bb9bf687324d5e3e1002d65748ec93a764aa2e0c3bcd18223595f09fa596d7a9->leave($__internal_bb9bf687324d5e3e1002d65748ec93a764aa2e0c3bcd18223595f09fa596d7a9_prof);

        
        $__internal_bfbdbae5876631bd7be77e45c4c6e8d9bf172cb32eed0f3eb87a1708a60f7799->leave($__internal_bfbdbae5876631bd7be77e45c4c6e8d9bf172cb32eed0f3eb87a1708a60f7799_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_23acfb01c792cb00118ef514e9afc8ae1d4df70d8a605b11301a20eb46515c2f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_23acfb01c792cb00118ef514e9afc8ae1d4df70d8a605b11301a20eb46515c2f->enter($__internal_23acfb01c792cb00118ef514e9afc8ae1d4df70d8a605b11301a20eb46515c2f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_80481d5437b62d9764a94f0cf7d12d9688a3ade74b27acda22029b6f521cbedf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_80481d5437b62d9764a94f0cf7d12d9688a3ade74b27acda22029b6f521cbedf->enter($__internal_80481d5437b62d9764a94f0cf7d12d9688a3ade74b27acda22029b6f521cbedf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("style.css"), "html", null, true);
        echo "\"/>
<table>

<tr><th>Id</th><th>Title</th><th>Content</th><th>Creation Date</th><th>Modification Date</th></tr>
";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new Twig_Error_Runtime('Variable "posts" does not exist.', 9, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["elem"]) {
            // line 10
            echo "<tr>
<td>";
            // line 11
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "id", array()), "html", null, true);
            echo "</td>
<td>";
            // line 12
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "title", array()), "html", null, true);
            echo "</td>
<td>";
            // line 13
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "content", array()), "html", null, true);
            echo "</td>
<td>";
            // line 14
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "creationDate", array()), "Y-m-d"), "html", null, true);
            echo "</td>
<td>";
            // line 15
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "modificationDate", array()), "Y-m-d"), "html", null, true);
            echo "</td>
<td><a href=\"";
            // line 16
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("edit_a_post", array("id" => twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "id", array()))), "html", null, true);
            echo "\">edit this article</a></td>
<td><a href=\"";
            // line 17
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("delete_a_post", array("id" => twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "id", array()))), "html", null, true);
            echo "\">delete this article</a></td>
</tr>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['elem'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "
<table>
<p class=\"home_btn\"><a href=\"";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_new_post");
        echo "\">Create a new Post</a></p>
<p class=\"home_btn\"><a href=\"";
        // line 24
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("show_my_posts");
        echo "\">Show my posts</a></p>





";
        
        $__internal_80481d5437b62d9764a94f0cf7d12d9688a3ade74b27acda22029b6f521cbedf->leave($__internal_80481d5437b62d9764a94f0cf7d12d9688a3ade74b27acda22029b6f521cbedf_prof);

        
        $__internal_23acfb01c792cb00118ef514e9afc8ae1d4df70d8a605b11301a20eb46515c2f->leave($__internal_23acfb01c792cb00118ef514e9afc8ae1d4df70d8a605b11301a20eb46515c2f_prof);

    }

    public function getTemplateName()
    {
        return "PlatypusBundle:Post:posts_admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 24,  104 => 23,  100 => 21,  90 => 17,  86 => 16,  82 => 15,  78 => 14,  74 => 13,  70 => 12,  66 => 11,  63 => 10,  59 => 9,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"base.html.twig\" %}

{% block body%}

<link rel=\"stylesheet\" href=\"{{ asset('style.css')}}\"/>
<table>

<tr><th>Id</th><th>Title</th><th>Content</th><th>Creation Date</th><th>Modification Date</th></tr>
{% for elem in posts%}
<tr>
<td>{{elem.id}}</td>
<td>{{elem.title}}</td>
<td>{{elem.content}}</td>
<td>{{elem.creationDate|date('Y-m-d')}}</td>
<td>{{elem.modificationDate|date('Y-m-d')}}</td>
<td><a href=\"{{ path('edit_a_post', {'id': elem.id}) }}\">edit this article</a></td>
<td><a href=\"{{ path('delete_a_post', {'id': elem.id}) }}\">delete this article</a></td>
</tr>

{%endfor%}

<table>
<p class=\"home_btn\"><a href=\"{{ path('create_new_post') }}\">Create a new Post</a></p>
<p class=\"home_btn\"><a href=\"{{ path('show_my_posts') }}\">Show my posts</a></p>





{% endblock %}
", "PlatypusBundle:Post:posts_admin.html.twig", "/home/elie/Documents/rendu/PHP/pool_symfony/test/src/PlatypusBundle/Resources/views/Post/posts_admin.html.twig");
    }
}
