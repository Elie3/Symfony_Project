<?php

/* PlatypusBundle:User:register.html.twig */
class __TwigTemplate_c98a99e46dcf8fa347e25686bd2e970fe0088509e21ad85fcd768f7db4904be9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "PlatypusBundle:User:register.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_71a03cc02df73efc2fa145ad8e8982076258b9380bdf53e98deef7ce9d15d9c1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_71a03cc02df73efc2fa145ad8e8982076258b9380bdf53e98deef7ce9d15d9c1->enter($__internal_71a03cc02df73efc2fa145ad8e8982076258b9380bdf53e98deef7ce9d15d9c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:User:register.html.twig"));

        $__internal_6b6e46b94bd2a6312fae8ab17ba63690318e7a050aee0a3c6aabcfdd5a96ea5d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6b6e46b94bd2a6312fae8ab17ba63690318e7a050aee0a3c6aabcfdd5a96ea5d->enter($__internal_6b6e46b94bd2a6312fae8ab17ba63690318e7a050aee0a3c6aabcfdd5a96ea5d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:User:register.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_71a03cc02df73efc2fa145ad8e8982076258b9380bdf53e98deef7ce9d15d9c1->leave($__internal_71a03cc02df73efc2fa145ad8e8982076258b9380bdf53e98deef7ce9d15d9c1_prof);

        
        $__internal_6b6e46b94bd2a6312fae8ab17ba63690318e7a050aee0a3c6aabcfdd5a96ea5d->leave($__internal_6b6e46b94bd2a6312fae8ab17ba63690318e7a050aee0a3c6aabcfdd5a96ea5d_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_d926b24d517bc27e3a1454735c375ac3ae102388cda59f643616c1d6c11a5ce9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d926b24d517bc27e3a1454735c375ac3ae102388cda59f643616c1d6c11a5ce9->enter($__internal_d926b24d517bc27e3a1454735c375ac3ae102388cda59f643616c1d6c11a5ce9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_eb9ff9ccf4ad03afd9e8df40331f85a6055855472da6746e13381107b28f1eaf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eb9ff9ccf4ad03afd9e8df40331f85a6055855472da6746e13381107b28f1eaf->enter($__internal_eb9ff9ccf4ad03afd9e8df40331f85a6055855472da6746e13381107b28f1eaf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
";
        // line 5
        if (((isset($context["tag"]) || array_key_exists("tag", $context) ? $context["tag"] : (function () { throw new Twig_Error_Runtime('Variable "tag" does not exist.', 5, $this->getSourceContext()); })()) == "pas encore fait")) {
            // line 6
            echo "
";
            // line 7
            echo             $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 7, $this->getSourceContext()); })()), 'form_start');
            echo "
";
            // line 8
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 8, $this->getSourceContext()); })()), 'widget');
            echo "
";
            // line 9
            echo             $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 9, $this->getSourceContext()); })()), 'form_end');
            echo "


";
        }
        // line 13
        echo "
";
        // line 14
        if (((isset($context["tag"]) || array_key_exists("tag", $context) ? $context["tag"] : (function () { throw new Twig_Error_Runtime('Variable "tag" does not exist.', 14, $this->getSourceContext()); })()) == "ok")) {
            // line 15
            echo "
";
            // line 16
            echo twig_escape_filter($this->env, (isset($context["text"]) || array_key_exists("text", $context) ? $context["text"] : (function () { throw new Twig_Error_Runtime('Variable "text" does not exist.', 16, $this->getSourceContext()); })()), "html", null, true);
            echo "
<a href=\"";
            // line 17
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("login");
            echo "\">Please log in</a>


";
        }
        // line 21
        echo "
";
        // line 22
        if (((isset($context["tag"]) || array_key_exists("tag", $context) ? $context["tag"] : (function () { throw new Twig_Error_Runtime('Variable "tag" does not exist.', 22, $this->getSourceContext()); })()) == "Erreur")) {
            // line 23
            echo "
";
            // line 24
            echo twig_escape_filter($this->env, (isset($context["text"]) || array_key_exists("text", $context) ? $context["text"] : (function () { throw new Twig_Error_Runtime('Variable "text" does not exist.', 24, $this->getSourceContext()); })()), "html", null, true);
            echo "

";
        }
        // line 27
        echo "
";
        
        $__internal_eb9ff9ccf4ad03afd9e8df40331f85a6055855472da6746e13381107b28f1eaf->leave($__internal_eb9ff9ccf4ad03afd9e8df40331f85a6055855472da6746e13381107b28f1eaf_prof);

        
        $__internal_d926b24d517bc27e3a1454735c375ac3ae102388cda59f643616c1d6c11a5ce9->leave($__internal_d926b24d517bc27e3a1454735c375ac3ae102388cda59f643616c1d6c11a5ce9_prof);

    }

    public function getTemplateName()
    {
        return "PlatypusBundle:User:register.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 27,  99 => 24,  96 => 23,  94 => 22,  91 => 21,  84 => 17,  80 => 16,  77 => 15,  75 => 14,  72 => 13,  65 => 9,  61 => 8,  57 => 7,  54 => 6,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"base.html.twig\" %}

{% block body%}

{% if tag == \"pas encore fait\"  %}

{{ form_start(form) }}
{{ form_widget(form) }}
{{ form_end(form) }}


{%endif%}

{% if tag == \"ok\" %}

{{text}}
<a href=\"{{ path('login') }}\">Please log in</a>


{%endif%}

{% if tag == \"Erreur\" %}

{{text}}

{%endif%}

{%endblock%}
", "PlatypusBundle:User:register.html.twig", "/home/elie/Gitlab/Symfony_Project/src/PlatypusBundle/Resources/views/User/register.html.twig");
    }
}
