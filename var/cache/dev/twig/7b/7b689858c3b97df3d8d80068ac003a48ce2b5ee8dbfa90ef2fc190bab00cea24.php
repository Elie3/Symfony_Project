<?php

/* @Framework/Form/button_attributes.html.php */
class __TwigTemplate_c6ef73861f95ac5fc02c9f77bdf2e99ece9af1cd98da7fe0a7ee4dda4d34c113 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d2e74813d0654d4f177fc619c6e4627a85aa60547300b3a8bef1c02d53eec73d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d2e74813d0654d4f177fc619c6e4627a85aa60547300b3a8bef1c02d53eec73d->enter($__internal_d2e74813d0654d4f177fc619c6e4627a85aa60547300b3a8bef1c02d53eec73d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_attributes.html.php"));

        $__internal_6549dd170b91be24b97f5c3db2c1cd6a3159b0638496b06bc4cf2d641e450fcf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6549dd170b91be24b97f5c3db2c1cd6a3159b0638496b06bc4cf2d641e450fcf->enter($__internal_6549dd170b91be24b97f5c3db2c1cd6a3159b0638496b06bc4cf2d641e450fcf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_attributes.html.php"));

        // line 1
        echo "id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $__internal_d2e74813d0654d4f177fc619c6e4627a85aa60547300b3a8bef1c02d53eec73d->leave($__internal_d2e74813d0654d4f177fc619c6e4627a85aa60547300b3a8bef1c02d53eec73d_prof);

        
        $__internal_6549dd170b91be24b97f5c3db2c1cd6a3159b0638496b06bc4cf2d641e450fcf->leave($__internal_6549dd170b91be24b97f5c3db2c1cd6a3159b0638496b06bc4cf2d641e450fcf_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/button_attributes.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_attributes.html.php");
    }
}
