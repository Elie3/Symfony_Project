<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_b9d1b17705ac9e4847598e11da598c5bc6bfce97b7e4a4d98c2cf4c2d18c8b40 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3f5fb459fe4b6dba812712f450420908462331fb43ec02b3301f326303413c9a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3f5fb459fe4b6dba812712f450420908462331fb43ec02b3301f326303413c9a->enter($__internal_3f5fb459fe4b6dba812712f450420908462331fb43ec02b3301f326303413c9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_ab5bc562bb5923b6fc5ccd1e9d8cf27ce82dbb4f514b45473ec7c434db146966 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ab5bc562bb5923b6fc5ccd1e9d8cf27ce82dbb4f514b45473ec7c434db146966->enter($__internal_ab5bc562bb5923b6fc5ccd1e9d8cf27ce82dbb4f514b45473ec7c434db146966_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3f5fb459fe4b6dba812712f450420908462331fb43ec02b3301f326303413c9a->leave($__internal_3f5fb459fe4b6dba812712f450420908462331fb43ec02b3301f326303413c9a_prof);

        
        $__internal_ab5bc562bb5923b6fc5ccd1e9d8cf27ce82dbb4f514b45473ec7c434db146966->leave($__internal_ab5bc562bb5923b6fc5ccd1e9d8cf27ce82dbb4f514b45473ec7c434db146966_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_bb3d962208807ec43b47a04295808d7c519ce8baafc8f202df7fc62d5aa345a9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bb3d962208807ec43b47a04295808d7c519ce8baafc8f202df7fc62d5aa345a9->enter($__internal_bb3d962208807ec43b47a04295808d7c519ce8baafc8f202df7fc62d5aa345a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_ce7a9675ebc02afd8975fe92623236e37e87735180e1a93319ee59c058feab5b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ce7a9675ebc02afd8975fe92623236e37e87735180e1a93319ee59c058feab5b->enter($__internal_ce7a9675ebc02afd8975fe92623236e37e87735180e1a93319ee59c058feab5b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_ce7a9675ebc02afd8975fe92623236e37e87735180e1a93319ee59c058feab5b->leave($__internal_ce7a9675ebc02afd8975fe92623236e37e87735180e1a93319ee59c058feab5b_prof);

        
        $__internal_bb3d962208807ec43b47a04295808d7c519ce8baafc8f202df7fc62d5aa345a9->leave($__internal_bb3d962208807ec43b47a04295808d7c519ce8baafc8f202df7fc62d5aa345a9_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_bc79b316131fef2e3c2e80df2e31ce44fe543f09a561d14bdda75fecf8a6a0fe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bc79b316131fef2e3c2e80df2e31ce44fe543f09a561d14bdda75fecf8a6a0fe->enter($__internal_bc79b316131fef2e3c2e80df2e31ce44fe543f09a561d14bdda75fecf8a6a0fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_9d2fad07a383e134fc067b923c42140e6fb171f86944d377c8dfed360e22ae37 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9d2fad07a383e134fc067b923c42140e6fb171f86944d377c8dfed360e22ae37->enter($__internal_9d2fad07a383e134fc067b923c42140e6fb171f86944d377c8dfed360e22ae37_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 137, $this->getSourceContext()); })()), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) || array_key_exists("status_code", $context) ? $context["status_code"] : (function () { throw new Twig_Error_Runtime('Variable "status_code" does not exist.', 137, $this->getSourceContext()); })()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) || array_key_exists("status_text", $context) ? $context["status_text"] : (function () { throw new Twig_Error_Runtime('Variable "status_text" does not exist.', 137, $this->getSourceContext()); })()), "html", null, true);
        echo ")
";
        
        $__internal_9d2fad07a383e134fc067b923c42140e6fb171f86944d377c8dfed360e22ae37->leave($__internal_9d2fad07a383e134fc067b923c42140e6fb171f86944d377c8dfed360e22ae37_prof);

        
        $__internal_bc79b316131fef2e3c2e80df2e31ce44fe543f09a561d14bdda75fecf8a6a0fe->leave($__internal_bc79b316131fef2e3c2e80df2e31ce44fe543f09a561d14bdda75fecf8a6a0fe_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_af49c435471a6272a231181e17be25aa523f6cc168ee6c1c502c6a6c9ffe895c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_af49c435471a6272a231181e17be25aa523f6cc168ee6c1c502c6a6c9ffe895c->enter($__internal_af49c435471a6272a231181e17be25aa523f6cc168ee6c1c502c6a6c9ffe895c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_c4a1b3dfa5d6cd541eb3d8b54883f84c0ac2ce4b233b0f5d9cd22dc9f79b5cfc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c4a1b3dfa5d6cd541eb3d8b54883f84c0ac2ce4b233b0f5d9cd22dc9f79b5cfc->enter($__internal_c4a1b3dfa5d6cd541eb3d8b54883f84c0ac2ce4b233b0f5d9cd22dc9f79b5cfc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_c4a1b3dfa5d6cd541eb3d8b54883f84c0ac2ce4b233b0f5d9cd22dc9f79b5cfc->leave($__internal_c4a1b3dfa5d6cd541eb3d8b54883f84c0ac2ce4b233b0f5d9cd22dc9f79b5cfc_prof);

        
        $__internal_af49c435471a6272a231181e17be25aa523f6cc168ee6c1c502c6a6c9ffe895c->leave($__internal_af49c435471a6272a231181e17be25aa523f6cc168ee6c1c502c6a6c9ffe895c_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/home/elie/Gitlab/Symfony_Project/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
