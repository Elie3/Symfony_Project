<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_b8f9aa06991283aeeb11ef01da5388fe70af9036774ecedc4781950de3fd4634 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2ab5c615fe2216571e8cca799e40359641fadab87747f7bdf3d9d4ee8c98c76e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2ab5c615fe2216571e8cca799e40359641fadab87747f7bdf3d9d4ee8c98c76e->enter($__internal_2ab5c615fe2216571e8cca799e40359641fadab87747f7bdf3d9d4ee8c98c76e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        $__internal_a097a5ba826e6b924ad7d01c62cd1579e69957d98c21508c31943e6fceffcb57 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a097a5ba826e6b924ad7d01c62cd1579e69957d98c21508c31943e6fceffcb57->enter($__internal_a097a5ba826e6b924ad7d01c62cd1579e69957d98c21508c31943e6fceffcb57_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_include($this->env, $context, "@Twig/Exception/exception.txt.twig", array("exception" => (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 2, $this->getSourceContext()); })())));
        echo "
*/
";
        
        $__internal_2ab5c615fe2216571e8cca799e40359641fadab87747f7bdf3d9d4ee8c98c76e->leave($__internal_2ab5c615fe2216571e8cca799e40359641fadab87747f7bdf3d9d4ee8c98c76e_prof);

        
        $__internal_a097a5ba826e6b924ad7d01c62cd1579e69957d98c21508c31943e6fceffcb57->leave($__internal_a097a5ba826e6b924ad7d01c62cd1579e69957d98c21508c31943e6fceffcb57_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ include('@Twig/Exception/exception.txt.twig', { exception: exception }) }}
*/
", "TwigBundle:Exception:exception.css.twig", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.css.twig");
    }
}
