<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_f220b1d96f4976a86ba86ab5207af3bce7503c13f58062f994a602dcf4d07bff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e46bec8983acdce44f600f107d38a4a4d53ab84035c139880fa1222e901b5b44 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e46bec8983acdce44f600f107d38a4a4d53ab84035c139880fa1222e901b5b44->enter($__internal_e46bec8983acdce44f600f107d38a4a4d53ab84035c139880fa1222e901b5b44_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        $__internal_93d4eea79c8512e9da2818fd208b219bb5fdd52d1322ab92880d7d94f7967fda = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_93d4eea79c8512e9da2818fd208b219bb5fdd52d1322ab92880d7d94f7967fda->enter($__internal_93d4eea79c8512e9da2818fd208b219bb5fdd52d1322ab92880d7d94f7967fda_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_e46bec8983acdce44f600f107d38a4a4d53ab84035c139880fa1222e901b5b44->leave($__internal_e46bec8983acdce44f600f107d38a4a4d53ab84035c139880fa1222e901b5b44_prof);

        
        $__internal_93d4eea79c8512e9da2818fd208b219bb5fdd52d1322ab92880d7d94f7967fda->leave($__internal_93d4eea79c8512e9da2818fd208b219bb5fdd52d1322ab92880d7d94f7967fda_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
", "@Framework/Form/form_enctype.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_enctype.html.php");
    }
}
