<?php

/* @Framework/Form/choice_options.html.php */
class __TwigTemplate_e787e85796e2b332617542b7bf074e93cb15020e97a82c4a429bf524509275a5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3c6e6a178c825248621131f5568fdcbd77963c65065edbde246003dc2aa988e2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3c6e6a178c825248621131f5568fdcbd77963c65065edbde246003dc2aa988e2->enter($__internal_3c6e6a178c825248621131f5568fdcbd77963c65065edbde246003dc2aa988e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        $__internal_f9d922f78f2b812366ba390720ea82ba4285a640f4aa6cce80015336def64750 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f9d922f78f2b812366ba390720ea82ba4285a640f4aa6cce80015336def64750->enter($__internal_f9d922f78f2b812366ba390720ea82ba4285a640f4aa6cce80015336def64750_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
";
        
        $__internal_3c6e6a178c825248621131f5568fdcbd77963c65065edbde246003dc2aa988e2->leave($__internal_3c6e6a178c825248621131f5568fdcbd77963c65065edbde246003dc2aa988e2_prof);

        
        $__internal_f9d922f78f2b812366ba390720ea82ba4285a640f4aa6cce80015336def64750->leave($__internal_f9d922f78f2b812366ba390720ea82ba4285a640f4aa6cce80015336def64750_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_options.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
", "@Framework/Form/choice_options.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_options.html.php");
    }
}
