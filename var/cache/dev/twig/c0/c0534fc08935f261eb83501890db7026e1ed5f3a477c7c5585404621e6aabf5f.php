<?php

/* PlatypusBundle:User:all_users_admin.html.twig */
class __TwigTemplate_cb0b39f63a67dcb571fcdcfaf778cc305f95a34d0970da5a950cc7d09b16b88a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "PlatypusBundle:User:all_users_admin.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_324f810f0c9ac9c02200d781d1793ccd333468d18823f2044b116da56af3046e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_324f810f0c9ac9c02200d781d1793ccd333468d18823f2044b116da56af3046e->enter($__internal_324f810f0c9ac9c02200d781d1793ccd333468d18823f2044b116da56af3046e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:User:all_users_admin.html.twig"));

        $__internal_edb296f0a1a171c731c75e553aad04f07f7a01e909a113114c50e35c6d308714 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_edb296f0a1a171c731c75e553aad04f07f7a01e909a113114c50e35c6d308714->enter($__internal_edb296f0a1a171c731c75e553aad04f07f7a01e909a113114c50e35c6d308714_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:User:all_users_admin.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_324f810f0c9ac9c02200d781d1793ccd333468d18823f2044b116da56af3046e->leave($__internal_324f810f0c9ac9c02200d781d1793ccd333468d18823f2044b116da56af3046e_prof);

        
        $__internal_edb296f0a1a171c731c75e553aad04f07f7a01e909a113114c50e35c6d308714->leave($__internal_edb296f0a1a171c731c75e553aad04f07f7a01e909a113114c50e35c6d308714_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_e182e951490b740d3f88080bd5fab3164e278418bede8163dbd2a76dd059d14a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e182e951490b740d3f88080bd5fab3164e278418bede8163dbd2a76dd059d14a->enter($__internal_e182e951490b740d3f88080bd5fab3164e278418bede8163dbd2a76dd059d14a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_4dbf65f1c08604568d77c6382ef326e146543f9df25bb2edfd794a860b4dea94 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4dbf65f1c08604568d77c6382ef326e146543f9df25bb2edfd794a860b4dea94->enter($__internal_4dbf65f1c08604568d77c6382ef326e146543f9df25bb2edfd794a860b4dea94_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("style.css"), "html", null, true);
        echo "\"/>
<table>

<tr><td>Id</td><td>Username</td><td>Email</td><td>First Name</td><td>Last Name</td><td>Creation Date</td><td>Role</td></tr>
";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new Twig_Error_Runtime('Variable "users" does not exist.', 9, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["elem"]) {
            // line 10
            echo "<tr>
<td>";
            // line 11
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "id", array()), "html", null, true);
            echo "</td>
<td>";
            // line 12
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "username", array()), "html", null, true);
            echo "</td>
<td>";
            // line 13
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "email", array()), "html", null, true);
            echo "</td>
<td>";
            // line 14
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "firstName", array()), "html", null, true);
            echo "</td>
<td>";
            // line 15
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "lastName", array()), "html", null, true);
            echo "</td>
<td>";
            // line 16
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "creattionDate", array()), "Y-m-d"), "html", null, true);
            echo "</td>
<td>";
            // line 17
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "roles", array()), 0, array(), "array"), "html", null, true);
            echo "<td>
<td><a href=\"";
            // line 18
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("edit_a_user", array("id" => twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "id", array()))), "html", null, true);
            echo "\">edit this user</a></td>
<td><a href=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("delete_a_user", array("id" => twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "id", array()))), "html", null, true);
            echo "\">delete this user</a></td>

</tr>



";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['elem'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "
</table>
";
        
        $__internal_4dbf65f1c08604568d77c6382ef326e146543f9df25bb2edfd794a860b4dea94->leave($__internal_4dbf65f1c08604568d77c6382ef326e146543f9df25bb2edfd794a860b4dea94_prof);

        
        $__internal_e182e951490b740d3f88080bd5fab3164e278418bede8163dbd2a76dd059d14a->leave($__internal_e182e951490b740d3f88080bd5fab3164e278418bede8163dbd2a76dd059d14a_prof);

    }

    public function getTemplateName()
    {
        return "PlatypusBundle:User:all_users_admin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 26,  98 => 19,  94 => 18,  90 => 17,  86 => 16,  82 => 15,  78 => 14,  74 => 13,  70 => 12,  66 => 11,  63 => 10,  59 => 9,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"base.html.twig\" %}

{% block body%}

<link rel=\"stylesheet\" href=\"{{ asset('style.css')}}\"/>
<table>

<tr><td>Id</td><td>Username</td><td>Email</td><td>First Name</td><td>Last Name</td><td>Creation Date</td><td>Role</td></tr>
{% for elem in users%}
<tr>
<td>{{elem.id}}</td>
<td>{{elem.username}}</td>
<td>{{elem.email}}</td>
<td>{{elem.firstName}}</td>
<td>{{elem.lastName}}</td>
<td>{{elem.creattionDate|date('Y-m-d')}}</td>
<td>{{elem.roles[0]}}<td>
<td><a href=\"{{ path('edit_a_user', {'id': elem.id}) }}\">edit this user</a></td>
<td><a href=\"{{ path('delete_a_user', {'id': elem.id}) }}\">delete this user</a></td>

</tr>



{%endfor%}

</table>
{%endblock%}
", "PlatypusBundle:User:all_users_admin.html.twig", "/home/elie/Documents/rendu/PHP/pool_symfony/test/src/PlatypusBundle/Resources/views/User/all_users_admin.html.twig");
    }
}
