<?php

/* PlatypusBundle:User:my_page.html.twig */
class __TwigTemplate_4d7c99e959af0e8ddd6d81145d5818a0a3f381441a144555ed71e3621f3fa3dd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "PlatypusBundle:User:my_page.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fd56210af5ba105298e55f34a553bc75d1b0f4efcb87ebf222b02bca1ad20fcb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fd56210af5ba105298e55f34a553bc75d1b0f4efcb87ebf222b02bca1ad20fcb->enter($__internal_fd56210af5ba105298e55f34a553bc75d1b0f4efcb87ebf222b02bca1ad20fcb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:User:my_page.html.twig"));

        $__internal_bf48127337438e5e29bc8a4d295b2cc5ddb9fe3bf363e1a2a48984bf5f8b4488 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bf48127337438e5e29bc8a4d295b2cc5ddb9fe3bf363e1a2a48984bf5f8b4488->enter($__internal_bf48127337438e5e29bc8a4d295b2cc5ddb9fe3bf363e1a2a48984bf5f8b4488_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:User:my_page.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fd56210af5ba105298e55f34a553bc75d1b0f4efcb87ebf222b02bca1ad20fcb->leave($__internal_fd56210af5ba105298e55f34a553bc75d1b0f4efcb87ebf222b02bca1ad20fcb_prof);

        
        $__internal_bf48127337438e5e29bc8a4d295b2cc5ddb9fe3bf363e1a2a48984bf5f8b4488->leave($__internal_bf48127337438e5e29bc8a4d295b2cc5ddb9fe3bf363e1a2a48984bf5f8b4488_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_2033251c2c19a0fa7794fbd2a576e73fb8e1e6e5aef5b1b7944eb50f1ad7b84b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2033251c2c19a0fa7794fbd2a576e73fb8e1e6e5aef5b1b7944eb50f1ad7b84b->enter($__internal_2033251c2c19a0fa7794fbd2a576e73fb8e1e6e5aef5b1b7944eb50f1ad7b84b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_ef067222fc05db0a9ad828cc96578524583d627e4c3c5efa83cd7f7ccacb4ca6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ef067222fc05db0a9ad828cc96578524583d627e4c3c5efa83cd7f7ccacb4ca6->enter($__internal_ef067222fc05db0a9ad828cc96578524583d627e4c3c5efa83cd7f7ccacb4ca6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<a href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("edit_user");
        echo "\">Edit User</a>
<a href=\"";
        // line 5
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("list_user");
        echo "\">List all users</a>
<a href=\"";
        // line 6
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("view_all_posts");
        echo "\">List all posts</a>
<a href=\"";
        // line 7
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_new_post");
        echo "\">Create a new Post</a>



Hello ";
        // line 11
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new Twig_Error_Runtime('Variable "user" does not exist.', 11, $this->getSourceContext()); })()), "getUsername", array(), "method"), "html", null, true);
        echo "

";
        
        $__internal_ef067222fc05db0a9ad828cc96578524583d627e4c3c5efa83cd7f7ccacb4ca6->leave($__internal_ef067222fc05db0a9ad828cc96578524583d627e4c3c5efa83cd7f7ccacb4ca6_prof);

        
        $__internal_2033251c2c19a0fa7794fbd2a576e73fb8e1e6e5aef5b1b7944eb50f1ad7b84b->leave($__internal_2033251c2c19a0fa7794fbd2a576e73fb8e1e6e5aef5b1b7944eb50f1ad7b84b_prof);

    }

    public function getTemplateName()
    {
        return "PlatypusBundle:User:my_page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 11,  62 => 7,  58 => 6,  54 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"base.html.twig\" %}

{% block body%}
<a href=\"{{ path('edit_user') }}\">Edit User</a>
<a href=\"{{ path('list_user') }}\">List all users</a>
<a href=\"{{ path('view_all_posts') }}\">List all posts</a>
<a href=\"{{ path('create_new_post') }}\">Create a new Post</a>



Hello {{user.getUsername()}}

{%endblock%}
", "PlatypusBundle:User:my_page.html.twig", "/home/elie/Gitlab/Symfony_Project/src/PlatypusBundle/Resources/views/User/my_page.html.twig");
    }
}
