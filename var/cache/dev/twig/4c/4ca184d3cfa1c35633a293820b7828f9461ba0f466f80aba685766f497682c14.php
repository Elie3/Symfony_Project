<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_bc9dfcc7b73f6cfb25b1b75f863bd30c0a3fadc8be94f03dd51dad63ab97b5b7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_872f8c1390811776dac9e385e12978440f9593d0e3ff823fe0c6c0b6e9cc3d7a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_872f8c1390811776dac9e385e12978440f9593d0e3ff823fe0c6c0b6e9cc3d7a->enter($__internal_872f8c1390811776dac9e385e12978440f9593d0e3ff823fe0c6c0b6e9cc3d7a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        $__internal_60a14148fdf543ede0f907c9b627bcc89615426cafba2b35f514a60d83d2880f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_60a14148fdf543ede0f907c9b627bcc89615426cafba2b35f514a60d83d2880f->enter($__internal_60a14148fdf543ede0f907c9b627bcc89615426cafba2b35f514a60d83d2880f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_872f8c1390811776dac9e385e12978440f9593d0e3ff823fe0c6c0b6e9cc3d7a->leave($__internal_872f8c1390811776dac9e385e12978440f9593d0e3ff823fe0c6c0b6e9cc3d7a_prof);

        
        $__internal_60a14148fdf543ede0f907c9b627bcc89615426cafba2b35f514a60d83d2880f->leave($__internal_60a14148fdf543ede0f907c9b627bcc89615426cafba2b35f514a60d83d2880f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
", "@Framework/Form/search_widget.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/search_widget.html.php");
    }
}
