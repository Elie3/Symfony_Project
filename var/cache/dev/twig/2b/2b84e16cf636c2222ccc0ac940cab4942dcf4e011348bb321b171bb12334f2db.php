<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_594a2b01a8d79d1af45323d71bdf4776936e0432c244ceb0a77c29ff73d7f147 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ebed7eb3463889c2039e33451b2afe04c7ab8096b35a3151879a236f5d125c85 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ebed7eb3463889c2039e33451b2afe04c7ab8096b35a3151879a236f5d125c85->enter($__internal_ebed7eb3463889c2039e33451b2afe04c7ab8096b35a3151879a236f5d125c85_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        $__internal_13e77777081a6731539606d07955f44b32ec8b62a5e71b114ae66c8d3d29a806 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_13e77777081a6731539606d07955f44b32ec8b62a5e71b114ae66c8d3d29a806->enter($__internal_13e77777081a6731539606d07955f44b32ec8b62a5e71b114ae66c8d3d29a806_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_ebed7eb3463889c2039e33451b2afe04c7ab8096b35a3151879a236f5d125c85->leave($__internal_ebed7eb3463889c2039e33451b2afe04c7ab8096b35a3151879a236f5d125c85_prof);

        
        $__internal_13e77777081a6731539606d07955f44b32ec8b62a5e71b114ae66c8d3d29a806->leave($__internal_13e77777081a6731539606d07955f44b32ec8b62a5e71b114ae66c8d3d29a806_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
", "@Framework/Form/hidden_widget.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/hidden_widget.html.php");
    }
}
