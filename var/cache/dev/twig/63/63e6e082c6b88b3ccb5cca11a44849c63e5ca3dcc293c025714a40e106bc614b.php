<?php

/* @Framework/FormTable/form_row.html.php */
class __TwigTemplate_2c0bf4be0290bf58343fcf6e7d9901b2c5d567e9db02e78c01213c99f4fcea67 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9c85a944308371af4700b7d32c350accae9212ba5a4845d3e49d647161748c87 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9c85a944308371af4700b7d32c350accae9212ba5a4845d3e49d647161748c87->enter($__internal_9c85a944308371af4700b7d32c350accae9212ba5a4845d3e49d647161748c87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        $__internal_4db8bad1d72085cb76b44baf7c2745220b2f9e39c429d4b9eced1c6dbcc6223c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4db8bad1d72085cb76b44baf7c2745220b2f9e39c429d4b9eced1c6dbcc6223c->enter($__internal_4db8bad1d72085cb76b44baf7c2745220b2f9e39c429d4b9eced1c6dbcc6223c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_row.html.php"));

        // line 1
        echo "<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_9c85a944308371af4700b7d32c350accae9212ba5a4845d3e49d647161748c87->leave($__internal_9c85a944308371af4700b7d32c350accae9212ba5a4845d3e49d647161748c87_prof);

        
        $__internal_4db8bad1d72085cb76b44baf7c2745220b2f9e39c429d4b9eced1c6dbcc6223c->leave($__internal_4db8bad1d72085cb76b44baf7c2745220b2f9e39c429d4b9eced1c6dbcc6223c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td>
        <?php echo \$view['form']->label(\$form) ?>
    </td>
    <td>
        <?php echo \$view['form']->errors(\$form) ?>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/form_row.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/form_row.html.php");
    }
}
