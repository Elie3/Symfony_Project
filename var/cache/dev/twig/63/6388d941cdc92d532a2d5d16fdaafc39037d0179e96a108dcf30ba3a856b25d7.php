<?php

/* @Framework/Form/reset_widget.html.php */
class __TwigTemplate_7fe5a6ad8fa01ad323cf8c066ac7a9163f67a887863a1a6d2c64edc1ecd9e7c2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4e225f51983290785b0896aa2daa2ada491bd0507e272de8b10524043522aec1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4e225f51983290785b0896aa2daa2ada491bd0507e272de8b10524043522aec1->enter($__internal_4e225f51983290785b0896aa2daa2ada491bd0507e272de8b10524043522aec1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        $__internal_5dc57b3efe528b5f95f77cba3fadea34be59baeae3b83d4bb2e4a3781dfce08e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5dc57b3efe528b5f95f77cba3fadea34be59baeae3b83d4bb2e4a3781dfce08e->enter($__internal_5dc57b3efe528b5f95f77cba3fadea34be59baeae3b83d4bb2e4a3781dfce08e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/reset_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
";
        
        $__internal_4e225f51983290785b0896aa2daa2ada491bd0507e272de8b10524043522aec1->leave($__internal_4e225f51983290785b0896aa2daa2ada491bd0507e272de8b10524043522aec1_prof);

        
        $__internal_5dc57b3efe528b5f95f77cba3fadea34be59baeae3b83d4bb2e4a3781dfce08e->leave($__internal_5dc57b3efe528b5f95f77cba3fadea34be59baeae3b83d4bb2e4a3781dfce08e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/reset_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'button_widget', array('type' => isset(\$type) ? \$type : 'reset')) ?>
", "@Framework/Form/reset_widget.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/reset_widget.html.php");
    }
}
