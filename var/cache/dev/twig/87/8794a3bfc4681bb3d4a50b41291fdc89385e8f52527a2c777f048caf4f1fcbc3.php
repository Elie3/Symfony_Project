<?php

/* PlatypusBundle:Post:new.html.twig */
class __TwigTemplate_9998bb4dff7820a79e1297200acd9a261d85291fcb9daee068be51126e32b697 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "PlatypusBundle:Post:new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2c5acfad3b3fe566a318bdd1199c9c48b4f4107ffa756735c2f70c62f70ff743 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2c5acfad3b3fe566a318bdd1199c9c48b4f4107ffa756735c2f70c62f70ff743->enter($__internal_2c5acfad3b3fe566a318bdd1199c9c48b4f4107ffa756735c2f70c62f70ff743_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:Post:new.html.twig"));

        $__internal_5f4543013738a3cd5d3c58dd396f2391061fb6af85d41fade1afb81b889cf93f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5f4543013738a3cd5d3c58dd396f2391061fb6af85d41fade1afb81b889cf93f->enter($__internal_5f4543013738a3cd5d3c58dd396f2391061fb6af85d41fade1afb81b889cf93f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:Post:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2c5acfad3b3fe566a318bdd1199c9c48b4f4107ffa756735c2f70c62f70ff743->leave($__internal_2c5acfad3b3fe566a318bdd1199c9c48b4f4107ffa756735c2f70c62f70ff743_prof);

        
        $__internal_5f4543013738a3cd5d3c58dd396f2391061fb6af85d41fade1afb81b889cf93f->leave($__internal_5f4543013738a3cd5d3c58dd396f2391061fb6af85d41fade1afb81b889cf93f_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_064134a172399b870804cc713dcc175e43791eced1ca69412eb9f21d296c592a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_064134a172399b870804cc713dcc175e43791eced1ca69412eb9f21d296c592a->enter($__internal_064134a172399b870804cc713dcc175e43791eced1ca69412eb9f21d296c592a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_2a496671fa6da3be104e380f0ab0ff7c86078143dbde8c38b63316e1070cf07e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2a496671fa6da3be104e380f0ab0ff7c86078143dbde8c38b63316e1070cf07e->enter($__internal_2a496671fa6da3be104e380f0ab0ff7c86078143dbde8c38b63316e1070cf07e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["text"]) || array_key_exists("text", $context) ? $context["text"] : (function () { throw new Twig_Error_Runtime('Variable "text" does not exist.', 5, $this->getSourceContext()); })()), "html", null, true);
        echo "

";
        // line 7
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 7, $this->getSourceContext()); })()), 'form_start');
        echo "
";
        // line 8
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 8, $this->getSourceContext()); })()), 'widget');
        echo "
";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 9, $this->getSourceContext()); })()), 'form_end');
        echo "



";
        
        $__internal_2a496671fa6da3be104e380f0ab0ff7c86078143dbde8c38b63316e1070cf07e->leave($__internal_2a496671fa6da3be104e380f0ab0ff7c86078143dbde8c38b63316e1070cf07e_prof);

        
        $__internal_064134a172399b870804cc713dcc175e43791eced1ca69412eb9f21d296c592a->leave($__internal_064134a172399b870804cc713dcc175e43791eced1ca69412eb9f21d296c592a_prof);

    }

    public function getTemplateName()
    {
        return "PlatypusBundle:Post:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 9,  61 => 8,  57 => 7,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"base.html.twig\" %}

{% block body%}

{{text}}

{{ form_start(form) }}
{{ form_widget(form) }}
{{ form_end(form) }}



{% endblock %}
", "PlatypusBundle:Post:new.html.twig", "/home/elie/Documents/rendu/PHP/pool_symfony/test/src/PlatypusBundle/Resources/views/Post/new.html.twig");
    }
}
