<?php

/* @Framework/Form/collection_widget.html.php */
class __TwigTemplate_a2c4b10a48cc4dfde66f2fb516f9527ea093d840376ae78f19adac503c82b6ba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2d1055e4849d2f756813d27b60290c9de0c31c4e13f9fd67d7367bef4d9c81b3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2d1055e4849d2f756813d27b60290c9de0c31c4e13f9fd67d7367bef4d9c81b3->enter($__internal_2d1055e4849d2f756813d27b60290c9de0c31c4e13f9fd67d7367bef4d9c81b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        $__internal_b7c94f9aa5646df3d140dbf4cd029ba66e7a2553e5b48cb425552b925e00a458 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b7c94f9aa5646df3d140dbf4cd029ba66e7a2553e5b48cb425552b925e00a458->enter($__internal_b7c94f9aa5646df3d140dbf4cd029ba66e7a2553e5b48cb425552b925e00a458_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/collection_widget.html.php"));

        // line 1
        echo "<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
";
        
        $__internal_2d1055e4849d2f756813d27b60290c9de0c31c4e13f9fd67d7367bef4d9c81b3->leave($__internal_2d1055e4849d2f756813d27b60290c9de0c31c4e13f9fd67d7367bef4d9c81b3_prof);

        
        $__internal_b7c94f9aa5646df3d140dbf4cd029ba66e7a2553e5b48cb425552b925e00a458->leave($__internal_b7c94f9aa5646df3d140dbf4cd029ba66e7a2553e5b48cb425552b925e00a458_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/collection_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (isset(\$prototype)): ?>
    <?php \$attr['data-prototype'] = \$view->escape(\$view['form']->row(\$prototype)) ?>
<?php endif ?>
<?php echo \$view['form']->widget(\$form, array('attr' => \$attr)) ?>
", "@Framework/Form/collection_widget.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/collection_widget.html.php");
    }
}
