<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_d53ac12b3a0d48c7ccbcc34bd04455db5fdd25cc6511349fc8f530a4dd35d94c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e673d150e735a15604a7923db8190665ed484e45dbd7173ca7915ba53e949adf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e673d150e735a15604a7923db8190665ed484e45dbd7173ca7915ba53e949adf->enter($__internal_e673d150e735a15604a7923db8190665ed484e45dbd7173ca7915ba53e949adf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        $__internal_fa5aa10ad27a694bc053bea0969cf83b89549e6ade860a19ac71449575afb4ca = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fa5aa10ad27a694bc053bea0969cf83b89549e6ade860a19ac71449575afb4ca->enter($__internal_fa5aa10ad27a694bc053bea0969cf83b89549e6ade860a19ac71449575afb4ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_e673d150e735a15604a7923db8190665ed484e45dbd7173ca7915ba53e949adf->leave($__internal_e673d150e735a15604a7923db8190665ed484e45dbd7173ca7915ba53e949adf_prof);

        
        $__internal_fa5aa10ad27a694bc053bea0969cf83b89549e6ade860a19ac71449575afb4ca->leave($__internal_fa5aa10ad27a694bc053bea0969cf83b89549e6ade860a19ac71449575afb4ca_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
", "@Framework/Form/number_widget.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/number_widget.html.php");
    }
}
