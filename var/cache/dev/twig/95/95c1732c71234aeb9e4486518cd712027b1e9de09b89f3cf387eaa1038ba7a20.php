<?php

/* TwigBundle:Exception:error.rdf.twig */
class __TwigTemplate_b39b38123e72a4159b05653b130229b5d6c369aa44df7aa804a3dba68e8a59b7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_939522e31f3b3a4f02e0b239c17aa246c2abf496b83b1bd8239ebab18a6a7c85 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_939522e31f3b3a4f02e0b239c17aa246c2abf496b83b1bd8239ebab18a6a7c85->enter($__internal_939522e31f3b3a4f02e0b239c17aa246c2abf496b83b1bd8239ebab18a6a7c85_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.rdf.twig"));

        $__internal_da074f27b1e449e09c60fcc7c211d9e3cc3b2862f6db5aebaf8d4c9728a3e3b3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_da074f27b1e449e09c60fcc7c211d9e3cc3b2862f6db5aebaf8d4c9728a3e3b3->enter($__internal_da074f27b1e449e09c60fcc7c211d9e3cc3b2862f6db5aebaf8d4c9728a3e3b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.rdf.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/error.xml.twig");
        echo "
";
        
        $__internal_939522e31f3b3a4f02e0b239c17aa246c2abf496b83b1bd8239ebab18a6a7c85->leave($__internal_939522e31f3b3a4f02e0b239c17aa246c2abf496b83b1bd8239ebab18a6a7c85_prof);

        
        $__internal_da074f27b1e449e09c60fcc7c211d9e3cc3b2862f6db5aebaf8d4c9728a3e3b3->leave($__internal_da074f27b1e449e09c60fcc7c211d9e3cc3b2862f6db5aebaf8d4c9728a3e3b3_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/error.xml.twig') }}
", "TwigBundle:Exception:error.rdf.twig", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.rdf.twig");
    }
}
