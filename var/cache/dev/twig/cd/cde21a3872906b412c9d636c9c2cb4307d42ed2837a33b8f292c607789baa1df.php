<?php

/* @Framework/Form/widget_container_attributes.html.php */
class __TwigTemplate_d3a2d0253641e6e6c6a77d79e7b24693b81be975c66e39cb336bae7b3c809e14 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1582f052566e8d5998e830fc8c8d601ca814c893e50d371f9b9a64ff029f3c2f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1582f052566e8d5998e830fc8c8d601ca814c893e50d371f9b9a64ff029f3c2f->enter($__internal_1582f052566e8d5998e830fc8c8d601ca814c893e50d371f9b9a64ff029f3c2f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_container_attributes.html.php"));

        $__internal_a2645f5a99668b96a536daf05886028b242e48922eeea3f41debcc6fc8b2118d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a2645f5a99668b96a536daf05886028b242e48922eeea3f41debcc6fc8b2118d->enter($__internal_a2645f5a99668b96a536daf05886028b242e48922eeea3f41debcc6fc8b2118d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_container_attributes.html.php"));

        // line 1
        echo "<?php if (!empty(\$id)): ?>id=\"<?php echo \$view->escape(\$id) ?>\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $__internal_1582f052566e8d5998e830fc8c8d601ca814c893e50d371f9b9a64ff029f3c2f->leave($__internal_1582f052566e8d5998e830fc8c8d601ca814c893e50d371f9b9a64ff029f3c2f_prof);

        
        $__internal_a2645f5a99668b96a536daf05886028b242e48922eeea3f41debcc6fc8b2118d->leave($__internal_a2645f5a99668b96a536daf05886028b242e48922eeea3f41debcc6fc8b2118d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/widget_container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (!empty(\$id)): ?>id=\"<?php echo \$view->escape(\$id) ?>\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/widget_container_attributes.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/widget_container_attributes.html.php");
    }
}
