<?php

/* PlatypusBundle:User:all_users.html.twig */
class __TwigTemplate_c424c7f6c062cda86cef6adb0ff03379cea61b88c92e3d5bfa75caeb7a2c361a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "PlatypusBundle:User:all_users.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d738c81c7a95ee11f9288ffa2ceed6213cfd2f7a87c2ce9884cb371347bb8c44 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d738c81c7a95ee11f9288ffa2ceed6213cfd2f7a87c2ce9884cb371347bb8c44->enter($__internal_d738c81c7a95ee11f9288ffa2ceed6213cfd2f7a87c2ce9884cb371347bb8c44_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:User:all_users.html.twig"));

        $__internal_f9a788d51db35e1f7de8e9aa99adb584ee1da5e6ececea94e541fb95b3dfc10d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f9a788d51db35e1f7de8e9aa99adb584ee1da5e6ececea94e541fb95b3dfc10d->enter($__internal_f9a788d51db35e1f7de8e9aa99adb584ee1da5e6ececea94e541fb95b3dfc10d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:User:all_users.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d738c81c7a95ee11f9288ffa2ceed6213cfd2f7a87c2ce9884cb371347bb8c44->leave($__internal_d738c81c7a95ee11f9288ffa2ceed6213cfd2f7a87c2ce9884cb371347bb8c44_prof);

        
        $__internal_f9a788d51db35e1f7de8e9aa99adb584ee1da5e6ececea94e541fb95b3dfc10d->leave($__internal_f9a788d51db35e1f7de8e9aa99adb584ee1da5e6ececea94e541fb95b3dfc10d_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_35003ab6bdff6f0ae9b53f264664f66ed8052b125bd8ec7a742e1ab156ba5b49 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_35003ab6bdff6f0ae9b53f264664f66ed8052b125bd8ec7a742e1ab156ba5b49->enter($__internal_35003ab6bdff6f0ae9b53f264664f66ed8052b125bd8ec7a742e1ab156ba5b49_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_2fae3a32bfcb37209c605b74f981d86f696e8d2bbc598cf3a89d322533b299df = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2fae3a32bfcb37209c605b74f981d86f696e8d2bbc598cf3a89d322533b299df->enter($__internal_2fae3a32bfcb37209c605b74f981d86f696e8d2bbc598cf3a89d322533b299df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("style.css"), "html", null, true);
        echo "\"/>
<table>

<tr><td>Id</td><td>Username</td><td>Email</td><td>First Name</td><td>Last Name</td><td>Creation Date</td><td>Role</td></tr>
";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["users"]) || array_key_exists("users", $context) ? $context["users"] : (function () { throw new Twig_Error_Runtime('Variable "users" does not exist.', 9, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["elem"]) {
            // line 10
            echo "<tr>
<td>";
            // line 11
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "id", array()), "html", null, true);
            echo "</td>
<td>";
            // line 12
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "username", array()), "html", null, true);
            echo "</td>
<td>";
            // line 13
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "email", array()), "html", null, true);
            echo "</td>
<td>";
            // line 14
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "firstName", array()), "html", null, true);
            echo "</td>
<td>";
            // line 15
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "lastName", array()), "html", null, true);
            echo "</td>
<td>";
            // line 16
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "creattionDate", array()), "Y-m-d"), "html", null, true);
            echo "</td>
<td>";
            // line 17
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "roles", array()), 0, array(), "array"), "html", null, true);
            echo "<td>
</tr>



";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['elem'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "
</table>
";
        
        $__internal_2fae3a32bfcb37209c605b74f981d86f696e8d2bbc598cf3a89d322533b299df->leave($__internal_2fae3a32bfcb37209c605b74f981d86f696e8d2bbc598cf3a89d322533b299df_prof);

        
        $__internal_35003ab6bdff6f0ae9b53f264664f66ed8052b125bd8ec7a742e1ab156ba5b49->leave($__internal_35003ab6bdff6f0ae9b53f264664f66ed8052b125bd8ec7a742e1ab156ba5b49_prof);

    }

    public function getTemplateName()
    {
        return "PlatypusBundle:User:all_users.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 23,  90 => 17,  86 => 16,  82 => 15,  78 => 14,  74 => 13,  70 => 12,  66 => 11,  63 => 10,  59 => 9,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"base.html.twig\" %}

{% block body%}

<link rel=\"stylesheet\" href=\"{{ asset('style.css')}}\"/>
<table>

<tr><td>Id</td><td>Username</td><td>Email</td><td>First Name</td><td>Last Name</td><td>Creation Date</td><td>Role</td></tr>
{% for elem in users%}
<tr>
<td>{{elem.id}}</td>
<td>{{elem.username}}</td>
<td>{{elem.email}}</td>
<td>{{elem.firstName}}</td>
<td>{{elem.lastName}}</td>
<td>{{elem.creattionDate|date('Y-m-d')}}</td>
<td>{{elem.roles[0]}}<td>
</tr>



{%endfor%}

</table>
{%endblock%}
", "PlatypusBundle:User:all_users.html.twig", "/home/elie/Gitlab/Symfony_Project/src/PlatypusBundle/Resources/views/User/all_users.html.twig");
    }
}
