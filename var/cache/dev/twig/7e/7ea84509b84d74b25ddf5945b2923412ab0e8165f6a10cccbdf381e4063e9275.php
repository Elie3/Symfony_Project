<?php

/* @Twig/images/icon-minus-square.svg */
class __TwigTemplate_2cc398588b255bdd77d22bf423d0fff5716f7a7f29dfc4c6d16334f5ca59e0e6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1c901c22463d4f3d8c5faf964f05fd7c0042d339fde1ca34c82f6199fd054fd0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1c901c22463d4f3d8c5faf964f05fd7c0042d339fde1ca34c82f6199fd054fd0->enter($__internal_1c901c22463d4f3d8c5faf964f05fd7c0042d339fde1ca34c82f6199fd054fd0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-minus-square.svg"));

        $__internal_04ea9e6a14be29d14aa66c22d388ef308b01ea7206878237de6597597029bd28 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_04ea9e6a14be29d14aa66c22d388ef308b01ea7206878237de6597597029bd28->enter($__internal_04ea9e6a14be29d14aa66c22d388ef308b01ea7206878237de6597597029bd28_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/icon-minus-square.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960V832q0-26-19-45t-45-19H448q-26 0-45 19t-19 45v128q0 26 19 45t45 19h896q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5T1376 1664H416q-119 0-203.5-84.5T128 1376V416q0-119 84.5-203.5T416 128h960q119 0 203.5 84.5T1664 416z\"/></svg>
";
        
        $__internal_1c901c22463d4f3d8c5faf964f05fd7c0042d339fde1ca34c82f6199fd054fd0->leave($__internal_1c901c22463d4f3d8c5faf964f05fd7c0042d339fde1ca34c82f6199fd054fd0_prof);

        
        $__internal_04ea9e6a14be29d14aa66c22d388ef308b01ea7206878237de6597597029bd28->leave($__internal_04ea9e6a14be29d14aa66c22d388ef308b01ea7206878237de6597597029bd28_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/icon-minus-square.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M1408 960V832q0-26-19-45t-45-19H448q-26 0-45 19t-19 45v128q0 26 19 45t45 19h896q26 0 45-19t19-45zm256-544v960q0 119-84.5 203.5T1376 1664H416q-119 0-203.5-84.5T128 1376V416q0-119 84.5-203.5T416 128h960q119 0 203.5 84.5T1664 416z\"/></svg>
", "@Twig/images/icon-minus-square.svg", "/home/elie/Gitlab/Symfony_Project/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/images/icon-minus-square.svg");
    }
}
