<?php

/* @Framework/Form/password_widget.html.php */
class __TwigTemplate_2537eb5e88e769e9e9b6c2b18556a444fb49289c380ba9d4eb562b5412566244 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a4d73f3b7ba7835a5b35c501327009deab8f486cfd04e515527e0889f81fd626 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a4d73f3b7ba7835a5b35c501327009deab8f486cfd04e515527e0889f81fd626->enter($__internal_a4d73f3b7ba7835a5b35c501327009deab8f486cfd04e515527e0889f81fd626_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        $__internal_77b1fbca5b6e8f6f12a80506ec3b46af130587964050a966801b942c61786a1d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_77b1fbca5b6e8f6f12a80506ec3b46af130587964050a966801b942c61786a1d->enter($__internal_77b1fbca5b6e8f6f12a80506ec3b46af130587964050a966801b942c61786a1d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/password_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
";
        
        $__internal_a4d73f3b7ba7835a5b35c501327009deab8f486cfd04e515527e0889f81fd626->leave($__internal_a4d73f3b7ba7835a5b35c501327009deab8f486cfd04e515527e0889f81fd626_prof);

        
        $__internal_77b1fbca5b6e8f6f12a80506ec3b46af130587964050a966801b942c61786a1d->leave($__internal_77b1fbca5b6e8f6f12a80506ec3b46af130587964050a966801b942c61786a1d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/password_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'password')) ?>
", "@Framework/Form/password_widget.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/password_widget.html.php");
    }
}
