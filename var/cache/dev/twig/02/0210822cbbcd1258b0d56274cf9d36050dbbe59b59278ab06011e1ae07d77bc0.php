<?php

/* @Framework/FormTable/button_row.html.php */
class __TwigTemplate_84e585cbef8028d7eea6b9840978363163df63b989c44136df256fb7cadea5bc extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e59939856650f070be37de476bcf4a37bca2da002a116968216b6c8b97e88838 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e59939856650f070be37de476bcf4a37bca2da002a116968216b6c8b97e88838->enter($__internal_e59939856650f070be37de476bcf4a37bca2da002a116968216b6c8b97e88838_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        $__internal_f1b345aab659eb7f991415150007f5ba4e29c1f6f3a215ffe48f9408e8c8ad7d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f1b345aab659eb7f991415150007f5ba4e29c1f6f3a215ffe48f9408e8c8ad7d->enter($__internal_f1b345aab659eb7f991415150007f5ba4e29c1f6f3a215ffe48f9408e8c8ad7d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/button_row.html.php"));

        // line 1
        echo "<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_e59939856650f070be37de476bcf4a37bca2da002a116968216b6c8b97e88838->leave($__internal_e59939856650f070be37de476bcf4a37bca2da002a116968216b6c8b97e88838_prof);

        
        $__internal_f1b345aab659eb7f991415150007f5ba4e29c1f6f3a215ffe48f9408e8c8ad7d->leave($__internal_f1b345aab659eb7f991415150007f5ba4e29c1f6f3a215ffe48f9408e8c8ad7d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr>
    <td></td>
    <td>
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/button_row.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/button_row.html.php");
    }
}
