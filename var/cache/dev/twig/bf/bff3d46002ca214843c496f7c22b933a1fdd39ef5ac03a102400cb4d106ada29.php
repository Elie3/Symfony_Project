<?php

/* @Framework/Form/textarea_widget.html.php */
class __TwigTemplate_9d53efc76f6c8fbd8360fdf060ae33c49b2a41285a12e4d39e71494e6af250a6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_042780d3c1c4779af2bc05965f65b77b675fd9ba4b0aa351f6d10ecb6a0b93f5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_042780d3c1c4779af2bc05965f65b77b675fd9ba4b0aa351f6d10ecb6a0b93f5->enter($__internal_042780d3c1c4779af2bc05965f65b77b675fd9ba4b0aa351f6d10ecb6a0b93f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        $__internal_69d926a39d60adb4fa5e685bed666b358b126bc1d630da57f6d404a0088be37a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_69d926a39d60adb4fa5e685bed666b358b126bc1d630da57f6d404a0088be37a->enter($__internal_69d926a39d60adb4fa5e685bed666b358b126bc1d630da57f6d404a0088be37a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/textarea_widget.html.php"));

        // line 1
        echo "<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
";
        
        $__internal_042780d3c1c4779af2bc05965f65b77b675fd9ba4b0aa351f6d10ecb6a0b93f5->leave($__internal_042780d3c1c4779af2bc05965f65b77b675fd9ba4b0aa351f6d10ecb6a0b93f5_prof);

        
        $__internal_69d926a39d60adb4fa5e685bed666b358b126bc1d630da57f6d404a0088be37a->leave($__internal_69d926a39d60adb4fa5e685bed666b358b126bc1d630da57f6d404a0088be37a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/textarea_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<textarea <?php echo \$view['form']->block(\$form, 'widget_attributes') ?>><?php echo \$view->escape(\$value) ?></textarea>
", "@Framework/Form/textarea_widget.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/textarea_widget.html.php");
    }
}
