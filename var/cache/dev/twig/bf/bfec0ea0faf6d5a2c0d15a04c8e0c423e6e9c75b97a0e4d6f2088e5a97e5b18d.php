<?php

/* @Framework/Form/form.html.php */
class __TwigTemplate_dab7b52c5f9cb5ad1767435ae39270c3fa413844b8dd3adecc2f4118f98f0480 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_65b14e161106f8894cef261b92e1ad3854bcd6f1f021b6308f749cb6d7d448d3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_65b14e161106f8894cef261b92e1ad3854bcd6f1f021b6308f749cb6d7d448d3->enter($__internal_65b14e161106f8894cef261b92e1ad3854bcd6f1f021b6308f749cb6d7d448d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        $__internal_cac5746e964ddb0b16693d2f7d6f65223fb4837f68c3b138d03c78cbe71390bb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cac5746e964ddb0b16693d2f7d6f65223fb4837f68c3b138d03c78cbe71390bb->enter($__internal_cac5746e964ddb0b16693d2f7d6f65223fb4837f68c3b138d03c78cbe71390bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form.html.php"));

        // line 1
        echo "<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
";
        
        $__internal_65b14e161106f8894cef261b92e1ad3854bcd6f1f021b6308f749cb6d7d448d3->leave($__internal_65b14e161106f8894cef261b92e1ad3854bcd6f1f021b6308f749cb6d7d448d3_prof);

        
        $__internal_cac5746e964ddb0b16693d2f7d6f65223fb4837f68c3b138d03c78cbe71390bb->leave($__internal_cac5746e964ddb0b16693d2f7d6f65223fb4837f68c3b138d03c78cbe71390bb_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->start(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
<?php echo \$view['form']->end(\$form) ?>
", "@Framework/Form/form.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form.html.php");
    }
}
