<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_34edeb018be0771bbd604a513881fa88fecf2c79350b34f034d9c7b129de1b14 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bb794b7324bbd9464c793e85bd2ab64e69518994b6c220e69126e89122e58ba7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bb794b7324bbd9464c793e85bd2ab64e69518994b6c220e69126e89122e58ba7->enter($__internal_bb794b7324bbd9464c793e85bd2ab64e69518994b6c220e69126e89122e58ba7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        $__internal_c6bbf4fbb86216e7a96a4f39ebde331ae644d298dfdb01a6e55af40bc8cd51e2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c6bbf4fbb86216e7a96a4f39ebde331ae644d298dfdb01a6e55af40bc8cd51e2->enter($__internal_c6bbf4fbb86216e7a96a4f39ebde331ae644d298dfdb01a6e55af40bc8cd51e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/exception.xml.twig", array("exception" => (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 1, $this->getSourceContext()); })())));
        echo "
";
        
        $__internal_bb794b7324bbd9464c793e85bd2ab64e69518994b6c220e69126e89122e58ba7->leave($__internal_bb794b7324bbd9464c793e85bd2ab64e69518994b6c220e69126e89122e58ba7_prof);

        
        $__internal_c6bbf4fbb86216e7a96a4f39ebde331ae644d298dfdb01a6e55af40bc8cd51e2->leave($__internal_c6bbf4fbb86216e7a96a4f39ebde331ae644d298dfdb01a6e55af40bc8cd51e2_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/exception.xml.twig', { exception: exception }) }}
", "TwigBundle:Exception:exception.rdf.twig", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.rdf.twig");
    }
}
