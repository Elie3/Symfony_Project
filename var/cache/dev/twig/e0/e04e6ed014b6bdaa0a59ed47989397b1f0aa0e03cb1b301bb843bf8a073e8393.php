<?php

/* @Framework/FormTable/form_widget_compound.html.php */
class __TwigTemplate_70b0f8010ad22ce4ab976f5661a1aeb9d22f01fb58d3c3a1ed4e6596af693a10 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3687fae1088fefca9733bc18cafe4e8cac0321f54034726b6bce554bd18863e1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3687fae1088fefca9733bc18cafe4e8cac0321f54034726b6bce554bd18863e1->enter($__internal_3687fae1088fefca9733bc18cafe4e8cac0321f54034726b6bce554bd18863e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        $__internal_d4c6186e8d16ae4a305d7df1510f94bc3d65efacf5ecacfb31a4e2cbc8e5138c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d4c6186e8d16ae4a305d7df1510f94bc3d65efacf5ecacfb31a4e2cbc8e5138c->enter($__internal_d4c6186e8d16ae4a305d7df1510f94bc3d65efacf5ecacfb31a4e2cbc8e5138c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        // line 1
        echo "<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
";
        
        $__internal_3687fae1088fefca9733bc18cafe4e8cac0321f54034726b6bce554bd18863e1->leave($__internal_3687fae1088fefca9733bc18cafe4e8cac0321f54034726b6bce554bd18863e1_prof);

        
        $__internal_d4c6186e8d16ae4a305d7df1510f94bc3d65efacf5ecacfb31a4e2cbc8e5138c->leave($__internal_d4c6186e8d16ae4a305d7df1510f94bc3d65efacf5ecacfb31a4e2cbc8e5138c_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form) ?>
        </td>
    </tr>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</table>
", "@Framework/FormTable/form_widget_compound.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/form_widget_compound.html.php");
    }
}
