<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_b2dcc25782919c673c591183009effac821ddcbf244228cc2124c03d91ab46c8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_31e80cc890179a0367b09e4bad286796d197fe9c12ce7bb29ba5164ef45be01c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_31e80cc890179a0367b09e4bad286796d197fe9c12ce7bb29ba5164ef45be01c->enter($__internal_31e80cc890179a0367b09e4bad286796d197fe9c12ce7bb29ba5164ef45be01c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_123e658388b8435ccc7ffaf0497d22b3cf83e219d5c69fec0615c0ed61907d99 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_123e658388b8435ccc7ffaf0497d22b3cf83e219d5c69fec0615c0ed61907d99->enter($__internal_123e658388b8435ccc7ffaf0497d22b3cf83e219d5c69fec0615c0ed61907d99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_31e80cc890179a0367b09e4bad286796d197fe9c12ce7bb29ba5164ef45be01c->leave($__internal_31e80cc890179a0367b09e4bad286796d197fe9c12ce7bb29ba5164ef45be01c_prof);

        
        $__internal_123e658388b8435ccc7ffaf0497d22b3cf83e219d5c69fec0615c0ed61907d99->leave($__internal_123e658388b8435ccc7ffaf0497d22b3cf83e219d5c69fec0615c0ed61907d99_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_07a9211bcc5644ba0332ec68457cdccf6d41a8c233ac9d7d492814dec1901e04 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_07a9211bcc5644ba0332ec68457cdccf6d41a8c233ac9d7d492814dec1901e04->enter($__internal_07a9211bcc5644ba0332ec68457cdccf6d41a8c233ac9d7d492814dec1901e04_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_616c2809427b0f0ddb49ad77a5c28f8d0d2fe0ab9104138262ecf2602f658fe7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_616c2809427b0f0ddb49ad77a5c28f8d0d2fe0ab9104138262ecf2602f658fe7->enter($__internal_616c2809427b0f0ddb49ad77a5c28f8d0d2fe0ab9104138262ecf2602f658fe7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_616c2809427b0f0ddb49ad77a5c28f8d0d2fe0ab9104138262ecf2602f658fe7->leave($__internal_616c2809427b0f0ddb49ad77a5c28f8d0d2fe0ab9104138262ecf2602f658fe7_prof);

        
        $__internal_07a9211bcc5644ba0332ec68457cdccf6d41a8c233ac9d7d492814dec1901e04->leave($__internal_07a9211bcc5644ba0332ec68457cdccf6d41a8c233ac9d7d492814dec1901e04_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_69e893e9e745a7e087b04a1511a1df0b2563d832e6274a0fd98953952af6172c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_69e893e9e745a7e087b04a1511a1df0b2563d832e6274a0fd98953952af6172c->enter($__internal_69e893e9e745a7e087b04a1511a1df0b2563d832e6274a0fd98953952af6172c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_997e5038c84e2fe36ed5133c7f6c0bf7bc08d6926f4583e887a4038f0ffb2f59 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_997e5038c84e2fe36ed5133c7f6c0bf7bc08d6926f4583e887a4038f0ffb2f59->enter($__internal_997e5038c84e2fe36ed5133c7f6c0bf7bc08d6926f4583e887a4038f0ffb2f59_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_997e5038c84e2fe36ed5133c7f6c0bf7bc08d6926f4583e887a4038f0ffb2f59->leave($__internal_997e5038c84e2fe36ed5133c7f6c0bf7bc08d6926f4583e887a4038f0ffb2f59_prof);

        
        $__internal_69e893e9e745a7e087b04a1511a1df0b2563d832e6274a0fd98953952af6172c->leave($__internal_69e893e9e745a7e087b04a1511a1df0b2563d832e6274a0fd98953952af6172c_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_5729622d34776ebdb75b8eb2f8592fb6b89b4c0bb27487fd455c4aaa11ed41b1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5729622d34776ebdb75b8eb2f8592fb6b89b4c0bb27487fd455c4aaa11ed41b1->enter($__internal_5729622d34776ebdb75b8eb2f8592fb6b89b4c0bb27487fd455c4aaa11ed41b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_32a7ba064502da7d315e96edfe1e5d351e92c5069eb96368c4cfe7845d201588 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_32a7ba064502da7d315e96edfe1e5d351e92c5069eb96368c4cfe7845d201588->enter($__internal_32a7ba064502da7d315e96edfe1e5d351e92c5069eb96368c4cfe7845d201588_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 13, $this->getSourceContext()); })()))));
        echo "
";
        
        $__internal_32a7ba064502da7d315e96edfe1e5d351e92c5069eb96368c4cfe7845d201588->leave($__internal_32a7ba064502da7d315e96edfe1e5d351e92c5069eb96368c4cfe7845d201588_prof);

        
        $__internal_5729622d34776ebdb75b8eb2f8592fb6b89b4c0bb27487fd455c4aaa11ed41b1->leave($__internal_5729622d34776ebdb75b8eb2f8592fb6b89b4c0bb27487fd455c4aaa11ed41b1_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/home/elie/Gitlab/Symfony_Project/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
