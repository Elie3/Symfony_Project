<?php

/* @Framework/Form/form_widget.html.php */
class __TwigTemplate_4c5e398cf32ecdcd0d964c7b0985dafb1ca5328db16ef1aeea780a91cafd3d49 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_030d7d74767baa8de28850113d08f0305e409a5794e2a42e02916c898f85b6fe = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_030d7d74767baa8de28850113d08f0305e409a5794e2a42e02916c898f85b6fe->enter($__internal_030d7d74767baa8de28850113d08f0305e409a5794e2a42e02916c898f85b6fe_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        $__internal_7a87fa74fea6e9434cfc17cb88694ea2c013ae360492943dc14682ef87df867e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7a87fa74fea6e9434cfc17cb88694ea2c013ae360492943dc14682ef87df867e->enter($__internal_7a87fa74fea6e9434cfc17cb88694ea2c013ae360492943dc14682ef87df867e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget.html.php"));

        // line 1
        echo "<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
";
        
        $__internal_030d7d74767baa8de28850113d08f0305e409a5794e2a42e02916c898f85b6fe->leave($__internal_030d7d74767baa8de28850113d08f0305e409a5794e2a42e02916c898f85b6fe_prof);

        
        $__internal_7a87fa74fea6e9434cfc17cb88694ea2c013ae360492943dc14682ef87df867e->leave($__internal_7a87fa74fea6e9434cfc17cb88694ea2c013ae360492943dc14682ef87df867e_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$compound): ?>
<?php echo \$view['form']->block(\$form, 'form_widget_compound')?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'form_widget_simple')?>
<?php endif ?>
", "@Framework/Form/form_widget.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_widget.html.php");
    }
}
