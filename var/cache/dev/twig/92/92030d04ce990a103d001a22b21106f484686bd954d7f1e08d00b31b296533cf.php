<?php

/* PlatypusBundle:Default:layout.html.twig */
class __TwigTemplate_732c0267aa1d7cbf48eceebf54c9e6812fdb95125a8252960b0d50429e003549 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "PlatypusBundle:Default:layout.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ab83945416d3c6bb55ee47b137088de3df6c691f785c4e5bc1f5a702c4c860cf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ab83945416d3c6bb55ee47b137088de3df6c691f785c4e5bc1f5a702c4c860cf->enter($__internal_ab83945416d3c6bb55ee47b137088de3df6c691f785c4e5bc1f5a702c4c860cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:Default:layout.html.twig"));

        $__internal_be94a470a1b90e31660277cac912c22af5b33b52612d29b0ee8ff0f33ba2f21c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_be94a470a1b90e31660277cac912c22af5b33b52612d29b0ee8ff0f33ba2f21c->enter($__internal_be94a470a1b90e31660277cac912c22af5b33b52612d29b0ee8ff0f33ba2f21c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:Default:layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ab83945416d3c6bb55ee47b137088de3df6c691f785c4e5bc1f5a702c4c860cf->leave($__internal_ab83945416d3c6bb55ee47b137088de3df6c691f785c4e5bc1f5a702c4c860cf_prof);

        
        $__internal_be94a470a1b90e31660277cac912c22af5b33b52612d29b0ee8ff0f33ba2f21c->leave($__internal_be94a470a1b90e31660277cac912c22af5b33b52612d29b0ee8ff0f33ba2f21c_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_40124e41af104605c2f11a9a2e8b98bd325f09836ed8af300bb0e8b09e3459ce = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_40124e41af104605c2f11a9a2e8b98bd325f09836ed8af300bb0e8b09e3459ce->enter($__internal_40124e41af104605c2f11a9a2e8b98bd325f09836ed8af300bb0e8b09e3459ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_fda54ea9158a5a2f21d13b1560364a08d70f107a684c1f6e72e89035130bcf6c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fda54ea9158a5a2f21d13b1560364a08d70f107a684c1f6e72e89035130bcf6c->enter($__internal_fda54ea9158a5a2f21d13b1560364a08d70f107a684c1f6e72e89035130bcf6c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_fda54ea9158a5a2f21d13b1560364a08d70f107a684c1f6e72e89035130bcf6c->leave($__internal_fda54ea9158a5a2f21d13b1560364a08d70f107a684c1f6e72e89035130bcf6c_prof);

        
        $__internal_40124e41af104605c2f11a9a2e8b98bd325f09836ed8af300bb0e8b09e3459ce->leave($__internal_40124e41af104605c2f11a9a2e8b98bd325f09836ed8af300bb0e8b09e3459ce_prof);

    }

    public function getTemplateName()
    {
        return "PlatypusBundle:Default:layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 2,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"base.html.twig\" %}
{% block body %}{% endblock %}
", "PlatypusBundle:Default:layout.html.twig", "/home/elie/Documents/rendu/PHP/pool_symfony/test/src/PlatypusBundle/Resources/views/Default/layout.html.twig");
    }
}
