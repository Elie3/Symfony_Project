<?php

/* @WebProfiler/Icon/yes.svg */
class __TwigTemplate_9a7a765c24d4cf9b911af77e6ffed4bcc3e2af09253d7f3184988ccb6a4ab253 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bb540ba4f4c77c4b9a3fc88192e545e41fb08b851626d3a67eea2b3674e0e278 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bb540ba4f4c77c4b9a3fc88192e545e41fb08b851626d3a67eea2b3674e0e278->enter($__internal_bb540ba4f4c77c4b9a3fc88192e545e41fb08b851626d3a67eea2b3674e0e278_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/yes.svg"));

        $__internal_c64b92f4cfdcb863f7d7e7ff6f3072da8e56ba5bb880fd54077c88298d720747 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c64b92f4cfdcb863f7d7e7ff6f3072da8e56ba5bb880fd54077c88298d720747->enter($__internal_c64b92f4cfdcb863f7d7e7ff6f3072da8e56ba5bb880fd54077c88298d720747_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Icon/yes.svg"));

        // line 1
        echo "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"28\" height=\"28\" viewBox=\"0 0 12 12\" enable-background=\"new 0 0 12 12\" xml:space=\"preserve\">
    <path fill=\"#5E976E\" d=\"M12,3.1c0,0.4-0.1,0.8-0.4,1.1L5.9,9.8c-0.3,0.3-0.6,0.4-1,0.4c-0.4,0-0.7-0.1-1-0.4L0.4,6.3
    C0.1,6,0,5.6,0,5.2c0-0.4,0.2-0.7,0.4-0.9C0.6,4,1,3.9,1.3,3.9c0.4,0,0.8,0.1,1.1,0.4l2.5,2.5l4.7-4.7c0.3-0.3,0.7-0.4,1-0.4
    c0.4,0,0.7,0.2,0.9,0.4C11.8,2.4,12,2.7,12,3.1z\"/>
</svg>
";
        
        $__internal_bb540ba4f4c77c4b9a3fc88192e545e41fb08b851626d3a67eea2b3674e0e278->leave($__internal_bb540ba4f4c77c4b9a3fc88192e545e41fb08b851626d3a67eea2b3674e0e278_prof);

        
        $__internal_c64b92f4cfdcb863f7d7e7ff6f3072da8e56ba5bb880fd54077c88298d720747->leave($__internal_c64b92f4cfdcb863f7d7e7ff6f3072da8e56ba5bb880fd54077c88298d720747_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Icon/yes.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"28\" height=\"28\" viewBox=\"0 0 12 12\" enable-background=\"new 0 0 12 12\" xml:space=\"preserve\">
    <path fill=\"#5E976E\" d=\"M12,3.1c0,0.4-0.1,0.8-0.4,1.1L5.9,9.8c-0.3,0.3-0.6,0.4-1,0.4c-0.4,0-0.7-0.1-1-0.4L0.4,6.3
    C0.1,6,0,5.6,0,5.2c0-0.4,0.2-0.7,0.4-0.9C0.6,4,1,3.9,1.3,3.9c0.4,0,0.8,0.1,1.1,0.4l2.5,2.5l4.7-4.7c0.3-0.3,0.7-0.4,1-0.4
    c0.4,0,0.7,0.2,0.9,0.4C11.8,2.4,12,2.7,12,3.1z\"/>
</svg>
", "@WebProfiler/Icon/yes.svg", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Icon/yes.svg");
    }
}
