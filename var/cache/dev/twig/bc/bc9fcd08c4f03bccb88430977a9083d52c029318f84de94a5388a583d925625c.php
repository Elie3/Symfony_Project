<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_b11e71f19d4dd921d6334c419fce5f06b6dce35e3c1de3b24c8b836f709c2c15 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7cdde9a106eaeb8cd8b3493c28fccf91068412d514ba30ea531a109126f9da2d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7cdde9a106eaeb8cd8b3493c28fccf91068412d514ba30ea531a109126f9da2d->enter($__internal_7cdde9a106eaeb8cd8b3493c28fccf91068412d514ba30ea531a109126f9da2d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $__internal_a68da2a50b4dadb91a1b0937c6f6972d9cf514d8c98b09c9bdd21c1d3a1cea2a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a68da2a50b4dadb91a1b0937c6f6972d9cf514d8c98b09c9bdd21c1d3a1cea2a->enter($__internal_a68da2a50b4dadb91a1b0937c6f6972d9cf514d8c98b09c9bdd21c1d3a1cea2a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7cdde9a106eaeb8cd8b3493c28fccf91068412d514ba30ea531a109126f9da2d->leave($__internal_7cdde9a106eaeb8cd8b3493c28fccf91068412d514ba30ea531a109126f9da2d_prof);

        
        $__internal_a68da2a50b4dadb91a1b0937c6f6972d9cf514d8c98b09c9bdd21c1d3a1cea2a->leave($__internal_a68da2a50b4dadb91a1b0937c6f6972d9cf514d8c98b09c9bdd21c1d3a1cea2a_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_eb79f05034191db7425c5f6da317c607870ad4cba9bf07429b29fc4f77710e33 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eb79f05034191db7425c5f6da317c607870ad4cba9bf07429b29fc4f77710e33->enter($__internal_eb79f05034191db7425c5f6da317c607870ad4cba9bf07429b29fc4f77710e33_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_87e5dccaddf0bb508959aa2285d4ae7e49eed41cca9eb1fb722dfcd525c83b17 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_87e5dccaddf0bb508959aa2285d4ae7e49eed41cca9eb1fb722dfcd525c83b17->enter($__internal_87e5dccaddf0bb508959aa2285d4ae7e49eed41cca9eb1fb722dfcd525c83b17_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_87e5dccaddf0bb508959aa2285d4ae7e49eed41cca9eb1fb722dfcd525c83b17->leave($__internal_87e5dccaddf0bb508959aa2285d4ae7e49eed41cca9eb1fb722dfcd525c83b17_prof);

        
        $__internal_eb79f05034191db7425c5f6da317c607870ad4cba9bf07429b29fc4f77710e33->leave($__internal_eb79f05034191db7425c5f6da317c607870ad4cba9bf07429b29fc4f77710e33_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_70e16ba256fea644cb95052a3e20979f614ac9f69f3d022c594b4912b002d4e3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_70e16ba256fea644cb95052a3e20979f614ac9f69f3d022c594b4912b002d4e3->enter($__internal_70e16ba256fea644cb95052a3e20979f614ac9f69f3d022c594b4912b002d4e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_eb0a9a16fba3c6e7cd583cec6d2a8eed58178c1d16a9c0a6307c56ce4200c66f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eb0a9a16fba3c6e7cd583cec6d2a8eed58178c1d16a9c0a6307c56ce4200c66f->enter($__internal_eb0a9a16fba3c6e7cd583cec6d2a8eed58178c1d16a9c0a6307c56ce4200c66f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) || array_key_exists("location", $context) ? $context["location"] : (function () { throw new Twig_Error_Runtime('Variable "location" does not exist.', 8, $this->getSourceContext()); })()), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) || array_key_exists("location", $context) ? $context["location"] : (function () { throw new Twig_Error_Runtime('Variable "location" does not exist.', 8, $this->getSourceContext()); })()), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_eb0a9a16fba3c6e7cd583cec6d2a8eed58178c1d16a9c0a6307c56ce4200c66f->leave($__internal_eb0a9a16fba3c6e7cd583cec6d2a8eed58178c1d16a9c0a6307c56ce4200c66f_prof);

        
        $__internal_70e16ba256fea644cb95052a3e20979f614ac9f69f3d022c594b4912b002d4e3->leave($__internal_70e16ba256fea644cb95052a3e20979f614ac9f69f3d022c594b4912b002d4e3_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 8,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/toolbar_redirect.html.twig");
    }
}
