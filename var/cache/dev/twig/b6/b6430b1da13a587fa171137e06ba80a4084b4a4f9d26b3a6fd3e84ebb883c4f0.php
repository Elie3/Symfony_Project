<?php

/* PlatypusBundle:User:login.html.twig */
class __TwigTemplate_7e3e0b8f3f4610bdd604744491ec7d2f893700cee6774f74298afe2c9952758b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 3
        $this->parent = $this->loadTemplate("base.html.twig", "PlatypusBundle:User:login.html.twig", 3);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d2351f3e641169adcb3413347b47c537985dfb12db5d883cf0977a5dd508384e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d2351f3e641169adcb3413347b47c537985dfb12db5d883cf0977a5dd508384e->enter($__internal_d2351f3e641169adcb3413347b47c537985dfb12db5d883cf0977a5dd508384e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:User:login.html.twig"));

        $__internal_220bd7ded3b90ef21fbe65054c82669eb0c93a121a3fcc492ead0f726631743f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_220bd7ded3b90ef21fbe65054c82669eb0c93a121a3fcc492ead0f726631743f->enter($__internal_220bd7ded3b90ef21fbe65054c82669eb0c93a121a3fcc492ead0f726631743f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:User:login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d2351f3e641169adcb3413347b47c537985dfb12db5d883cf0977a5dd508384e->leave($__internal_d2351f3e641169adcb3413347b47c537985dfb12db5d883cf0977a5dd508384e_prof);

        
        $__internal_220bd7ded3b90ef21fbe65054c82669eb0c93a121a3fcc492ead0f726631743f->leave($__internal_220bd7ded3b90ef21fbe65054c82669eb0c93a121a3fcc492ead0f726631743f_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_19f100d8578c2cdd9552411c10edf85bca9de5b2ca9213e991f87090afa9c6be = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_19f100d8578c2cdd9552411c10edf85bca9de5b2ca9213e991f87090afa9c6be->enter($__internal_19f100d8578c2cdd9552411c10edf85bca9de5b2ca9213e991f87090afa9c6be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_9df3aa27d6cc477890e6536b838d2f88f7aa1cfe18b2a324c98949918bd4b747 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9df3aa27d6cc477890e6536b838d2f88f7aa1cfe18b2a324c98949918bd4b747->enter($__internal_9df3aa27d6cc477890e6536b838d2f88f7aa1cfe18b2a324c98949918bd4b747_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
";
        // line 7
        if ((isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 7, $this->getSourceContext()); })())) {
            // line 8
            echo "    <div>";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 8, $this->getSourceContext()); })()), "messageKey", array()), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new Twig_Error_Runtime('Variable "error" does not exist.', 8, $this->getSourceContext()); })()), "messageData", array()), "security"), "html", null, true);
            echo "</div>
";
        }
        // line 10
        echo "
<a href=\"";
        // line 11
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("register");
        echo "\">Register</a>


<form action=\"";
        // line 14
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("login");
        echo "\" method=\"post\">
    <label for=\"username\">Username:</label>
    <input type=\"text\" id=\"username\" name=\"_username\" value=\"";
        // line 16
        echo twig_escape_filter($this->env, (isset($context["last_username"]) || array_key_exists("last_username", $context) ? $context["last_username"] : (function () { throw new Twig_Error_Runtime('Variable "last_username" does not exist.', 16, $this->getSourceContext()); })()), "html", null, true);
        echo "\" />
    <label for=\"password\">Password:</label>
    <input type=\"password\" id=\"password\" name=\"_password\" />

    ";
        // line 25
        echo "    <input type=\"checkbox\" name=\"_remember_me\" >
    <label for=\"remember_me\">Keep me logged in</label>

    <button type=\"submit\">login</button>
</form>
";
        
        $__internal_9df3aa27d6cc477890e6536b838d2f88f7aa1cfe18b2a324c98949918bd4b747->leave($__internal_9df3aa27d6cc477890e6536b838d2f88f7aa1cfe18b2a324c98949918bd4b747_prof);

        
        $__internal_19f100d8578c2cdd9552411c10edf85bca9de5b2ca9213e991f87090afa9c6be->leave($__internal_19f100d8578c2cdd9552411c10edf85bca9de5b2ca9213e991f87090afa9c6be_prof);

    }

    public function getTemplateName()
    {
        return "PlatypusBundle:User:login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 25,  74 => 16,  69 => 14,  63 => 11,  60 => 10,  54 => 8,  52 => 7,  49 => 6,  40 => 5,  11 => 3,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# app/Resources/views/security/login.html.twig #}
{# ... you will probably extend your base template, like base.html.twig #}
{% extends \"base.html.twig\" %}

{% block body%}

{% if error %}
    <div>{{ error.messageKey|trans(error.messageData, 'security') }}</div>
{% endif %}

<a href=\"{{ path('register') }}\">Register</a>


<form action=\"{{ path('login') }}\" method=\"post\">
    <label for=\"username\">Username:</label>
    <input type=\"text\" id=\"username\" name=\"_username\" value=\"{{ last_username }}\" />
    <label for=\"password\">Password:</label>
    <input type=\"password\" id=\"password\" name=\"_password\" />

    {#
        If you want to control the URL the user
        is redirected to on success (more details below)
        <input type=\"hidden\" name=\"_target_path\" value=\"/account\" />
    #}
    <input type=\"checkbox\" name=\"_remember_me\" >
    <label for=\"remember_me\">Keep me logged in</label>

    <button type=\"submit\">login</button>
</form>
{%endblock%}
", "PlatypusBundle:User:login.html.twig", "/home/elie/Gitlab/Symfony_Project/src/PlatypusBundle/Resources/views/User/login.html.twig");
    }
}
