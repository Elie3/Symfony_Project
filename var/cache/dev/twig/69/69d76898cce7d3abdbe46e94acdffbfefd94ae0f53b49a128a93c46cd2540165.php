<?php

/* @Framework/Form/form_rest.html.php */
class __TwigTemplate_ba35e73652e32c7bf0f6e4cc515d8e1f08248792af9fb890c686763cec2f3b25 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0b3161d86effc938deccb4c79d0570d59606a3f75c6dc7e37b8d29db8165948a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0b3161d86effc938deccb4c79d0570d59606a3f75c6dc7e37b8d29db8165948a->enter($__internal_0b3161d86effc938deccb4c79d0570d59606a3f75c6dc7e37b8d29db8165948a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        $__internal_2d8c6deb4edf929f641ef3b0c27ece2db9a3a05a3d56f57c722dcf991e0b5f8f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2d8c6deb4edf929f641ef3b0c27ece2db9a3a05a3d56f57c722dcf991e0b5f8f->enter($__internal_2d8c6deb4edf929f641ef3b0c27ece2db9a3a05a3d56f57c722dcf991e0b5f8f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rest.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
";
        
        $__internal_0b3161d86effc938deccb4c79d0570d59606a3f75c6dc7e37b8d29db8165948a->leave($__internal_0b3161d86effc938deccb4c79d0570d59606a3f75c6dc7e37b8d29db8165948a_prof);

        
        $__internal_2d8c6deb4edf929f641ef3b0c27ece2db9a3a05a3d56f57c722dcf991e0b5f8f->leave($__internal_2d8c6deb4edf929f641ef3b0c27ece2db9a3a05a3d56f57c722dcf991e0b5f8f_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rest.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child): ?>
    <?php if (!\$child->isRendered()): ?>
        <?php echo \$view['form']->row(\$child) ?>
    <?php endif; ?>
<?php endforeach; ?>
", "@Framework/Form/form_rest.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_rest.html.php");
    }
}
