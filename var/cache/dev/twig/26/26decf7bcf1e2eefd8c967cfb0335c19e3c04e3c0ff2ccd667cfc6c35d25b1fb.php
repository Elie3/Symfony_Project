<?php

/* PlatypusBundle:Post:posts_blogger.html.twig */
class __TwigTemplate_4c41aa061bbe13250dc9b2dfaa5fa43fbfd8c96fbfd77f3491d78e01a7659d6c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "PlatypusBundle:Post:posts_blogger.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a4644cfd2d77d322f84b94e930e50b76137af6e7926482763c6fed391107c7c3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a4644cfd2d77d322f84b94e930e50b76137af6e7926482763c6fed391107c7c3->enter($__internal_a4644cfd2d77d322f84b94e930e50b76137af6e7926482763c6fed391107c7c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:Post:posts_blogger.html.twig"));

        $__internal_bd1c3254e52b88c3143326d0c31de54c6e368c31b296f9e97bcfec3ce30c96f8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bd1c3254e52b88c3143326d0c31de54c6e368c31b296f9e97bcfec3ce30c96f8->enter($__internal_bd1c3254e52b88c3143326d0c31de54c6e368c31b296f9e97bcfec3ce30c96f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:Post:posts_blogger.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a4644cfd2d77d322f84b94e930e50b76137af6e7926482763c6fed391107c7c3->leave($__internal_a4644cfd2d77d322f84b94e930e50b76137af6e7926482763c6fed391107c7c3_prof);

        
        $__internal_bd1c3254e52b88c3143326d0c31de54c6e368c31b296f9e97bcfec3ce30c96f8->leave($__internal_bd1c3254e52b88c3143326d0c31de54c6e368c31b296f9e97bcfec3ce30c96f8_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_123b7ad047118ea4013becd542320936c3c59bc551d586342bb68c75ba9970f3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_123b7ad047118ea4013becd542320936c3c59bc551d586342bb68c75ba9970f3->enter($__internal_123b7ad047118ea4013becd542320936c3c59bc551d586342bb68c75ba9970f3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_6bf412883f85f0de4cd2a7a1fbf7143ea62cad2b6ac09e5b577abeaa27174d86 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6bf412883f85f0de4cd2a7a1fbf7143ea62cad2b6ac09e5b577abeaa27174d86->enter($__internal_6bf412883f85f0de4cd2a7a1fbf7143ea62cad2b6ac09e5b577abeaa27174d86_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("style.css"), "html", null, true);
        echo "\"/>
<table>

<tr><td>Id</td><td>Title</td><td>Content</td><td>Creation Date</td><td>Modification Date</td></tr>
";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new Twig_Error_Runtime('Variable "posts" does not exist.', 9, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["elem"]) {
            // line 10
            echo "<tr>
<td>";
            // line 11
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "id", array()), "html", null, true);
            echo "</td>
<td>";
            // line 12
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "title", array()), "html", null, true);
            echo "</td>
<td>";
            // line 13
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "content", array()), "html", null, true);
            echo "</td>
<td>";
            // line 14
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "creationDate", array()), "Y-m-d"), "html", null, true);
            echo "</td>
<td>";
            // line 15
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "modificationDate", array()), "Y-m-d"), "html", null, true);
            echo "</td>
</tr>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['elem'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "
<table>

  <p><a href=\"";
        // line 22
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("show_my_posts");
        echo "\" class=\"home_btn\">Show my posts</a></p>



";
        
        $__internal_6bf412883f85f0de4cd2a7a1fbf7143ea62cad2b6ac09e5b577abeaa27174d86->leave($__internal_6bf412883f85f0de4cd2a7a1fbf7143ea62cad2b6ac09e5b577abeaa27174d86_prof);

        
        $__internal_123b7ad047118ea4013becd542320936c3c59bc551d586342bb68c75ba9970f3->leave($__internal_123b7ad047118ea4013becd542320936c3c59bc551d586342bb68c75ba9970f3_prof);

    }

    public function getTemplateName()
    {
        return "PlatypusBundle:Post:posts_blogger.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  97 => 22,  92 => 19,  82 => 15,  78 => 14,  74 => 13,  70 => 12,  66 => 11,  63 => 10,  59 => 9,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"base.html.twig\" %}

{% block body%}

<link rel=\"stylesheet\" href=\"{{ asset('style.css')}}\"/>
<table>

<tr><td>Id</td><td>Title</td><td>Content</td><td>Creation Date</td><td>Modification Date</td></tr>
{% for elem in posts%}
<tr>
<td>{{elem.id}}</td>
<td>{{elem.title}}</td>
<td>{{elem.content}}</td>
<td>{{elem.creationDate|date('Y-m-d')}}</td>
<td>{{elem.modificationDate|date('Y-m-d')}}</td>
</tr>

{%endfor%}

<table>

  <p><a href=\"{{ path('show_my_posts') }}\" class=\"home_btn\">Show my posts</a></p>



{% endblock %}
", "PlatypusBundle:Post:posts_blogger.html.twig", "/home/elie/Documents/rendu/PHP/pool_symfony/test/src/PlatypusBundle/Resources/views/Post/posts_blogger.html.twig");
    }
}
