<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_11d15114edb16eb3fdc0488967b0a7d96e87f337dca5248c479bef3493cd2980 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b2c569f0245910429f589363cb68378fa4c3e1e8db848455fd1b2b53cfe33cf9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b2c569f0245910429f589363cb68378fa4c3e1e8db848455fd1b2b53cfe33cf9->enter($__internal_b2c569f0245910429f589363cb68378fa4c3e1e8db848455fd1b2b53cfe33cf9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        $__internal_9f1e90c4072a0efad55a50a7d7583d141a37b42b6ae8ff96db741b04427d62e0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9f1e90c4072a0efad55a50a7d7583d141a37b42b6ae8ff96db741b04427d62e0->enter($__internal_9f1e90c4072a0efad55a50a7d7583d141a37b42b6ae8ff96db741b04427d62e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_include($this->env, $context, "@Twig/Exception/exception.txt.twig", array("exception" => (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 2, $this->getSourceContext()); })())));
        echo "
*/
";
        
        $__internal_b2c569f0245910429f589363cb68378fa4c3e1e8db848455fd1b2b53cfe33cf9->leave($__internal_b2c569f0245910429f589363cb68378fa4c3e1e8db848455fd1b2b53cfe33cf9_prof);

        
        $__internal_9f1e90c4072a0efad55a50a7d7583d141a37b42b6ae8ff96db741b04427d62e0->leave($__internal_9f1e90c4072a0efad55a50a7d7583d141a37b42b6ae8ff96db741b04427d62e0_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ include('@Twig/Exception/exception.txt.twig', { exception: exception }) }}
*/
", "TwigBundle:Exception:exception.js.twig", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception.js.twig");
    }
}
