<?php

/* TwigBundle:Exception:error.atom.twig */
class __TwigTemplate_7769a2bf121a16b69a543407a57e2a110a4cf87a40d604ae80bd5011ab542719 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c4036039a653020dde9d7565da8f2149fde9cbdf8c88d3b8995ee5188b5580f4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c4036039a653020dde9d7565da8f2149fde9cbdf8c88d3b8995ee5188b5580f4->enter($__internal_c4036039a653020dde9d7565da8f2149fde9cbdf8c88d3b8995ee5188b5580f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        $__internal_b8c7ece2dcc975be7590962512960ccadee39d45d009534969de863867f31aa1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b8c7ece2dcc975be7590962512960ccadee39d45d009534969de863867f31aa1->enter($__internal_b8c7ece2dcc975be7590962512960ccadee39d45d009534969de863867f31aa1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.atom.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/error.xml.twig");
        echo "
";
        
        $__internal_c4036039a653020dde9d7565da8f2149fde9cbdf8c88d3b8995ee5188b5580f4->leave($__internal_c4036039a653020dde9d7565da8f2149fde9cbdf8c88d3b8995ee5188b5580f4_prof);

        
        $__internal_b8c7ece2dcc975be7590962512960ccadee39d45d009534969de863867f31aa1->leave($__internal_b8c7ece2dcc975be7590962512960ccadee39d45d009534969de863867f31aa1_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/error.xml.twig') }}
", "TwigBundle:Exception:error.atom.twig", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/error.atom.twig");
    }
}
