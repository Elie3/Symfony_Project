<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_0fbff5b886f18f5639f1b885e89da12b11f6a82cc2da60cb53764eb916eb2fb4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c25a03f5acc6301c55ac5acdf095fcc16ccfbe1e7e62e3438d23b6ccdd889fa1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c25a03f5acc6301c55ac5acdf095fcc16ccfbe1e7e62e3438d23b6ccdd889fa1->enter($__internal_c25a03f5acc6301c55ac5acdf095fcc16ccfbe1e7e62e3438d23b6ccdd889fa1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        $__internal_dcca435c453587c70bab25eefe2b2c5c5181562545e4efa97474b1e02a91822d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dcca435c453587c70bab25eefe2b2c5c5181562545e4efa97474b1e02a91822d->enter($__internal_dcca435c453587c70bab25eefe2b2c5c5181562545e4efa97474b1e02a91822d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_c25a03f5acc6301c55ac5acdf095fcc16ccfbe1e7e62e3438d23b6ccdd889fa1->leave($__internal_c25a03f5acc6301c55ac5acdf095fcc16ccfbe1e7e62e3438d23b6ccdd889fa1_prof);

        
        $__internal_dcca435c453587c70bab25eefe2b2c5c5181562545e4efa97474b1e02a91822d->leave($__internal_dcca435c453587c70bab25eefe2b2c5c5181562545e4efa97474b1e02a91822d_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
", "@Framework/Form/email_widget.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/email_widget.html.php");
    }
}
