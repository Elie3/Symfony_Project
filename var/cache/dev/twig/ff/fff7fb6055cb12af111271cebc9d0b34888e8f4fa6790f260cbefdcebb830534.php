<?php

/* PlatypusBundle:Post:posts_user.html.twig */
class __TwigTemplate_32c8ebf620301261b4207641ad9dd280124f5aeed174b5baba626aacf796e933 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "PlatypusBundle:Post:posts_user.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4503c31ca0a3808fc674b07ebea0cfb289f9327ed33765de6bdc66d9fe12c0a9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4503c31ca0a3808fc674b07ebea0cfb289f9327ed33765de6bdc66d9fe12c0a9->enter($__internal_4503c31ca0a3808fc674b07ebea0cfb289f9327ed33765de6bdc66d9fe12c0a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:Post:posts_user.html.twig"));

        $__internal_4207a7ce2076d57510631ee4c9f618ebb301af6980414ec63700dc67d446b493 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4207a7ce2076d57510631ee4c9f618ebb301af6980414ec63700dc67d446b493->enter($__internal_4207a7ce2076d57510631ee4c9f618ebb301af6980414ec63700dc67d446b493_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:Post:posts_user.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_4503c31ca0a3808fc674b07ebea0cfb289f9327ed33765de6bdc66d9fe12c0a9->leave($__internal_4503c31ca0a3808fc674b07ebea0cfb289f9327ed33765de6bdc66d9fe12c0a9_prof);

        
        $__internal_4207a7ce2076d57510631ee4c9f618ebb301af6980414ec63700dc67d446b493->leave($__internal_4207a7ce2076d57510631ee4c9f618ebb301af6980414ec63700dc67d446b493_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_95a601064041a5f0aae0c0b21f9ccb26b87d0a3e5254cc73a674e6efd80ea7cd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_95a601064041a5f0aae0c0b21f9ccb26b87d0a3e5254cc73a674e6efd80ea7cd->enter($__internal_95a601064041a5f0aae0c0b21f9ccb26b87d0a3e5254cc73a674e6efd80ea7cd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_ce42ed613c2674d1ee25dd1c99ba12e8ee741ead16bfed8a0b3778c8f6d7b886 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ce42ed613c2674d1ee25dd1c99ba12e8ee741ead16bfed8a0b3778c8f6d7b886->enter($__internal_ce42ed613c2674d1ee25dd1c99ba12e8ee741ead16bfed8a0b3778c8f6d7b886_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("style.css"), "html", null, true);
        echo "\"/>
<table>

<tr><td>Id</td><td>Title</td><td>Content</td><td>Creation Date</td><td>Modification Date</td></tr>
";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new Twig_Error_Runtime('Variable "posts" does not exist.', 9, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["elem"]) {
            // line 10
            echo "<tr>
<td>";
            // line 11
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "id", array()), "html", null, true);
            echo "</td>
<td>";
            // line 12
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "title", array()), "html", null, true);
            echo "</td>
<td>";
            // line 13
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "content", array()), "html", null, true);
            echo "</td>
<td>";
            // line 14
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "creationDate", array()), "Y-m-d"), "html", null, true);
            echo "</td>
<td>";
            // line 15
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "modificationDate", array()), "Y-m-d"), "html", null, true);
            echo "</td>
</tr>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['elem'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "
<table>




";
        
        $__internal_ce42ed613c2674d1ee25dd1c99ba12e8ee741ead16bfed8a0b3778c8f6d7b886->leave($__internal_ce42ed613c2674d1ee25dd1c99ba12e8ee741ead16bfed8a0b3778c8f6d7b886_prof);

        
        $__internal_95a601064041a5f0aae0c0b21f9ccb26b87d0a3e5254cc73a674e6efd80ea7cd->leave($__internal_95a601064041a5f0aae0c0b21f9ccb26b87d0a3e5254cc73a674e6efd80ea7cd_prof);

    }

    public function getTemplateName()
    {
        return "PlatypusBundle:Post:posts_user.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 19,  82 => 15,  78 => 14,  74 => 13,  70 => 12,  66 => 11,  63 => 10,  59 => 9,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"base.html.twig\" %}

{% block body%}

<link rel=\"stylesheet\" href=\"{{ asset('style.css')}}\"/>
<table>

<tr><td>Id</td><td>Title</td><td>Content</td><td>Creation Date</td><td>Modification Date</td></tr>
{% for elem in posts%}
<tr>
<td>{{elem.id}}</td>
<td>{{elem.title}}</td>
<td>{{elem.content}}</td>
<td>{{elem.creationDate|date('Y-m-d')}}</td>
<td>{{elem.modificationDate|date('Y-m-d')}}</td>
</tr>

{%endfor%}

<table>




{% endblock %}
", "PlatypusBundle:Post:posts_user.html.twig", "/home/elie/Documents/rendu/PHP/pool_symfony/test/src/PlatypusBundle/Resources/views/Post/posts_user.html.twig");
    }
}
