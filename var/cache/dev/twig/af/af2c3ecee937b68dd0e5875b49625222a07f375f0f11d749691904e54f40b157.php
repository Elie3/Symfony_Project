<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_6cafe743c7842cc2cac3000572289a6daadb839517865a736ef43ea1d86432af extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_471fae71821e0858b00d84882741d9079beb992ac8ad1e8300da235bae3f20cb = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_471fae71821e0858b00d84882741d9079beb992ac8ad1e8300da235bae3f20cb->enter($__internal_471fae71821e0858b00d84882741d9079beb992ac8ad1e8300da235bae3f20cb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        $__internal_d6506097d85a75347734f4c7d139f49da7018dc928d48c1d6bfb853e5fd82376 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d6506097d85a75347734f4c7d139f49da7018dc928d48c1d6bfb853e5fd82376->enter($__internal_d6506097d85a75347734f4c7d139f49da7018dc928d48c1d6bfb853e5fd82376_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_471fae71821e0858b00d84882741d9079beb992ac8ad1e8300da235bae3f20cb->leave($__internal_471fae71821e0858b00d84882741d9079beb992ac8ad1e8300da235bae3f20cb_prof);

        
        $__internal_d6506097d85a75347734f4c7d139f49da7018dc928d48c1d6bfb853e5fd82376->leave($__internal_d6506097d85a75347734f4c7d139f49da7018dc928d48c1d6bfb853e5fd82376_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
", "@Framework/Form/choice_widget.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/choice_widget.html.php");
    }
}
