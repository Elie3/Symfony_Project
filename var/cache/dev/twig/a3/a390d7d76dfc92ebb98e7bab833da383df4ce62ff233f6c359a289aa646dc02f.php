<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_f3e445c0016a149d1983c07d2585961d8f0a63f8b2bddee6d2bef06e297516f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_057ebed110dd403bc86ee4700fa53efa840967bcaad3f2e8849924fedc82dc61 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_057ebed110dd403bc86ee4700fa53efa840967bcaad3f2e8849924fedc82dc61->enter($__internal_057ebed110dd403bc86ee4700fa53efa840967bcaad3f2e8849924fedc82dc61_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        $__internal_01303cc97494a09ab44581b4ae720cfdf04379975aad77f9b81d52607a04eeaa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_01303cc97494a09ab44581b4ae720cfdf04379975aad77f9b81d52607a04eeaa->enter($__internal_01303cc97494a09ab44581b4ae720cfdf04379975aad77f9b81d52607a04eeaa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_057ebed110dd403bc86ee4700fa53efa840967bcaad3f2e8849924fedc82dc61->leave($__internal_057ebed110dd403bc86ee4700fa53efa840967bcaad3f2e8849924fedc82dc61_prof);

        
        $__internal_01303cc97494a09ab44581b4ae720cfdf04379975aad77f9b81d52607a04eeaa->leave($__internal_01303cc97494a09ab44581b4ae720cfdf04379975aad77f9b81d52607a04eeaa_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_75728f9c0c025f54c868bb7ae77e5ce609b39a925b7d69f593095698c1dd3cf1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_75728f9c0c025f54c868bb7ae77e5ce609b39a925b7d69f593095698c1dd3cf1->enter($__internal_75728f9c0c025f54c868bb7ae77e5ce609b39a925b7d69f593095698c1dd3cf1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_409565377d39f773f0165c12c5071aa2bb3c2e80b05d9718686e4f80d4c272be = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_409565377d39f773f0165c12c5071aa2bb3c2e80b05d9718686e4f80d4c272be->enter($__internal_409565377d39f773f0165c12c5071aa2bb3c2e80b05d9718686e4f80d4c272be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_409565377d39f773f0165c12c5071aa2bb3c2e80b05d9718686e4f80d4c272be->leave($__internal_409565377d39f773f0165c12c5071aa2bb3c2e80b05d9718686e4f80d4c272be_prof);

        
        $__internal_75728f9c0c025f54c868bb7ae77e5ce609b39a925b7d69f593095698c1dd3cf1->leave($__internal_75728f9c0c025f54c868bb7ae77e5ce609b39a925b7d69f593095698c1dd3cf1_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  26 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "WebProfilerBundle:Profiler:ajax_layout.html.twig", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Profiler/ajax_layout.html.twig");
    }
}
