<?php

/* PlatypusBundle:Checker:show_action.html.twig */
class __TwigTemplate_9ddd6043110f47a014d5363b53e122424e6b73d10873cfc8c374acb86040fc74 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("PlatypusBundle:Default:layout.html.twig", "PlatypusBundle:Checker:show_action.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "PlatypusBundle:Default:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d8bfde13f4c134ed6e4a208b9b28705863690131f5611468b5d4b24f14a36ba5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d8bfde13f4c134ed6e4a208b9b28705863690131f5611468b5d4b24f14a36ba5->enter($__internal_d8bfde13f4c134ed6e4a208b9b28705863690131f5611468b5d4b24f14a36ba5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:Checker:show_action.html.twig"));

        $__internal_93b282e0d0717bcfec8645f7e59a4a432609c4e1e88b273131b9bd97841910f2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_93b282e0d0717bcfec8645f7e59a4a432609c4e1e88b273131b9bd97841910f2->enter($__internal_93b282e0d0717bcfec8645f7e59a4a432609c4e1e88b273131b9bd97841910f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:Checker:show_action.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d8bfde13f4c134ed6e4a208b9b28705863690131f5611468b5d4b24f14a36ba5->leave($__internal_d8bfde13f4c134ed6e4a208b9b28705863690131f5611468b5d4b24f14a36ba5_prof);

        
        $__internal_93b282e0d0717bcfec8645f7e59a4a432609c4e1e88b273131b9bd97841910f2->leave($__internal_93b282e0d0717bcfec8645f7e59a4a432609c4e1e88b273131b9bd97841910f2_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_9e06fee455f40c87c500cac9ee80ec7fc6e33139b66e8147a040dda3aec4c6d1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9e06fee455f40c87c500cac9ee80ec7fc6e33139b66e8147a040dda3aec4c6d1->enter($__internal_9e06fee455f40c87c500cac9ee80ec7fc6e33139b66e8147a040dda3aec4c6d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_8134219a3c2f77ff250992ea171d40c0d709b3d7f5c053e91c3b125b63fe2b5b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8134219a3c2f77ff250992ea171d40c0d709b3d7f5c053e91c3b125b63fe2b5b->enter($__internal_8134219a3c2f77ff250992ea171d40c0d709b3d7f5c053e91c3b125b63fe2b5b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "PlatypusBundle:Checker:show";
        
        $__internal_8134219a3c2f77ff250992ea171d40c0d709b3d7f5c053e91c3b125b63fe2b5b->leave($__internal_8134219a3c2f77ff250992ea171d40c0d709b3d7f5c053e91c3b125b63fe2b5b_prof);

        
        $__internal_9e06fee455f40c87c500cac9ee80ec7fc6e33139b66e8147a040dda3aec4c6d1->leave($__internal_9e06fee455f40c87c500cac9ee80ec7fc6e33139b66e8147a040dda3aec4c6d1_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_6f5aad9ea9f2c9dbc049a0b41700fb51019d4dc80bbcb7b9ff03f489f4ed4fe1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6f5aad9ea9f2c9dbc049a0b41700fb51019d4dc80bbcb7b9ff03f489f4ed4fe1->enter($__internal_6f5aad9ea9f2c9dbc049a0b41700fb51019d4dc80bbcb7b9ff03f489f4ed4fe1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_ebbd42c1b6a17016497f9eb60d27f58a797b292e141e4c93f3c7cb8f7fd0b3d2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ebbd42c1b6a17016497f9eb60d27f58a797b292e141e4c93f3c7cb8f7fd0b3d2->enter($__internal_ebbd42c1b6a17016497f9eb60d27f58a797b292e141e4c93f3c7cb8f7fd0b3d2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
";
        // line 7
        if (((isset($context["type"]) || array_key_exists("type", $context) ? $context["type"] : (function () { throw new Twig_Error_Runtime('Variable "type" does not exist.', 7, $this->getSourceContext()); })()) == "Platypus")) {
            // line 8
            echo "  <h1>I salute you, Ô great master</h1>
";
        } else {
            // line 10
            echo "  <h1>Greetings, great ";
            echo twig_escape_filter($this->env, (isset($context["type"]) || array_key_exists("type", $context) ? $context["type"] : (function () { throw new Twig_Error_Runtime('Variable "type" does not exist.', 10, $this->getSourceContext()); })()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["id"]) || array_key_exists("id", $context) ? $context["id"] : (function () { throw new Twig_Error_Runtime('Variable "id" does not exist.', 10, $this->getSourceContext()); })()), "html", null, true);
            echo "</h1>
";
        }
        // line 12
        echo "
";
        
        $__internal_ebbd42c1b6a17016497f9eb60d27f58a797b292e141e4c93f3c7cb8f7fd0b3d2->leave($__internal_ebbd42c1b6a17016497f9eb60d27f58a797b292e141e4c93f3c7cb8f7fd0b3d2_prof);

        
        $__internal_6f5aad9ea9f2c9dbc049a0b41700fb51019d4dc80bbcb7b9ff03f489f4ed4fe1->leave($__internal_6f5aad9ea9f2c9dbc049a0b41700fb51019d4dc80bbcb7b9ff03f489f4ed4fe1_prof);

    }

    public function getTemplateName()
    {
        return "PlatypusBundle:Checker:show_action.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 12,  77 => 10,  73 => 8,  71 => 7,  68 => 6,  59 => 5,  41 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"PlatypusBundle:Default:layout.html.twig\" %}

{% block title %}PlatypusBundle:Checker:show{% endblock %}

{% block body %}

{% if type == \"Platypus\"%}
  <h1>I salute you, Ô great master</h1>
{% else %}
  <h1>Greetings, great {{type}} {{id}}</h1>
{% endif %}

{% endblock %}
", "PlatypusBundle:Checker:show_action.html.twig", "/home/elie/Documents/rendu/PHP/pool_symfony/test/src/PlatypusBundle/Resources/views/Checker/show_action.html.twig");
    }
}
