<?php

/* @WebProfiler/Collector/exception.html.twig */
class __TwigTemplate_a4a6e37587f790fa77bff5af918fc4d22299769037b1fff8663ac23544938d88 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/exception.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9b4d999461a718e97bebc6e3d6cb85304f124c5af6b174714f34d8183f861435 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9b4d999461a718e97bebc6e3d6cb85304f124c5af6b174714f34d8183f861435->enter($__internal_9b4d999461a718e97bebc6e3d6cb85304f124c5af6b174714f34d8183f861435_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $__internal_b1778c6c8479a31b0470bbb3b7b7b73169a8525870f5076c67e80b749044391f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b1778c6c8479a31b0470bbb3b7b7b73169a8525870f5076c67e80b749044391f->enter($__internal_b1778c6c8479a31b0470bbb3b7b7b73169a8525870f5076c67e80b749044391f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/exception.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9b4d999461a718e97bebc6e3d6cb85304f124c5af6b174714f34d8183f861435->leave($__internal_9b4d999461a718e97bebc6e3d6cb85304f124c5af6b174714f34d8183f861435_prof);

        
        $__internal_b1778c6c8479a31b0470bbb3b7b7b73169a8525870f5076c67e80b749044391f->leave($__internal_b1778c6c8479a31b0470bbb3b7b7b73169a8525870f5076c67e80b749044391f_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_6e7dfb4478cd991b419d24c7ac418585aa50f7b5fc4564b97bc8dced7a8402dc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6e7dfb4478cd991b419d24c7ac418585aa50f7b5fc4564b97bc8dced7a8402dc->enter($__internal_6e7dfb4478cd991b419d24c7ac418585aa50f7b5fc4564b97bc8dced7a8402dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_8e508b669aa469edced25c5e884cac711d1651f1cb7710cc23d0411b28c074dd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8e508b669aa469edced25c5e884cac711d1651f1cb7710cc23d0411b28c074dd->enter($__internal_8e508b669aa469edced25c5e884cac711d1651f1cb7710cc23d0411b28c074dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    ";
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 4, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 5
            echo "        <style>
            ";
            // line 6
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception_css", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 6, $this->getSourceContext()); })()))));
            echo "
        </style>
    ";
        }
        // line 9
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
        
        $__internal_8e508b669aa469edced25c5e884cac711d1651f1cb7710cc23d0411b28c074dd->leave($__internal_8e508b669aa469edced25c5e884cac711d1651f1cb7710cc23d0411b28c074dd_prof);

        
        $__internal_6e7dfb4478cd991b419d24c7ac418585aa50f7b5fc4564b97bc8dced7a8402dc->leave($__internal_6e7dfb4478cd991b419d24c7ac418585aa50f7b5fc4564b97bc8dced7a8402dc_prof);

    }

    // line 12
    public function block_menu($context, array $blocks = array())
    {
        $__internal_80b15a2840459403922893173525b72f14eaf52ec67dca129249d9fcfba72381 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_80b15a2840459403922893173525b72f14eaf52ec67dca129249d9fcfba72381->enter($__internal_80b15a2840459403922893173525b72f14eaf52ec67dca129249d9fcfba72381_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_675651cc735ce5edacd1b03eeffcef81369c4e33b88adf255034b2486ea7dd91 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_675651cc735ce5edacd1b03eeffcef81369c4e33b88adf255034b2486ea7dd91->enter($__internal_675651cc735ce5edacd1b03eeffcef81369c4e33b88adf255034b2486ea7dd91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 13
        echo "    <span class=\"label ";
        echo ((twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 13, $this->getSourceContext()); })()), "hasexception", array())) ? ("label-status-error") : ("disabled"));
        echo "\">
        <span class=\"icon\">";
        // line 14
        echo twig_include($this->env, $context, "@WebProfiler/Icon/exception.svg");
        echo "</span>
        <strong>Exception</strong>
        ";
        // line 16
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 16, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 17
            echo "            <span class=\"count\">
                <span>1</span>
            </span>
        ";
        }
        // line 21
        echo "    </span>
";
        
        $__internal_675651cc735ce5edacd1b03eeffcef81369c4e33b88adf255034b2486ea7dd91->leave($__internal_675651cc735ce5edacd1b03eeffcef81369c4e33b88adf255034b2486ea7dd91_prof);

        
        $__internal_80b15a2840459403922893173525b72f14eaf52ec67dca129249d9fcfba72381->leave($__internal_80b15a2840459403922893173525b72f14eaf52ec67dca129249d9fcfba72381_prof);

    }

    // line 24
    public function block_panel($context, array $blocks = array())
    {
        $__internal_ffedcab00bdc67e99767ffc8caf278b0aea93a9942f259481ac3464d9a12cc94 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ffedcab00bdc67e99767ffc8caf278b0aea93a9942f259481ac3464d9a12cc94->enter($__internal_ffedcab00bdc67e99767ffc8caf278b0aea93a9942f259481ac3464d9a12cc94_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_6e3de49d0ec8a6f9ebcd2b0a4f71f3b74b06df86351e9b426298a665d69a3442 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6e3de49d0ec8a6f9ebcd2b0a4f71f3b74b06df86351e9b426298a665d69a3442->enter($__internal_6e3de49d0ec8a6f9ebcd2b0a4f71f3b74b06df86351e9b426298a665d69a3442_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 25
        echo "    <h2>Exceptions</h2>

    ";
        // line 27
        if ( !twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 27, $this->getSourceContext()); })()), "hasexception", array())) {
            // line 28
            echo "        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    ";
        } else {
            // line 32
            echo "        <div class=\"sf-reset\">
            ";
            // line 33
            echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_exception", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 33, $this->getSourceContext()); })()))));
            echo "
        </div>
    ";
        }
        
        $__internal_6e3de49d0ec8a6f9ebcd2b0a4f71f3b74b06df86351e9b426298a665d69a3442->leave($__internal_6e3de49d0ec8a6f9ebcd2b0a4f71f3b74b06df86351e9b426298a665d69a3442_prof);

        
        $__internal_ffedcab00bdc67e99767ffc8caf278b0aea93a9942f259481ac3464d9a12cc94->leave($__internal_ffedcab00bdc67e99767ffc8caf278b0aea93a9942f259481ac3464d9a12cc94_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 33,  135 => 32,  129 => 28,  127 => 27,  123 => 25,  114 => 24,  103 => 21,  97 => 17,  95 => 16,  90 => 14,  85 => 13,  76 => 12,  63 => 9,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block head %}
    {% if collector.hasexception %}
        <style>
            {{ render(path('_profiler_exception_css', { token: token })) }}
        </style>
    {% endif %}
    {{ parent() }}
{% endblock %}

{% block menu %}
    <span class=\"label {{ collector.hasexception ? 'label-status-error' : 'disabled' }}\">
        <span class=\"icon\">{{ include('@WebProfiler/Icon/exception.svg') }}</span>
        <strong>Exception</strong>
        {% if collector.hasexception %}
            <span class=\"count\">
                <span>1</span>
            </span>
        {% endif %}
    </span>
{% endblock %}

{% block panel %}
    <h2>Exceptions</h2>

    {% if not collector.hasexception %}
        <div class=\"empty\">
            <p>No exception was thrown and caught during the request.</p>
        </div>
    {% else %}
        <div class=\"sf-reset\">
            {{ render(path('_profiler_exception', { token: token })) }}
        </div>
    {% endif %}
{% endblock %}
", "@WebProfiler/Collector/exception.html.twig", "/home/elie/Gitlab/Symfony_Project/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/exception.html.twig");
    }
}
