<?php

/* PlatypusBundle:Post:edit_post.html.twig */
class __TwigTemplate_ca2827f58b263ba1da7d170c2d461c9d13ed571223dcd16e9156d40e4e2d1db0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "PlatypusBundle:Post:edit_post.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7cb5f2ee8ef3cea4077fb36e4d5088f8f5ca8b10a94b37366c44be445f008d97 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7cb5f2ee8ef3cea4077fb36e4d5088f8f5ca8b10a94b37366c44be445f008d97->enter($__internal_7cb5f2ee8ef3cea4077fb36e4d5088f8f5ca8b10a94b37366c44be445f008d97_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:Post:edit_post.html.twig"));

        $__internal_599600330b88d41e11e32a074b3f06c95a019cfed9d855c9b7e3cdba25c8e1eb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_599600330b88d41e11e32a074b3f06c95a019cfed9d855c9b7e3cdba25c8e1eb->enter($__internal_599600330b88d41e11e32a074b3f06c95a019cfed9d855c9b7e3cdba25c8e1eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:Post:edit_post.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7cb5f2ee8ef3cea4077fb36e4d5088f8f5ca8b10a94b37366c44be445f008d97->leave($__internal_7cb5f2ee8ef3cea4077fb36e4d5088f8f5ca8b10a94b37366c44be445f008d97_prof);

        
        $__internal_599600330b88d41e11e32a074b3f06c95a019cfed9d855c9b7e3cdba25c8e1eb->leave($__internal_599600330b88d41e11e32a074b3f06c95a019cfed9d855c9b7e3cdba25c8e1eb_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_fc1479af822c0e993cc614945a8b22727093be2f311d63f1e1c12abb519cbbf7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fc1479af822c0e993cc614945a8b22727093be2f311d63f1e1c12abb519cbbf7->enter($__internal_fc1479af822c0e993cc614945a8b22727093be2f311d63f1e1c12abb519cbbf7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_c5d27fd8d8da0cfd2c3369b8b4827a394c2415fb280daae073d4ec38ba6273eb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c5d27fd8d8da0cfd2c3369b8b4827a394c2415fb280daae073d4ec38ba6273eb->enter($__internal_c5d27fd8d8da0cfd2c3369b8b4827a394c2415fb280daae073d4ec38ba6273eb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<p class=\"form\">
";
        // line 5
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 5, $this->getSourceContext()); })()), 'form_start');
        echo "
";
        // line 6
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 6, $this->getSourceContext()); })()), 'widget');
        echo "
";
        // line 7
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 7, $this->getSourceContext()); })()), 'form_end');
        echo "
</p>

";
        
        $__internal_c5d27fd8d8da0cfd2c3369b8b4827a394c2415fb280daae073d4ec38ba6273eb->leave($__internal_c5d27fd8d8da0cfd2c3369b8b4827a394c2415fb280daae073d4ec38ba6273eb_prof);

        
        $__internal_fc1479af822c0e993cc614945a8b22727093be2f311d63f1e1c12abb519cbbf7->leave($__internal_fc1479af822c0e993cc614945a8b22727093be2f311d63f1e1c12abb519cbbf7_prof);

    }

    public function getTemplateName()
    {
        return "PlatypusBundle:Post:edit_post.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 7,  56 => 6,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"base.html.twig\" %}

{% block body%}
<p class=\"form\">
{{ form_start(form) }}
{{ form_widget(form) }}
{{ form_end(form) }}
</p>

{% endblock %}
", "PlatypusBundle:Post:edit_post.html.twig", "/home/elie/Documents/rendu/PHP/pool_symfony/test/src/PlatypusBundle/Resources/views/Post/edit_post.html.twig");
    }
}
