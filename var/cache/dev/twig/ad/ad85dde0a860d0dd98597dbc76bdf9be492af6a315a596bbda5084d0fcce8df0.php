<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_34f7fe0fd5deaff3e0f6dcc896a20eb2952fb3203d6fc3ea8e628f3c855db125 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_859ea232a437bbabd2aff83efa0ea998421d482fc8887d9f46162492229943dc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_859ea232a437bbabd2aff83efa0ea998421d482fc8887d9f46162492229943dc->enter($__internal_859ea232a437bbabd2aff83efa0ea998421d482fc8887d9f46162492229943dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        $__internal_0647fc5ec60948bbf01728b17eb6403020718267fc2182ad21a98ccc51f5912b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0647fc5ec60948bbf01728b17eb6403020718267fc2182ad21a98ccc51f5912b->enter($__internal_0647fc5ec60948bbf01728b17eb6403020718267fc2182ad21a98ccc51f5912b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_859ea232a437bbabd2aff83efa0ea998421d482fc8887d9f46162492229943dc->leave($__internal_859ea232a437bbabd2aff83efa0ea998421d482fc8887d9f46162492229943dc_prof);

        
        $__internal_0647fc5ec60948bbf01728b17eb6403020718267fc2182ad21a98ccc51f5912b->leave($__internal_0647fc5ec60948bbf01728b17eb6403020718267fc2182ad21a98ccc51f5912b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/button_row.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/button_row.html.php");
    }
}
