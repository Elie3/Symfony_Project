<?php

/* :post:new.html.twig */
class __TwigTemplate_f431a8edd14b54999e6e76a127702d0ab4cc5d304bab3d119e21b755cc8ca3e3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":post:new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c9ae9cd180ca450cd62c4e1a3abd7145a017522c9008e5300b1c540700ed4cf2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c9ae9cd180ca450cd62c4e1a3abd7145a017522c9008e5300b1c540700ed4cf2->enter($__internal_c9ae9cd180ca450cd62c4e1a3abd7145a017522c9008e5300b1c540700ed4cf2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":post:new.html.twig"));

        $__internal_b57bb8bf6bc0e73e7fc43a253aa1bba894fadaafe8f633a2b7504605a9b85e48 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b57bb8bf6bc0e73e7fc43a253aa1bba894fadaafe8f633a2b7504605a9b85e48->enter($__internal_b57bb8bf6bc0e73e7fc43a253aa1bba894fadaafe8f633a2b7504605a9b85e48_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":post:new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c9ae9cd180ca450cd62c4e1a3abd7145a017522c9008e5300b1c540700ed4cf2->leave($__internal_c9ae9cd180ca450cd62c4e1a3abd7145a017522c9008e5300b1c540700ed4cf2_prof);

        
        $__internal_b57bb8bf6bc0e73e7fc43a253aa1bba894fadaafe8f633a2b7504605a9b85e48->leave($__internal_b57bb8bf6bc0e73e7fc43a253aa1bba894fadaafe8f633a2b7504605a9b85e48_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_5eebc49c5e31a8f70304dc7379a60a1e19327194138ad8523fcc4d4f58fb0942 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5eebc49c5e31a8f70304dc7379a60a1e19327194138ad8523fcc4d4f58fb0942->enter($__internal_5eebc49c5e31a8f70304dc7379a60a1e19327194138ad8523fcc4d4f58fb0942_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_e28296f4572ef4b1e88a609b6f65101a4d3a5c9a8018783c797e57ebaff66a3c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e28296f4572ef4b1e88a609b6f65101a4d3a5c9a8018783c797e57ebaff66a3c->enter($__internal_e28296f4572ef4b1e88a609b6f65101a4d3a5c9a8018783c797e57ebaff66a3c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Post creation</h1>

    ";
        // line 6
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 6, $this->getSourceContext()); })()), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 7, $this->getSourceContext()); })()), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" />
    ";
        // line 9
        echo         $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new Twig_Error_Runtime('Variable "form" does not exist.', 9, $this->getSourceContext()); })()), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("post_index");
        echo "\">Back to the list</a>
        </li>
    </ul>
";
        
        $__internal_e28296f4572ef4b1e88a609b6f65101a4d3a5c9a8018783c797e57ebaff66a3c->leave($__internal_e28296f4572ef4b1e88a609b6f65101a4d3a5c9a8018783c797e57ebaff66a3c_prof);

        
        $__internal_5eebc49c5e31a8f70304dc7379a60a1e19327194138ad8523fcc4d4f58fb0942->leave($__internal_5eebc49c5e31a8f70304dc7379a60a1e19327194138ad8523fcc4d4f58fb0942_prof);

    }

    public function getTemplateName()
    {
        return ":post:new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 13,  62 => 9,  57 => 7,  53 => 6,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Post creation</h1>

    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input type=\"submit\" value=\"Create\" />
    {{ form_end(form) }}

    <ul>
        <li>
            <a href=\"{{ path('post_index') }}\">Back to the list</a>
        </li>
    </ul>
{% endblock %}
", ":post:new.html.twig", "/home/elie/Documents/rendu/PHP/pool_symfony/test/app/Resources/views/post/new.html.twig");
    }
}
