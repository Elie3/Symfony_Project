<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_213321444c2a43dfe05dc4e1e9c2b18d97e3e19c5555732537ebba806e0e250a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_26afc6f42c7b00fe3a6eec7ca2008f25f749a95f26e562a35e2892a881fab4ec = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_26afc6f42c7b00fe3a6eec7ca2008f25f749a95f26e562a35e2892a881fab4ec->enter($__internal_26afc6f42c7b00fe3a6eec7ca2008f25f749a95f26e562a35e2892a881fab4ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        $__internal_5eb2363933bbd91508d2b4bbf2c4c748024f4aec193c874b2fc92c0dfcc85f4b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5eb2363933bbd91508d2b4bbf2c4c748024f4aec193c874b2fc92c0dfcc85f4b->enter($__internal_5eb2363933bbd91508d2b4bbf2c4c748024f4aec193c874b2fc92c0dfcc85f4b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_26afc6f42c7b00fe3a6eec7ca2008f25f749a95f26e562a35e2892a881fab4ec->leave($__internal_26afc6f42c7b00fe3a6eec7ca2008f25f749a95f26e562a35e2892a881fab4ec_prof);

        
        $__internal_5eb2363933bbd91508d2b4bbf2c4c748024f4aec193c874b2fc92c0dfcc85f4b->leave($__internal_5eb2363933bbd91508d2b4bbf2c4c748024f4aec193c874b2fc92c0dfcc85f4b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
", "@Framework/Form/percent_widget.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/percent_widget.html.php");
    }
}
