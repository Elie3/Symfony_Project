<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_85702d91ff8554395f2447ac0bc557d2432fd0f8778ed8ca9671756fa6638e80 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4034704c745530dfcc4eef27ae64db34748190231a0f9b7fbde1aae8e3011eae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4034704c745530dfcc4eef27ae64db34748190231a0f9b7fbde1aae8e3011eae->enter($__internal_4034704c745530dfcc4eef27ae64db34748190231a0f9b7fbde1aae8e3011eae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        $__internal_58754b8c00a05da6459d8dd67e1656d0e3c52f96fcdef0646ad474402a67008a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_58754b8c00a05da6459d8dd67e1656d0e3c52f96fcdef0646ad474402a67008a->enter($__internal_58754b8c00a05da6459d8dd67e1656d0e3c52f96fcdef0646ad474402a67008a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_4034704c745530dfcc4eef27ae64db34748190231a0f9b7fbde1aae8e3011eae->leave($__internal_4034704c745530dfcc4eef27ae64db34748190231a0f9b7fbde1aae8e3011eae_prof);

        
        $__internal_58754b8c00a05da6459d8dd67e1656d0e3c52f96fcdef0646ad474402a67008a->leave($__internal_58754b8c00a05da6459d8dd67e1656d0e3c52f96fcdef0646ad474402a67008a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
", "@Framework/Form/container_attributes.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/container_attributes.html.php");
    }
}
