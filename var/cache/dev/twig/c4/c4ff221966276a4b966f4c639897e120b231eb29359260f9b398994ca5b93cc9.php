<?php

/* @Framework/FormTable/hidden_row.html.php */
class __TwigTemplate_c477a836e135cb88cbd875420797abb5a748baa5794fe60e4021da151357093d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_13c204cb60b679ee03ad012df2fd24907eff0c8233af7b6b488efcc99953755a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_13c204cb60b679ee03ad012df2fd24907eff0c8233af7b6b488efcc99953755a->enter($__internal_13c204cb60b679ee03ad012df2fd24907eff0c8233af7b6b488efcc99953755a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        $__internal_e21e1d1092a5b0cb22f7e8cd4e5602a26943c07b3105cd1f75ea4273c4e513a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e21e1d1092a5b0cb22f7e8cd4e5602a26943c07b3105cd1f75ea4273c4e513a8->enter($__internal_e21e1d1092a5b0cb22f7e8cd4e5602a26943c07b3105cd1f75ea4273c4e513a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/hidden_row.html.php"));

        // line 1
        echo "<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
";
        
        $__internal_13c204cb60b679ee03ad012df2fd24907eff0c8233af7b6b488efcc99953755a->leave($__internal_13c204cb60b679ee03ad012df2fd24907eff0c8233af7b6b488efcc99953755a_prof);

        
        $__internal_e21e1d1092a5b0cb22f7e8cd4e5602a26943c07b3105cd1f75ea4273c4e513a8->leave($__internal_e21e1d1092a5b0cb22f7e8cd4e5602a26943c07b3105cd1f75ea4273c4e513a8_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<tr style=\"display: none\">
    <td colspan=\"2\">
        <?php echo \$view['form']->widget(\$form) ?>
    </td>
</tr>
", "@Framework/FormTable/hidden_row.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/FormTable/hidden_row.html.php");
    }
}
