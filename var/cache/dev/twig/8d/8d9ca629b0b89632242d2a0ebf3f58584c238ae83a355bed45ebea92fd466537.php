<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_54f4973d01f36e3e963d368d93381514bd75e2db335afd9db7518c716209a59a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_913444af7779bf0671028233d104414326cf6fb88db52d6bf55044550b2a2048 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_913444af7779bf0671028233d104414326cf6fb88db52d6bf55044550b2a2048->enter($__internal_913444af7779bf0671028233d104414326cf6fb88db52d6bf55044550b2a2048_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        $__internal_0635338e3fe469200433bb2cdedc1b2d86e8b756c89e936531c1ab79fc01d705 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0635338e3fe469200433bb2cdedc1b2d86e8b756c89e936531c1ab79fc01d705->enter($__internal_0635338e3fe469200433bb2cdedc1b2d86e8b756c89e936531c1ab79fc01d705_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_913444af7779bf0671028233d104414326cf6fb88db52d6bf55044550b2a2048->leave($__internal_913444af7779bf0671028233d104414326cf6fb88db52d6bf55044550b2a2048_prof);

        
        $__internal_0635338e3fe469200433bb2cdedc1b2d86e8b756c89e936531c1ab79fc01d705->leave($__internal_0635338e3fe469200433bb2cdedc1b2d86e8b756c89e936531c1ab79fc01d705_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
", "@Framework/Form/integer_widget.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/integer_widget.html.php");
    }
}
