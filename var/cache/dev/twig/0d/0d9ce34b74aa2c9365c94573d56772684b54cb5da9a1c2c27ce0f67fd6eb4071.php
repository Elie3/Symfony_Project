<?php

/* @Framework/Form/form_row.html.php */
class __TwigTemplate_a7ef40cafac11eebbaff44ffdebc8c7252561b620626b78935f0799281b17a71 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9108d2c18aaf589c3c9ee4f7d40ce2ba57ce95bb1ef98f011563d3494ec957dc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9108d2c18aaf589c3c9ee4f7d40ce2ba57ce95bb1ef98f011563d3494ec957dc->enter($__internal_9108d2c18aaf589c3c9ee4f7d40ce2ba57ce95bb1ef98f011563d3494ec957dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        $__internal_91de731f2f6cdc4447b4446cafa10973514b6f0a0b0aae9a370a9d90cf85f075 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_91de731f2f6cdc4447b4446cafa10973514b6f0a0b0aae9a370a9d90cf85f075->enter($__internal_91de731f2f6cdc4447b4446cafa10973514b6f0a0b0aae9a370a9d90cf85f075_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_9108d2c18aaf589c3c9ee4f7d40ce2ba57ce95bb1ef98f011563d3494ec957dc->leave($__internal_9108d2c18aaf589c3c9ee4f7d40ce2ba57ce95bb1ef98f011563d3494ec957dc_prof);

        
        $__internal_91de731f2f6cdc4447b4446cafa10973514b6f0a0b0aae9a370a9d90cf85f075->leave($__internal_91de731f2f6cdc4447b4446cafa10973514b6f0a0b0aae9a370a9d90cf85f075_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->label(\$form) ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/form_row.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/form_row.html.php");
    }
}
