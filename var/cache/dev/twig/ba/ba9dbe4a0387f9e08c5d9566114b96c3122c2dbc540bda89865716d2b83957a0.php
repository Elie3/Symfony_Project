<?php

/* @Framework/Form/money_widget.html.php */
class __TwigTemplate_3a7d9be90ac4c8f7d509cb23a6fa258a9771ef4b4acf04a11697610e642bf75d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4b83548672f496fe0a9972306db2b8ad2459c66d767456758391d5f421144ae2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4b83548672f496fe0a9972306db2b8ad2459c66d767456758391d5f421144ae2->enter($__internal_4b83548672f496fe0a9972306db2b8ad2459c66d767456758391d5f421144ae2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        $__internal_673d2c0f4ddc540ddb564a048c224552ca4d5ff806ffd995980fbdc3b788a642 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_673d2c0f4ddc540ddb564a048c224552ca4d5ff806ffd995980fbdc3b788a642->enter($__internal_673d2c0f4ddc540ddb564a048c224552ca4d5ff806ffd995980fbdc3b788a642_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/money_widget.html.php"));

        // line 1
        echo "<?php echo str_replace('";
        echo twig_escape_filter($this->env, (isset($context["widget"]) || array_key_exists("widget", $context) ? $context["widget"] : (function () { throw new Twig_Error_Runtime('Variable "widget" does not exist.', 1, $this->getSourceContext()); })()), "html", null, true);
        echo "', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
";
        
        $__internal_4b83548672f496fe0a9972306db2b8ad2459c66d767456758391d5f421144ae2->leave($__internal_4b83548672f496fe0a9972306db2b8ad2459c66d767456758391d5f421144ae2_prof);

        
        $__internal_673d2c0f4ddc540ddb564a048c224552ca4d5ff806ffd995980fbdc3b788a642->leave($__internal_673d2c0f4ddc540ddb564a048c224552ca4d5ff806ffd995980fbdc3b788a642_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/money_widget.html.php";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo str_replace('{{ widget }}', \$view['form']->block(\$form, 'form_widget_simple'), \$money_pattern) ?>
", "@Framework/Form/money_widget.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/money_widget.html.php");
    }
}
