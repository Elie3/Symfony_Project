<?php

/* @Framework/Form/widget_attributes.html.php */
class __TwigTemplate_cfe50e9bfbfb7246f8638d0a12a8c8112736197e19a61e2a772fecdee67a1d64 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f1473fc815a5136762146a5c031099ac9bb1cdaaecba7388cb98fe5a54a2ed10 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f1473fc815a5136762146a5c031099ac9bb1cdaaecba7388cb98fe5a54a2ed10->enter($__internal_f1473fc815a5136762146a5c031099ac9bb1cdaaecba7388cb98fe5a54a2ed10_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_attributes.html.php"));

        $__internal_a2d12f868ecd3659289fd987d685fcfb9376a071a96fd8ff2de26cac98b6bc03 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a2d12f868ecd3659289fd987d685fcfb9376a071a96fd8ff2de26cac98b6bc03->enter($__internal_a2d12f868ecd3659289fd987d685fcfb9376a071a96fd8ff2de26cac98b6bc03_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/widget_attributes.html.php"));

        // line 1
        echo "id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php if (\$required): ?> required=\"required\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
";
        
        $__internal_f1473fc815a5136762146a5c031099ac9bb1cdaaecba7388cb98fe5a54a2ed10->leave($__internal_f1473fc815a5136762146a5c031099ac9bb1cdaaecba7388cb98fe5a54a2ed10_prof);

        
        $__internal_a2d12f868ecd3659289fd987d685fcfb9376a071a96fd8ff2de26cac98b6bc03->leave($__internal_a2d12f868ecd3659289fd987d685fcfb9376a071a96fd8ff2de26cac98b6bc03_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/widget_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("id=\"<?php echo \$view->escape(\$id) ?>\" name=\"<?php echo \$view->escape(\$full_name) ?>\"<?php if (\$disabled): ?> disabled=\"disabled\"<?php endif ?>
<?php if (\$required): ?> required=\"required\"<?php endif ?>
<?php echo \$attr ? ' '.\$view['form']->block(\$form, 'attributes') : '' ?>
", "@Framework/Form/widget_attributes.html.php", "/home/elie/Documents/rendu/PHP/pool_symfony/test/vendor/symfony/symfony/src/Symfony/Bundle/FrameworkBundle/Resources/views/Form/widget_attributes.html.php");
    }
}
