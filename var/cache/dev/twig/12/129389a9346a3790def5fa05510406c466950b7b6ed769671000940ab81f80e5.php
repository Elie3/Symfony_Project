<?php

/* PlatypusBundle:Post:my_posts.html.twig */
class __TwigTemplate_97f8d1484475497a4e9f69337329fa1e5d3a52755ed8f68c5f8671acc037a551 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "PlatypusBundle:Post:my_posts.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7aa44a7b5cbcb8b5d21e48295251ac869598d9572566012640838c52b828a2ab = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7aa44a7b5cbcb8b5d21e48295251ac869598d9572566012640838c52b828a2ab->enter($__internal_7aa44a7b5cbcb8b5d21e48295251ac869598d9572566012640838c52b828a2ab_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:Post:my_posts.html.twig"));

        $__internal_db04ff266520584b50de0cc37f7e629a6d3784fe11173e002515084dcc979485 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_db04ff266520584b50de0cc37f7e629a6d3784fe11173e002515084dcc979485->enter($__internal_db04ff266520584b50de0cc37f7e629a6d3784fe11173e002515084dcc979485_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PlatypusBundle:Post:my_posts.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7aa44a7b5cbcb8b5d21e48295251ac869598d9572566012640838c52b828a2ab->leave($__internal_7aa44a7b5cbcb8b5d21e48295251ac869598d9572566012640838c52b828a2ab_prof);

        
        $__internal_db04ff266520584b50de0cc37f7e629a6d3784fe11173e002515084dcc979485->leave($__internal_db04ff266520584b50de0cc37f7e629a6d3784fe11173e002515084dcc979485_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_679b55238e586073bb1af0e4f24dacae028149c3eeb1f69c8acb6f0423d082ce = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_679b55238e586073bb1af0e4f24dacae028149c3eeb1f69c8acb6f0423d082ce->enter($__internal_679b55238e586073bb1af0e4f24dacae028149c3eeb1f69c8acb6f0423d082ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_8c4ca366f86bdb4b78d4d40fd75eece2230e6295554fa9f6d35e0e42760bad5b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8c4ca366f86bdb4b78d4d40fd75eece2230e6295554fa9f6d35e0e42760bad5b->enter($__internal_8c4ca366f86bdb4b78d4d40fd75eece2230e6295554fa9f6d35e0e42760bad5b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
<link rel=\"stylesheet\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("style.css"), "html", null, true);
        echo "\"/>
<table>

<tr><th>Id</th><th>Title</th><th>Content</th><th>Creation Date</th><th>Modification Date</th></tr>
";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["posts"]) || array_key_exists("posts", $context) ? $context["posts"] : (function () { throw new Twig_Error_Runtime('Variable "posts" does not exist.', 9, $this->getSourceContext()); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["elem"]) {
            // line 10
            echo "<tr>
<td>";
            // line 11
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "id", array()), "html", null, true);
            echo "</td>
<td>";
            // line 12
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "title", array()), "html", null, true);
            echo "</td>
<td>";
            // line 13
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "content", array()), "html", null, true);
            echo "</td>
<td>";
            // line 14
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "creation_date", array()), "Y-m-d"), "html", null, true);
            echo "</td>
<td>";
            // line 15
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "modification_date", array()), "Y-m-d"), "html", null, true);
            echo "</td>
<td><a href=\"";
            // line 16
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("edit_a_post", array("id" => twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "id", array()))), "html", null, true);
            echo "\">edit this article</a></td>
<td><a href=\"";
            // line 17
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("delete_a_post", array("id" => twig_get_attribute($this->env, $this->getSourceContext(), $context["elem"], "id", array()))), "html", null, true);
            echo "\">delete this article</a></td>
</tr>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['elem'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 21
        echo "
<table>
<p><a href=\"";
        // line 23
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("create_new_post");
        echo "\" class=\"home_btn\">Create a new Post</a></p>




";
        
        $__internal_8c4ca366f86bdb4b78d4d40fd75eece2230e6295554fa9f6d35e0e42760bad5b->leave($__internal_8c4ca366f86bdb4b78d4d40fd75eece2230e6295554fa9f6d35e0e42760bad5b_prof);

        
        $__internal_679b55238e586073bb1af0e4f24dacae028149c3eeb1f69c8acb6f0423d082ce->leave($__internal_679b55238e586073bb1af0e4f24dacae028149c3eeb1f69c8acb6f0423d082ce_prof);

    }

    public function getTemplateName()
    {
        return "PlatypusBundle:Post:my_posts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 23,  100 => 21,  90 => 17,  86 => 16,  82 => 15,  78 => 14,  74 => 13,  70 => 12,  66 => 11,  63 => 10,  59 => 9,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"base.html.twig\" %}

{% block body%}

<link rel=\"stylesheet\" href=\"{{ asset('style.css')}}\"/>
<table>

<tr><th>Id</th><th>Title</th><th>Content</th><th>Creation Date</th><th>Modification Date</th></tr>
{% for elem in posts%}
<tr>
<td>{{elem.id}}</td>
<td>{{elem.title}}</td>
<td>{{elem.content}}</td>
<td>{{elem.creation_date|date('Y-m-d')}}</td>
<td>{{elem.modification_date|date('Y-m-d')}}</td>
<td><a href=\"{{ path('edit_a_post', {'id': elem.id}) }}\">edit this article</a></td>
<td><a href=\"{{ path('delete_a_post', {'id': elem.id}) }}\">delete this article</a></td>
</tr>

{%endfor%}

<table>
<p><a href=\"{{ path('create_new_post') }}\" class=\"home_btn\">Create a new Post</a></p>




{% endblock %}
", "PlatypusBundle:Post:my_posts.html.twig", "/home/elie/Documents/rendu/PHP/pool_symfony/test/src/PlatypusBundle/Resources/views/Post/my_posts.html.twig");
    }
}
